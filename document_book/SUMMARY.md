# Summary

* [概要](README.md)

* [架构](Framework.md)
  
### src目录

* [api](src/api.md)
* [assets](src/assets.md)
* [components](src/components.md)
* [filters](src/filters.md)
* [mixins](src/mixins.md)
* [router](src/router.md)
* [store](src/store.md)
* [styles](src/styles.md)
* [utils](src/utils.md)

* [views](src/views/README.md)
    * [activate](src/views/activate.md)
    * [approvemanage](src/views/approvemanage.md)
    * [sealmanage](src/views/sealmanage.md)
    * [deptmanage](src/views/deptmanage.md)
    * [faildoc](src/views/faildoc.md)
    * [files](src/views/files.md)
    * [init](src/views/init.md)
    * [layout](src/views/layout.md)
    * [login](src/views/login.md)
    * [myfiles](src/views/myfiles.md)
    * [passmail](src/views/passmail.md)
    * [redirect](src/views/redirect.md)
    * [sysmanage](src/views/sysmanage.md)
    * [template](src/views/template.md)
    * [updateinfo](src/views/updateinfo.md)
    * [usermanage](src/views/usermanage.md)
    
* [配置文件](src/config.md)

* [新建页面](src/newPage.md)
