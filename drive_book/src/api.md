### api 接口文档目录

##### cfg.ts

```typescript
export const PREFIX: string =
  process.env.NODE_ENV === "development" ? "/api" : "";
```

> 由于项目最终是打包放到服务端的静态资源路径下。开发环境默认接口请求前缀配置为 **/api** ，生产环境取消前缀。（考虑到后期如果是前后分离，需要做反向代理的情况。或者是其他项目想要用这套架构。）



##### drive.ts

驱动管理 接口

```typescript
/**
 * 获取驱动列表
 * @param data
 */
export function drive_pages(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/driver/drivers`,
    method: "post",
    data
  });
}
...
...
```



##### log.ts

日志管理 接口

```typescript
/**
 * 获取安装列表
 * @param {*} data
 */
export function log_pages(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/driver/installlogs`,
    method: "post",
    data
  });
}
...
...
```



##### user.ts

用户登录，修改密码 接口

```typescript
/**
 * 修改密码
 * @param data
 */
export function user_passwd(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/user/passwd`,
    method: "put",
    data
  });
}
...
...
```



##### strategy.ts

升级策略 接口

```typescript
/**
 * 获取升级策略
 */
export function strategy(): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/strategy`,
    method: "get"
  });
}
```

