### 工具包



##### index.ts

自定义工具包主文件

```javascript
/**
 * remove Object key/value for value is undefined
 *
 * @export
 * @template T
 * @returns {*}
 */
export function ObjectToCleanUndefined<T extends any>(target: T): any {
  const cache: any = {};
  for (const key in target) {
    if (target.hasOwnProperty(key)) {
      const val = target[key];
      if (key == "startTime" || key == "endTime") {
        const _keys: any = {
          startTime: DateRound.Start,
          endTime: DateRound.end
        };
        const _date = FmtDate(val, _keys[key]);
        if (_date) cache[key] = _date;
      } else if (val || val == 0) {
        if (val !== "") cache[key] = val;
      }
    }
  }
  return cache;
}

/**
 *formate date
 *
 * @export
 * @template T
 * @param {T} date
 * @returns {string}
 */
export function FmtDate<T extends any>(
  date: T,
  tail?: DateRound,
  format: string = "YYYY-MM-DD"
): string {
  if (!date) return "";
  let tailStr: string = "";
  if (tail) {
    tailStr = " " + (tail == DateRound.Start ? "00:00:00" : "23:59:59");
  }
  const _date = moment(date).format(format);
  return _date + tailStr;
}
...
```



##### Error.ts

错误码配置文件， 用在 *request.ts* 请求时，服务端抛异常错误码对应的文字说明。

```typescript
export default {
  3000: "文件数目错误",
  3001: "文件状态错误",
  3002: "未查找到驱动",
  3003: "下载队列拥挤，请稍后再试",
  1000: "服务器系统错误",
  2001: "请求参数错误",
  ...errorCode
};
```



##### VEnum.ts

枚举文件

```typescript
/**
 * 升级结果
 *
 * @export
 * @enum {number}
 */
export enum UpgradeResult {
  Error,
  Success
}

/**
 * 开始 & 结束 日期枚举
 *
 * @export
 * @enum {number}
 */
export enum DateRound {
  Start = 1,
  end
}
...
```

