### 布局模块

> 该模块无需再处理。如若后期 **国际化** ，需要再**Navbar** 里面 放开国际化标签即可。



```javascript
      <template v-if="device!=='mobile'">
        <!-- 全屏 -->
        <el-tooltip :content="$t('navbar.screen')" effect="dark" placement="bottom">
          <screenfull class="screenfull right-menu-item"/>
        </el-tooltip>
        <!-- 布局大小 -->
        <el-tooltip :content="$t('navbar.size')" effect="dark" placement="bottom">
          <size-select class="international right-menu-item"/>
        </el-tooltip>
        <!-- 换肤 -->
        <!-- <el-tooltip :content="$t('navbar.theme')" effect="dark" placement="bottom">
          <theme-picker class="theme-switch right-menu-item"/>
        </el-tooltip> -->

        <!-- 国际化 -->
        <!-- <el-tooltip :content="$t('navbar.language')" effect="dark" placement="bottom">
          <lang-select class="right-menu-item hover-effect" />
        </el-tooltip> -->
      </template>
```

