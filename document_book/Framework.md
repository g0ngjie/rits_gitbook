# 架构

##### 技术选型

> vue-cli@2、webpack@3、element-ui@2.12.0、scss、

* **ECMAScript** 的新标准 【熟练使用】
* **vue-cli** 【（vue）熟练使用】
* **webpack** 【了解】 
* **[element-ui](https://element.eleme.cn/#/zh-CN)**【需要看文档】
  * 目前市场上vue最好后台管理 *UI框架*。
  * 社区活跃，一般遇到的问题，都会有解决方式。
* scss【了解】 
  * element-ui 底层使用的是scss。

