FROM nginx

WORKDIR /app

RUN cd /app && mkdir drive doc basic
COPY drive_book/_book /app/drive
COPY document_book/_book doc
COPY vue_basic_book/_book basic

RUN rm /etc/nginx/conf.d/default.conf
RUN rm /etc/nginx/nginx.conf
COPY nginx.conf /etc/nginx/

RUN rm /usr/share/nginx/html/index.html
COPY index.html /usr/share/nginx/html
COPY favicon.ico /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]