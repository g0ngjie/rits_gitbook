### 管理员初始化

![](..\..\images\init.jpg)



##### index.vue

```javascript
<script>
import { user_admin } from '@/api/user';// 接口文件
import { LocalStorageEnum as LSEnum } from '@/utils/Enum/VEnum'; // 前端枚举

export default {
    data() {
        return {
            loading: false, // loading
            form: {} // 表单数据
        }
    },
    methods: {
        async submit() { // 表单提交
            try {
                this.loading = true;
                let { data } = await user_admin(this.form);
                this.loading = false;
                this.$alert('管理员创建成功', '提示', {
                    confirmButtonText: '确定',
                    type: 'success', showClose: false
                }).then(() => {
                    localStorage.removeItem(LSEnum.token);
                    location.href = '/';
                    return;
                })
            } catch (error) {
                this.loading = false;
            }
        },
        async validate() { // 表单校验
            this.$refs.form.validate(valid => {
                if (valid) {
                    this.submit();
                }
            });
        },
    }
}
</script>
```

