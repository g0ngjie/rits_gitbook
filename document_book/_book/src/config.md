### 配置文件

> webpack文件，开发环境中，需要改动的地方只有如下三项。其他不需要再更改。
>
> * `const Switch = DevConf.Remote; ` 这个配置 是做服务端代理修改的。默认已经都配置好。只需要切换就可以。
> * `host: 'localhost'` 修改 前端项目的访问路径，注意，如果想要用ip访问的话，localhost 需要改为本机实际的ip地址。
> * `port: 8080` 前端访问的端口号配置



```javascript
/**
 * 需要的地址在这里配
 */
const DevConf = {
  Local: {
    api: "http://127.0.0.1:3000",
    ws: "http://127.0.0.1:3001"
  },
  Remote: {//17服务器
    api: "http://172.25.73.17:12306",
    ws: "http://172.25.73.17:12307"
  },
  Pro: {//17 QA测试专用服务
    api: "http://172.25.73.17:3000",
    ws: "http://172.25.73.17:3001"
  }
}
//只需要改它就可以了,
const Switch = DevConf.Remote; 

module.exports = {
  Switch,
  dev: {

    // Paths
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {
      "/api": {
        target: Switch.api,
        changeOrigin: true,
        pathRewrite: {
          "^/api": "/"
        }
      },
      "/ws": {
        target: Switch.ws,
        changeOrigin: true,
        ws: true,
        secure: false,
        pathRewrite: {
          "^/ws": "/"
        }
      }
    },

    // Various Dev Server settings
    host: 'localhost', // can be overwritten by process.env.HOST
    port: 8080, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
    autoOpenBrowser: false,
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-

   ...
   ...   
}
```

