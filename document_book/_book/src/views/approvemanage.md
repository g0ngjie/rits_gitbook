### 审批规则管理



![](..\..\images\approvate.jpg)



##### index.js

> 表格页面。



```javascript
<script>
import { rule } from '@/api';// 接口引入
import {
    Pagination as paginationTemp, // 分页组件
    ToolButton as toolBtn //工具栏组件
} from '@/components';
import { RuleType as RuleTypeEnum } from '@/utils/Enum/FEnum'; // 服务端枚举
import { RouterToEnum } from '@/utils/Enum/VEnum'; // 前端枚举

export default {
    components: { paginationTemp, toolBtn }, // 局部组件注册
    data() {
        return {
            loading: false, // loading
            btnEnable: false, // 定义按钮是否禁用
            listQuery: { page: 0, count: 10 }, // 默认查询参数 添加分页
            tableData: [], // 表格数据
            multipleSelection: [], // 多选
            page: { current: 1, total: 0 }, // element 组件的当前页 和 总数
        }
    },
    computed: { // 计算函数
        fileTypeEnum() {// 适用文档类型 国际化配置文件
            return require(`./temp/conf`)['fileTypeEnum' + this.$t("lang.type")]
        }
    },
    methods: {
        searchChange(key, e) {// 搜索栏 发生改变时触发， 主要解决输入为空时， 移除key/value
            if (!e) delete this.listQuery[key];
        },
        handleSelectionChange(val) {// 多选时 触发
            this.multipleSelection = val;
            this.btnEnable = val.length === 1;
        },
        async del() {// 删除
            try {
                const res = await this.$confirm(this.$t("approvemanage.msg.isDel"));
                if (res == "confirm") {
                    let { _id } = this.multipleSelection[0];
                    this.loading = true;
                    let { data } = await rule.rule_delete(_id);
                    this.loading = false;
                    this.$message.success(this.$t("approvemanage.msg.success"));
                    this.initList();
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async edit() {// 跳转编辑页面
            this.$store.dispatch("setParams", this.multipleSelection[0]);
            this.$router.push({ path: "/updateapprove", query: { to: RouterToEnum.edit } });
        },
        openAdd() {// 跳转添加页面
            this.$router.push({ path: "/updateapprove", query: { to: RouterToEnum.add } });
        },
        search() { // 搜索函数
            this.initList();
        },
        async initList() {// 初始化的数据
            const sortHandler = (item1, item2) => {
                return item2.level - item1.level;
            }
            this.loading = true;
            const query = {...this.listQuery, type: RuleTypeEnum.audit};
            const [{ data: page }, { data: count }] = await Promise.all([rule.rule_page(query), rule.rule_count(query)]);
            this.page.total = count.data.count;
            this.loading = false;
            let formData = page.data.map(item => {
                return {
                    ...item,
                    auditPath: item.auditPath.sort(sortHandler),
                }
            });
            this.tableData = formData;
        },
    },
    created() {// 钩子函数 页面加载前触发
        this.initList();
        this.$store.dispatch("clearParams");// 清理跳转参数
    }
}
</script>
```



##### temp/index.vue

> 添加/编辑页面

```javascript
<script>
import { mapGetters } from "vuex";
import { userLevel, autidUsers, LimitLevel, S2C } from "./conf";// 审批规则配置文件
import { rule, dept } from "@/api";// 接口文件
import { RuleType as RuleTypeEnum } from '@/utils/Enum/FEnum';// 服务端枚举
import { RouterToEnum } from '@/utils/Enum/VEnum';// 前端枚举

export default {
    data() {
        return {
            form: { categories: [], auditPath: [] },// 表单数据
            loading: false,// loading
            selectLevel: {
                default: "1", //默认一级
                deptLevel: [], //部门等级
            },
            auditList: [1] //默认一级
        };
    },
    computed: {//计算函数
        ...mapGetters(["queryParams"]),// vuex getters使用
        title() {// 根据queryParams的name 判断时 添加 还是编辑。改变title的值
            if (!this.queryParams.name) {
                return this.$t("approvemanage.temp.title.add");
            } else {
                return this.$t("approvemanage.temp.title.edit");
            }
        },
        fileTypeList() {// 获取国际化的 文档类型
            return require(`./conf`)["fileTypeList" + this.$t("lang.type")];
        }
    },
    methods: {
        back() {// 返回上一页
            this.$router.go(-1);
        },
        async submit() {// 表单提交
            let { name, categories } = JSON.parse(
                JSON.stringify(this.form)
            );
            let auditPath = [];
            const level = this.selectLevel.default;
            for (let i = 1; i <= +level; i++) {
                auditPath.push({
                    ...userLevel[i],
                    roleName: this.$t('approvemanage.auditUsers')[i]
                })
            }
            let formData = { name, categories, auditPath, type: RuleTypeEnum.audit };
            try {
                this.loading = true;
                if (!this.queryParams.name) {
                    let data = await rule.rule_add(formData);
                    this.$message.success(this.$t("approvemanage.msg.add"));
                } else {
                    let { data } = await rule.rule_edit(this.form._id, formData);
                    this.$message.success(this.$t("approvemanage.msg.edit"));
                }
                this.loading = false;
                this.back();
            } catch (error) {
                this.loading = false;
            }
        },
        async validate() {// 表单校验
            if (this.form.categories.length == 0) {
                this.$message.warning(this.$t("approvemanage.msg.categories"));
                return;
            }
            this.$refs.form.validate(valid => {
                if (valid) {
                    this.submit();
                }
            });
        },
        initAudit(){//编辑页面,审查路径和等级 初始化
            const { auditPath } = this.queryParams;
            //获取服务端最大级别职位 对应的客户端 最小等级
            const minLevel = auditPath.map(line => line.level).sort()[0];
            //设置默认选中等级
            this.selectLevel.default = S2C[minLevel] + '';
            //设置审查路径
            this.fmtAuditPath(S2C[minLevel]);
        },
        async initDeptLevels(){
            const { data } = await dept.dept_level();
            if(data.errorCode == 0){
                const { level } = data.data;
                for (let i = 1; i <= level; i++) {
                    if(i > LimitLevel) break;
                    this.selectLevel.deptLevel.push({
                        level: i + '',
                        name: this.$t('approvemanage.levelToStr')[i - 1],
                    })
                }
            }
        },
        fmtAuditPath(level){//获取审查人员路径
            this.auditList = [];
            for (let j = 0; j < level; j++) {
                this.auditList.push(autidUsers[j])
            }
        }
    },
    async created() {// 钩子、页面加载前触发
        await this.initDeptLevels();
        if (this.queryParams.name) {
            this.form = this.queryParams;
            this.initAudit();
        }
        if (this.$route.query.to == RouterToEnum.edit && !this.queryParams.name) {
            this.$router.go(-1);
        }
    }
};
</script>
```



##### conf.js

> 审批规则配置文件

```javascript
/**
 * 国际化 数据配置文件
 */
export const fileTypeListZh = [
    {
        label: '收入合同',
        value: 1
    },
    {
        label: '支出合同',
        value: 2
    },
    {
        label: '不涉及金额特别合同',
        value: 3
    },
    {
        label: '不涉及金额一般合同',
        value: 4
    },
    {
        label: '文书',
        value: 5
    },
    {
        label: '直接保管文书',
        value: 6
    }
]

export const fileTypeListJa = [
    ...
]

export const fileTypeListEn = [
    ...
]

export const fileTypeEnumZh = [
    '收入合同','支出合同','不涉及金额特别合同','不涉及金额一般合同','文书','直接保管文书'
]
export const fileTypeEnumJa = [...]
export const fileTypeEnumEn = [...]

export const docListZh = [
    {
        label: '课长',
        value: 1
    },{
        label: '部长',
        value: 2
    },{
        label: '事业部长或中心长',
        value: 3
    },{
        label: '总经理',
        value: 4
    }
]
export const docListJa = [
    ...
]
export const docListEn = [
    ...
]

import { 
    SUserLevelEnum as SULEnum, 
    CUserLevelEnum as CULEnum 
} from '@/utils/Enum/VEnum';

/**
 * 客户端 用户审查列表
 */
export const autidUsers = [ CULEnum.class, CULEnum.dept, CULEnum.center, CULEnum.boss ];

/**
 * 客户端对应服务端
 */
export const C2S = {
    [CULEnum.class]: SULEnum.class,
    [CULEnum.dept]: SULEnum.dept,
    [CULEnum.center]: SULEnum.center,
    [CULEnum.boss]: SULEnum.boss
}

/**
 * 服务端对应客户端
 */
export const S2C = {
    [SULEnum.class]: CULEnum.class,
    [SULEnum.dept]: CULEnum.dept,
    [SULEnum.center]: CULEnum.center,
    [SULEnum.boss]: CULEnum.boss
}

/**
 * 用户等级
 */
export const userLevel = {
    [CULEnum.class]: { role: -1, level: SULEnum.class},
    [CULEnum.dept]: { role: -1, level: SULEnum.dept},
    [CULEnum.center]: { role: -1, level: SULEnum.center},
    [CULEnum.boss]: { role: -1, level: SULEnum.boss}
}

/**
 * 限制 等级列表
 * 目前最大为四级
 */
export const LimitLevel = 4;
```

