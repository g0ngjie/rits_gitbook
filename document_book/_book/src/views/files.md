### 全部文档、更早文档、上下期度文档



![](..\..\images\files.jpg)



##### index.vue

```javascript
<script>
import { doc, operator, file } from '@/api'; // 接口文件
import readTemp from './readTemp'; // 预览 组件
import searchTemp from './searchTemp'; // 搜索栏 组件
import { mapGetters } from 'vuex';
import { fileI18nHandler, sortChange } from '@/mixins'; // mixins
import { DomArgsEnum, RouterPathEnum, RouterToEnum } from '@/utils/Enum/VEnum'; // 前端枚举
import { 
    DocumentState as DocStateEnum,
    UserRole as RoleEnum,
} from '@/utils/Enum/FEnum'; // 服务端枚举
import {
    ToolButton as toolBtn,
    Pagination as paginationTemp,
} from '@/components'; // 工具栏、分页 组件
import { fmtSearchTime } from '@/utils';// 工具
import { fmtDate } from '@/filters'; // 过滤器
import printList from '@/views/myfiles/printList';// 打印预览附件列表 组件

export default {
    components: { // 组件注册
        readTemp,
        searchTemp,
        paginationTemp,
        toolBtn,
        printList
    },
    filters: { fmtDate }, // 过滤器
    data() {
        return {
            DomArgsEnum, /* toolBar形参枚举 */
            RouterToEnum, /* 路径 */
            loading: false, // loading
            listQuery: { page: 0, count: 10, category: null, type: null }, // 查询参数
            tableData: [], // 表格 数据
            page: { current: 1, total: 0 }, // 分页数据
            btnEnable: false, // 安装状态
            /* 导出按钮判断 */
            xlsxEnable: false,
            xlsxShow: false,
            docxEnable: false,
            docxShow: false,
            /* 导出按钮判断 */
            exportIds: [], // 报表导出 的行选择 id 列表
            archived: false, // 是否存档完成
            isEdit: false, //是否可编辑
            form: {}, // 单行选中数据
            footer: { totalCount: 0, totalAmount: 0, details: [] }, // 页脚 数据
            multipleSelection: [], // 行选中列表
        }
    },
    mixins: [fileI18nHandler, sortChange], // mixins
    computed: {
        ...mapGetters([
            'cacheMenu',
            'isAdmin',
            'isTax',
            'queryParams',
            'user'
        ]),
        isDel() {//管理员 和 存档担当 具有 删除权限
            if (this.isAdmin || this.user.roles.includes(RoleEnum.archiver)) {
                return true;
            } else {
                return false;
            }
        }
    },
    methods: {
        dispatchClick(arg){// 点击事件 分发
            const { del, reset, print } = DomArgsEnum;
            const { add, copy, edit, lend } = RouterToEnum;
            switch (arg) {
                case add:
                case copy:
                case edit:
                    this.$router.push({ path: RouterPathEnum.addTemp, query: { to: arg } })
                    break;
                case lend: /* 纸质文档借还 */
                    this.$router.push({ path: RouterPathEnum.lendTemp, query: { to: arg } })
                    break
                case del: /* 删除文档 */
                    this.deleteFile()
                    break;
                case reset: /* 重置文档 */
                    this.resetFile()
                    break;
                case print:
                    this.printHandler();
                    break;
                default:
                    break;
            }
        },
        async printHandler(){// 点击打印按钮，获取 当前选中行 的附件列表
            let { _id, viewPos, uploadPos } = this.multipleSelection[0];
            console.log(viewPos, 'viewPos')
            // viewPos = uploadPos; 临时为了测试 所以才吧 uploadPos 赋值给 viewPos
            if(!viewPos || viewPos.length == 0){
                this.$message.warning('当前文档文件正在做格式转换,请稍后再试...');
                return;
            }
            await this.$store.dispatch('cacheLocalPrintJob', _id);
            this.form = this.multipleSelection[0]
            this.$refs.printList.isShow = true;
        },
        async choicePrint([row, { fileName, storagePath }]){ // 根据选中附件， 跳转到 pdf 预览页面
            try {
                const { _id } = row;
                // this.loading = true;
                const { data: view } = await file.file_view({ docId: _id, fileName, storagePath });
                console.log(view,' view')
                this.loading = false;
                if(view.errorCode == 0){
                    const secretKey = view.data;
                    const { data: urlData } = await file.file_pdf(secretKey);
                    // const pdfPublicUri = `/api/file/pdf?key=${secretKey}`;
                    console.log(urlData, 'urlddata')
                    const { value } = urlData;
                    console.log(value, 'value')
                    // const src = 'https://cdn.filestackcontent.com/wcrjf9qPTCKXV3hMXDwK'
                    const job = await this.$store.dispatch('cacheLocalPrintJob', { _id, fileName, src: value })
                    if(job){
                        const { href } = this.$router.resolve({ path: '/pdfview' });
                        window.open(href, '_blank');
                    }
                }
            } catch (err){
                this.loading = false;
            }
        },
        exportShowHandler(checks) {//判断 导出按钮 是否展示
            /**
             * 1、选中至少一条 V
             * 2、state === 5
             * 3、type === 11
             * 4、isTax 免税担当
             */
            if (this.isTax) {
                this.xlsxEnable = checks.length > 0;
                this.docxEnable = checks.length === 1;
            }
            const { sealed, denied, reject, lent } = DocStateEnum;
            if (checks.length === 1) {//判断word
                if (checks[0].state >= sealed && checks[0].state != denied && checks[0].state != reject && checks[0].type == lent) {
                    this.xlsxShow = true;
                    this.docxShow = true;
                } else {
                    this.xlsxShow = false;
                    this.docxShow = false;
                }
            } else if (checks.length > 1) {
                this.docxShow = false;
                for (let i = 0; i < checks.length; i++) {
                    const item = checks[i];
                    if (item.state >= sealed && item.state != denied && item.state != reject && item.type == lent) {
                        this.xlsxShow = true;
                    } else {
                        this.xlsxShow = false;
                        break;
                    }
                }
            } else {
                this.xlsxShow = false;
                this.docxShow = false;
            }
            if (!this.xlsxShow) {
                this.xlsxEnable = false;
            }
            if (!this.docxShow) {
                this.docxEnable = false;
            }
            this.exportIds = checks.map(item => item._id);
        },
        handleSelectionChange(val) {// 行选中事件
            this.multipleSelection = val;
            this.btnEnable = val.length === 1;
            this.exportShowHandler(val);
            if (val.length === 1) {
                this.$store.dispatch("setParams", val[0]);
                const { state, uid } = val[0];
                //存档完成 包含的两个状态
                const archivedStates = [DocStateEnum.archived, DocStateEnum.lent]
                if (archivedStates.includes(state)) {
                    //判断当前登录用户角色是否包含 存档担当
                    this.archived = this.user.roles.includes(RoleEnum.archiver);
                }
                //可编辑 包含的两个状态
                const editables = [DocStateEnum.making, DocStateEnum.reject]
                if (editables.includes(state)) {
                    this.isEdit = true;
                }
            } else {
                this.$store.dispatch("clearParams");
                this.archived = false;
                this.isEdit = false;
            }
        },
        async rowClick(row, column, event) {// 行点击事件
            if (column.property == "createTime" || column.type == "selection") {
                return;
            }
            await this.$refs.readTemp.initRecords(row._id);
            this.$refs.readTemp.isShow = true;
            this.form = row;
        },
        async exportOffice(num) { // 报表导出 函数
            const conf = ['财务印花税使用.xlsx', '免税商委使用.xlsx', '免税税务局使用.xlsx', '免税备案表_税务局使用.docx']
            const ids = this.exportIds.join();
            const data = await doc.doc_export({ ids, type: num, name: conf[num - 1] });
            window.location.href = `/api/doc/export?ids=${ids}&type=${num}&name=${conf[num - 1]}`;
        },
        async resetFile() {//重置文档
            try {
                const res = await this.$confirm(this.$t('files.table.msg.reset'));
                if (res == "confirm") {
                    this.loading = true;
                    let { data } = await operator.operator_doc_reset(this.multipleSelection[0]._id);
                    this.loading = false;
                    if (data.errorCode === 0) {
                        this.$message.success(this.$t('files.table.msg.successReset'));
                        this.initList();
                    }
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async deleteFile() {//删除文档
            try {
                const res = await this.$confirm(this.$t('files.table.msg.confirm'));
                if (res == "confirm") {
                    this.loading = true;
                    let { data } = await doc.doc_delete(this.queryParams._id);
                    this.loading = false;
                    if (data.errorCode === 0) {
                        this.$message.success(this.$t('files.table.msg.del'));
                        this.initList();
                    }
                }
            } catch (error) {
                this.loading = false;
            }
        },
        fmtQuery() {// 格式化查询参数
            let query = fmtSearchTime(this.listQuery)
            if (this.cacheMenu[this.$route.path]) {
                query = { ...query, ...this.cacheMenu[this.$route.path] };
            }
            return query;
        },
        async initList() { // 初始化数据
            this.loading = true;
            let query = this.fmtQuery();
            const [{ data: page }, { data: count }] = await Promise.all([doc.doc_page(query), doc.doc_count(query)]);
            this.page.total = count.data.totalCount;
            this.footer = count.data;
            this.loading = false;
            this.tableData = page.data;
        },
    },
    created() {// 钩子、页面加载前触发
        this.initList();
        this.$store.dispatch("clearParams");
    }
}
</script>
```



![](..\..\images\files_read.jpg)



##### readTemp/index.vue

> 文档查看

```javascript
<script>
import { doc_records } from '@/api/doc'; // 接口文件
import { fileI18nHandler } from '@/mixins'; // mixins
import { fmtDate } from '@/filters'; // 过滤器
import { RecordType as RecordTypeEnum } from '@/utils/Enum/FEnum'; // 服务端枚举

export default {
    filters: { fmtDate }, // 过滤器注册
    data() {
        return {
            isShow: false, // 是否展示
            flowList: [], // 履历数据列表
        }
    },
    props: {
        form: {// 父组件传参
            type: Object,
            default() { }
        }
    },
    mixins: [fileI18nHandler], // mixins
    methods: {
        fmtCurrencyUnit(num) {// 格式化 货币单位
            let _curr = {};
            this.currencyUnit.map(item => {
                _curr[item.id] = item.name
            })
            return num && _curr[+num];
        },
        async initRecords(docId) {//查询 审核记录 或 出借记录
            try {
                let { data } = await doc_records(docId, RecordTypeEnum.review);
                if (data.errorCode == 0) {
                    this.flowList = data.data;
                }
            } catch (error) {

            }
        }
    },
}
</script>
```



![](..\..\images\files_add.jpg)



##### addTemp/index.vue

> 文档添加、编辑、复制

```javascript
<script>
import moment from 'moment'; // moment 日期工具
import { mapGetters } from 'vuex';
import { 
    ReqOptType as ReqOptTypeEnum,
    DocumentCategory as CategoryEnum,
    DocumentType as TypeEnum,
    DocumentState as StateEnum
} from '@/utils/Enum/FEnum'; // 服务端枚举
import { doc } from '@/api';// 接口文件
import { igniteDataHandler, uploadHandler, fileI18nHandler } from '@/mixins'; // mixins
import approveTemp from '@/views/files/approve'; // 审查模板组件
import { Switch as vswitch } from '@/components'; // switch 选择器 组件
import { LocalStorageEnum as LSEnum, RouterToEnum } from '@/utils/Enum/VEnum'; // 前端枚举
import _ from 'lodash'; // lodash 工具
import { omit } from '@/utils'; // 自定义工具

export default {
    components: { // 组件注册
        approveTemp,
        vswitch
    },
    mixins: [uploadHandler, fileI18nHandler, igniteDataHandler], // mixins
    data() {
        return {
            loading: false, // loading
            headers: { token: localStorage.getItem(LSEnum.token) }, // 上传文件 请求头部
            form: { sealTypes: [], uploadPos: [], category: null, type: null, format: false, pagingSeal: false, state: StateEnum.making, signatoryType: true }, // 表单form 数据
            checkBoxActive: 0,
            typeList: [], // 文档类别 列表 （父）
            childTypeList: [], // 文档类型 列表 （子）
            noPrice: false, // 是否涉及金额
            noContract: false, // 是否 显示签约方
            checkBoxs: [], // 盖章 列表
            isSeam: false,//骑缝章
            isDf: true,//是否需要 DF
            isDateRound: false, // 合同期间 是否展示
            approveBtn: '', // 国际化 按钮内容
            approveObj: { reqOptType: null, documentCategory: null, multiple: false }, //审批模板参数
            approveLines: {}, // 获取审查 模板 数据
            isOverseasContract: false, //境外合同
            isCurrencyUnit: false, //货币单位是否必选
            auditList: [], // 审查路径人员列表
            defaultAudits: {}, //默认值
        }
    },
    computed: {
        ...mapGetters([
            'user',
            'queryParams'
        ]),
        uploadData() {// 上传 默认 data 值
            return { storage: 1, documentCategory: this.form.category };
        },
        title() {// 小标题（国际化）
            return this.$route.query.to == RouterToEnum.edit ? this.$t('files.addTemp.title')[0] : this.$t('files.addTemp.title')[1];
        },
        approveBtnCache() {// 国际化 按钮内容 
            return this.$t('files.addTemp.approveBtn')
        },
        inputMoney() {
            return [this.form.contractMoney, this.form.applyMoney];
        }
    },
    watch: {// 监听 data 属性改变
        approveBtnCache() {
            //重新获取 国际化 按钮内容
            if (this.form.category) {
                this.approveBtn = !this.contractType[this.form.category - 1].isSave ? this.approveBtnCache[0] : this.approveBtnCache[1];
            } else if (this.$route.query.to != RouterToEnum.add) {
                let { category } = this.queryParams;
                //申请保管 or 申请盖章
                this.approveBtn = !this.contractType[category - 1].isSave ? this.approveBtnCache[0] : this.approveBtnCache[1];
            } else {
                this.approveBtn = this.approveBtnCache[0];
            }
        },
        contractType() {
            this.initSelectList();
            this.getSealType(this.form.type, null);
        },
        inputMoney(moneys) {
            let flag = false;
            moneys.forEach(val => {
                if (val != null && val != undefined && val != '') {
                    flag = true;
                }
            });
            this.isCurrencyUnit = flag;
            if (!flag) {
                this.clearValidate();
            }
        }
    },
    methods: {
        back() {// 返回上一层
            this.$router.go(-1);
        },
        contractTypeHandler(e) {// 文档类别（父） 下拉选择器 选择时触发 函数
            this.clearValidate();
            //合同期间 
            this.dateRoundShow(e);
            if (!e) {
                this.childTypeList = _.cloneDeep(this.childContractType);
                this.form.type = undefined;
                this.getSealType(null, null);
                this.noPrice = false;
                this.noContract = false;
                this.approveBtn = this.approveBtnCache[1];//申请保管
                return;
            }
            let obj = {};
            for (const id in this.childContractType) {
                if (Math.floor(id / 10) === e) {
                    obj[id] = this.childContractType[id];
                }
            }
            this.childTypeList = obj;
            this.form.type = Object.keys(obj)[0];
            this.getSealType(Object.keys(obj)[0], null);
            //是否涉及金额
            this.noPrice = !this.contractType[e - 1].noPrice;
            //是否 显示签约方
            this.noContract = !this.contractType[e - 1].noContract;
            //申请保管 or 申请盖章
            this.approveBtn = !this.contractType[e - 1].isSave ? this.approveBtnCache[0] : this.approveBtnCache[1];
            //设置审批参数
            this.approveObj.reqOptType = !this.contractType[e - 1].isSave ? ReqOptTypeEnum.seal : ReqOptTypeEnum.keep;
            this.approveObj.documentCategory = e;
        },
        childContractTypeHandler(e) {// 文档类型（子） 下拉选择器 选择时触发 函数
            this.clearValidate();
            if (!e) {
                this.typeList = _.cloneDeep(this.contractType);
                this.getSealType(null, null);
                //合同期间 
                this.dateRoundShow(null);
                return;
            }
            /**格式化当前选中归属与的子类型 */
            let obj = {};
            for (const id in this.childContractType) {
                if (Math.floor(id / 10) === Math.floor(e / 10)) {
                    obj[id] = this.childContractType[id];
                }
            }
            this.childTypeList = obj;
            /***************************** */
            this.getSealType(e, null);
            //合同期间 
            this.dateRoundShow(Math.floor(e / 10));
            this.form.category = Math.floor(e / 10);
            //设置审批参数
            this.approveObj.documentCategory = Math.floor(e / 10);
            this.approveObj.reqOptType = !this.contractType[this.form.category - 1].isSave ? ReqOptTypeEnum.seal : ReqOptTypeEnum.keep;
            //是否涉及金额
            this.noPrice = !this.contractType[this.form.category - 1].noPrice;
            //是否 显示签约方
            this.noContract = !this.contractType[this.form.category - 1].noContract;
            //申请保管 or 申请盖章
            this.approveBtn = !this.contractType[this.form.category - 1].isSave ? this.approveBtnCache[0] : this.approveBtnCache[1];
        },
        dateRoundShow(category) {// 合同期间 函数
            const { incomeContract, exportContract, specialContract, normalContract } = CategoryEnum;
            const required_ = [incomeContract, exportContract, specialContract, normalContract];
            if (category && required_.indexOf(category) != -1) {
                this.isDateRound = true;
            } else {
                this.isDateRound = false;
                this.form = omit(this.form, ['startTime', 'startTime_', 'endTime', 'endTime_'])
            }
        },
        getSealType(e, isCreated) {//获取盖章列表
            //判断境外合同，校验货币单位、合同金额 e == 11 境外合同； 
            this.isOverseasContract = e == TypeEnum.foreginTrustee;
            if (e) {//isDf
                this.isDf = !this.childContractType[e].noDf;
            }
            
            const { pagingSeal } = this.form;

            if (e && this.childContractType[e].sealType != null) {
                this.checkBoxs = this.sealTypeList[this.childContractType[e].sealType];
                this.form.sealTypes = [];
                if (this.childContractType[e].sealType == 0) {
                    this.form.sealTypes = [1];
                }
                //"收入合同"和"支出合同"时, 默认勾选【公司合同章】。
                const { incomeContract, exportContract, instrument } = CategoryEnum;
                if ([incomeContract, exportContract].includes(Math.floor(e / 10))) {
                    this.form.sealTypes.push(2);
                }
                if (Math.floor(e / 10) != instrument) {
                    this.form.pagingSeal = isCreated == 'created' ? pagingSeal : true;
                    this.isSeam = true;
                } else {
                    this.form.pagingSeal = isCreated == 'created' ? pagingSeal : false;
                    this.isSeam = false;
                }
            } else {
                this.checkBoxs = [];
                this.isSeam = false;
                this.form.pagingSeal = isCreated == 'created' ? pagingSeal : false;
            }
        },
        tempDestroyed(signatoryType) {// 审查模板 关闭时 触发函数
            this.form.signatoryType = signatoryType;
        },
        async approveHandler() {// 1、审查模板 内 是否多人确认 按钮点击时 触发 当前（父） 组件函数 2、表单提交时 获取需要的审查数据
            const { instrument, otherInstrument } = CategoryEnum;
            const { noAudit } = TypeEnum;
            const { type, category, contractMoney, signatoryType, sealTypes } = this.form;
            if (![instrument, otherInstrument].includes(category)) {
                //签约方 如果文档类别为 文书（category == 5 || 6），则不传参
                this.approveObj.signatoryType = signatoryType;
            }
            if (+type == noAudit) {
                this.approveObj.documentType = +type;
                //文档类型如果为 直接保管文书 66，reqOptType = 8
                this.approveObj.reqOptType = ReqOptTypeEnum.directArchive;
            }
            if (+contractMoney) {
                this.approveObj.contractMoney = +contractMoney
            } else {
                this.approveObj = omit(this.approveObj, 'contractMoney')
            }
            if (sealTypes.length > 0) {
                this.approveObj.sealType = sealTypes;
            }
            if (+category == otherInstrument) {
                this.approveObj = omit(this.approveObj, 'sealType')
            }
            try {
                let { data } = await doc.doc_process(this.approveObj);
                if (data.errorCode === 0) {
                    this.approveLines = data.data;
                    if (data.data.keepLines.length > 0) {
                        this.$refs.approve.isKeepLines = true;
                    } else {
                        this.$refs.approve.isKeepLines = false;
                    }
                }
                if (+type == noAudit) {
                    let onlySaveData = await this.getDefaultFormData();
                    onlySaveData.reqOptType = ReqOptTypeEnum.directArchive;
                    this.submit('approve', onlySaveData)
                } else {
                    this.auditList = await this.getAuditList();
                    this.defaultAudits = await this.getDefaultAudits();
                    this.$refs.approve.isShow = true;
                }
            } catch ({ errorCode, errorMsg }) {

            }
        },
        async submit(decide, formData) {//表单提交
            const { add, edit, copy } = RouterToEnum;
            const checkVal = this.$route.query.to;
            try {
                this.loading = true;
                let data;
                if (checkVal == edit) {
                    let res = await doc.doc_update(this.form._id, this.form);
                    data = res.data;
                } else if (checkVal == add) {
                    let res = await doc.doc_save({ ...this.form, uid: this.user._id });
                    data = res.data;
                } else if (checkVal == copy) {
                    this.form = omit(this.form, ['_id', '__v', 'createTime', 'uid', 'archiveEndTime']);
                    this.form.finalPos = [];
                    let res = await doc.doc_save({ ...this.form, uid: this.user._id });
                    data = res.data;
                }
                this.loading = false;
                if (data.errorCode === 0) {
                    if (decide == 'approve') {
                        let _doc = {};
                        if (checkVal == edit) {
                            _doc = {
                                docId: this.form._id,
                                docName: this.form.name
                            }
                        } else {
                            _doc = {
                                docId: data.data._id,
                                docName: data.data.name
                            }
                        }
                        await this.subApprove(_doc, formData);
                    } else {
                        this.$message.success(this.$t('files.addTemp.msg.success'));
                        this.$router.go(-1);
                    }
                }
            } catch (error) {
                //解决 添加不成功时, ui样式 因为 boolean 转换成 数字 bug.
                this.form.signatoryType = !!this.form.signatoryType;
                this.loading = false;
            }
        },
        async subApprove({ docId, docName }, form) {// 发起审查
            const formData = {
                docId,
                docName,
                reqOptType: !this.contractType[this.form.category - 1].isSave ? ReqOptTypeEnum.seal : ReqOptTypeEnum.keep,
                ...form
            }
            try {
                let { data } = await doc.doc_ignite(formData);
                if (data.errorCode == 0) {
                    this.$message.success(this.$t('files.addTemp.msg.success'));
                    this.$router.go(-1);
                }
            } catch (error) {

            }
        },
        async validate(e) {// 表单校验
            //判断是否设计金额
            if (!this.noPrice) {//不涉及金额
                this.form = omit(this.form, ['currencyUnit', 'contractMoney', 'applyMoney'])
            }
            if (!this.noContract) {//没有签约方
                this.form = omit(this.form, ['signatoryType', 'signatory', 'format'])
            }
            const { currencyUnit, contractMoney, applyMoney, description, startTime_, endTime_, category, decisiveNo, decisiveLink, sealTypes, signatory, signatoryType } = this.form;
            const { incomeContract, exportContract } = CategoryEnum;
            //如果类别为 收入或支出合同时，DF可决号为必填项
            //年后又添加的 => 选择"收入合同"和"支出合同"时，【合同期间】，【DF可决号】，【DF连接】，【签约方】请设定为必填项。且需要盖章类型请默认勾选【公司合同章】。
            if ([incomeContract, exportContract].includes(category)) {
                if(!decisiveNo) {
                    console.log("DF可决号必填")
                    this.$message.warning(this.$t('files.addTemp.msg.warningDF'));
                    return;
                }
                if(!decisiveLink) {
                    this.$message.warning('DF链接不能为空');
                    return;
                }
                if(!signatory) {
                    this.$message.warning('签约方不能为空');
                    return;
                }
            }
            //申请盖章下 盖章类型必填
            if (this.checkBoxs.length > 0 && sealTypes.length == 0) {
                this.$message.warning(this.$t('files.addTemp.msg.warningSealTypes'));
                return;
            }

            if (contractMoney || applyMoney) {
                if (!currencyUnit) {
                    this.$message.warning(this.$t('files.addTemp.msg.warningUnit'))//请选择货币单位
                    return;
                }
            }
            //如果合同金额和DF金额都填写了，并且不一致的情况下，说明文当则必填
            if (contractMoney && applyMoney) {
                if (contractMoney != applyMoney && !description) {
                    this.$message.warning(this.$t('files.addTemp.msg.warningDesc'))//请填写文档说明
                    return;
                }
            }
            if (!this.isDf) {
                this.form = omit(this.form, ['decisiveNo', 'decisiveLink'])
            }
            //校验 合同期间
            if (startTime_ && endTime_ && startTime_ <= endTime_) {
                this.form.startTime = startTime_ + ' 00:00:00';
                this.form.endTime = endTime_ + ' 23:59:59';
            } else if (startTime_ || endTime_) {
                this.$message.warning(this.$t('files.addTemp.msg.warningDate'))//请选择正确的期间
                return;
            } else {
                //校验 如果文档类型不为 支出 和 收入 时， 合同期间非必填 
                const requireds = [incomeContract, exportContract];
                if (requireds.indexOf(category) != -1) {
                    this.$message.warning(this.$t('files.addTemp.msg.warningDate'))//请选择正确的期间
                    return;
                } else {
                    this.form.startTime = null;
                    this.form.endTime = null;
                }
            }

            this.$refs.form.validate(valid => {
                if (valid) {
                    //格式化 集团签约方 为 number
                    this.form.signatoryType = ~~signatoryType;
                    if (e == 'save') {
                        this.submit();
                    } else {
                        this.approveHandler();
                    }
                }
            });
        },
        clearValidate() { // 清除表单校验
            this.$refs.form.clearValidate();
        },
        uploadSuccess({ data, errorCode }, file, fileList) { // 附件上传成功时 回调
            if (errorCode === 0) {
                const _uploadPos = { timestamp: moment().format("YYYY-MM-DD HH:mm:ss"), ...data };
                console.log(_uploadPos, '_uploadPos')
                this.form.uploadPos.push(_uploadPos);
            }
        },
        closeUpload(e, i) { // 删除上传的附件
            this.form.uploadPos.splice(i, 1);
        },
        async initSelectList() { // 初始化 文档类别 、文档类型的 下拉数据列表
            this.typeList = _.cloneDeep(this.contractType);
            this.childTypeList = _.cloneDeep(this.childContractType);
        },
    },
    created() {// 钩子 、页面加载前触发
        const { add, copy } = RouterToEnum;
        this.approveBtn = this.approveBtnCache[0];
        this.initSelectList();
        //判断是否刷新页面，当edit 或者 copy 时
        if (this.$route.query.to != add && !this.queryParams.name) {
            this.$router.go(-1);
            return;
        }
        if (this.$route.query.to != add) {
            /**格式化子类型列表范围 */
            let { category, type, sealTypes, startTime, endTime, pagingSeal, signatoryType } = this.queryParams;
            //合同期间
            this.dateRoundShow(category);
            let obj = {};
            for (const id in this.childContractType) {
                if (Math.floor(id / 10) === category) {
                    obj[id] = this.childContractType[id];
                }
            }
            this.childTypeList = obj;
            let fileDate = {
                startTime_: startTime && moment(startTime).format("YYYY-MM-DD"),
                endTime_: endTime && moment(endTime).format("YYYY-MM-DD")
            }
            this.form = { ...this.queryParams, ...fileDate, type: this.queryParams.type + '' };
            //格式化 集团签约方checkbox 为boolean
            if (signatoryType || signatoryType == 0) {
                this.form.signatoryType = !!signatoryType;
            }
            //是否涉及金额
            this.noPrice = !this.contractType[category - 1].noPrice;
            //是否 显示签约方
            this.noContract = !this.contractType[category - 1].noContract;
            this.getSealType(type, 'created');
            this.form.sealTypes = sealTypes;
            //设置审批参数
            this.approveObj.reqOptType = !this.contractType[category - 1].isSave ? ReqOptTypeEnum.seal : ReqOptTypeEnum.keep;
            this.approveObj.documentCategory = category;
            //申请保管 or 申请盖章
            this.approveBtn = !this.contractType[category - 1].isSave ? this.approveBtnCache[0] : this.approveBtnCache[1];
        }
        if (this.$route.query.to == copy) {
            this.form.name = '';
            this.form.state = StateEnum.making;
            this.form.uploadPos = [];
        }
    }
}
</script>
```



##### approve/index.vue

```javascript
<script>
import { mapGetters } from 'vuex';
import moment from 'moment'; // moment.js 日期工具
import { Switch as vswitch } from '@/components'; // switch 选择器 组件
import { igniteDataHandler } from '@/mixins'; // mixins
import { ResOptType as ResOptTypeEnum } from '@/utils/Enum/FEnum'; // 服务端枚举

export default {
    data() {
        return {
            isShow: false, // 是否展示
            form: { emergency: false }, // 表单数据
            isKeepLines: false, // 是否 盖章需多人确认
            auditData: []
        }
    },
    mixins: [igniteDataHandler], // mixins
    watch: {// 监听 data 属性 改变
        isShow(bool) {
            if (!bool) {
                this.$emit('tempDestroyed', !!this.approveObj.signatoryType)
            }
        }
    },
    components: {// 组件注册
        vswitch
    },
    computed: {
        ...mapGetters(['user']),
    },
    props: {// 父组件传参
        approveLines: {
            type: Object,
            default() { }
        },
        approveObj: {
            type: Object,
            default() { }
        },
        auditList: {
            type: Array,
            default() { }
        },
        defaultAudits: {
            type: Object,
            default() { }
        }
    },
    methods: {
        changeMore(bool) {// 是否多人确认 选择时 触发
            this.$emit("approveHandler");
        },
        async subApprove() {// 提交审核
            const today = moment().format("YYYY-MM-DD");
            if (this.form.date && this.form.date < today) {
                this.$message.warning(this.$t('files.subAppTemp.msg.warningDate'));//纳期日小于当前日期
                return;
            }
            if (this.approveLines.auditLines.length == 0) {
                this.$message.warning(this.$t('files.subAppTemp.msg.warningNull'));//暂未获得文书审查路径
                return;
            }
            const auditLines = await this.getFormData(this.defaultAudits, this.auditList);
            this.approveLines.auditLines = auditLines;
            const data = await this.getDefaultFormData();
            this.isShow = false;
            this.$emit('submit', 'approve', data);
        },
        async getFormData(defaultIds, auditList) {// 格式化数据格式
            let auditLines = []
            for (const key in defaultIds) {
                if (defaultIds.hasOwnProperty(key)) {
                    const id = defaultIds[key];
                    for (let j = 0; j < auditList[key].length; j++) {
                        let user = auditList[key][j];
                        if (id == user._id) {
                            auditLines.push({
                                uid: user._id,
                                name: user.name,
                                resOptType: ResOptTypeEnum.audit,
                                deptId: user.deptId
                            })
                        }
                    }
                }
            }
            return auditLines;
        },
    }
}
</script>
```



![](..\..\images\files_search.jpg)



##### searchTemp/index.vue

```javascript
<script>
import { doc } from '@/api'; // 接口文件
import { fileI18nHandler } from '@/mixins'; // mixins 国际化文件
import { omit, verifyDateRange } from '@/utils'; // 自定义工具
import _ from 'lodash'; // lodash 工具

export default {
    data() {
        return {
            forDeptList: [], //所属部门列表
            typeList: [], // 文档类别 列表
            childTypeList: [], // 文档类型 列表
            signList: [], // 签约方 列表
            tagList: [], // 标签 列表
            currency: [], // 货币单位 列表
        }
    },
    props: {// 父组件 传参
        query: {
            type: Object,
            default() { }
        },
        isAllDate: {
            type: Boolean,
            default: false
        },
        isOperate: {
            type: Boolean,
            default: true
        },
    },
    mixins: [fileI18nHandler], // mixins
    watch: { // 监听 data 属性 变化
        contractType() {
            this.initSelectList();
        },
    },
    computed: {
        docState() {
            return require("./conf")['docState' + this.$t('lang.type')]
        },
        expireList() {
            return require("./conf")['expireStatus' + this.$t('lang.type')]
        }
    },
    methods: {
        searchChange(key, e) { // 该函数主要用来 判断 查询条件是否为空， 移除 key/value
            if (!e) delete this.query[key];
        },
        search() {// 点击查询
            let verify = verifyDateRange(this.query);
            if (!verify) {
                this.$message.warning(this.$t('files.search.warning'))//请选择正确的期间
                return;
            }
            this.$emit("search");
        },
        contractTypeHandler(e) {// 文档类别 选择时
            if (!e) {
                this.childTypeList = _.cloneDeep(this.childContractType);
                this.query = omit(this.query, ['type', 'category'])
                return;
            }
            let obj = {};
            for (const id in this.childContractType) {
                if (Math.floor(id / 10) === e) {
                    obj[id] = this.childContractType[id];
                }
            }
            this.childTypeList = obj;
            this.query.type = Object.keys(obj)[0];
        },
        childContractTypeHandler(e) {// 文档类型 选择时
            if (!e) {
                this.typeList = _.cloneDeep(this.contractType);
                delete this.query.type;
                return;
            }
            let obj = {};
            for (const id in this.childContractType) {
                if (Math.floor(id / 10) === Math.floor(e / 10)) {
                    obj[id] = this.childContractType[id];
                }
            }
            this.childTypeList = obj;
            this.query.category = Math.floor(e / 10);
        },
        async initSelectList() {// 初始化 下拉选择器 数据列表
            this.typeList = this.contractType;
            this.childTypeList = this.childContractType;
            this.tagList = await this.initTags();
            this.currency = this.currencyUnit;
            //所属部门列表
            this.forDeptList = await this.initDeptList();
            //获取 签约方
            this.signList = await this.initSign();
        },
        async initTags() {//获取 存档标签
            let { data } = await doc.doc_tags();
            return data.data;
        },
        async initSign() {// 获取 签约方
            let { data } = await doc.doc_signatory();
            return data.data.map(item => {
                return { name: item._id }
            });
        },
        async initDeptList() {//初始化部门列表
            let { data } = await doc.doc_queryDept();
            return data.data.map(item => {
                return {
                    value: item._id,
                    label: item.name
                }
            })
        },
    },
    created() {// 钩子、页面加载前触发
        this.initSelectList();
    }
}
</script>
```



##### lendTemp/index.vue

```javascript
<script>
import { mapGetters } from 'vuex';
import { 
    ReqOptType as ReqOptTypeEnum, 
    RecordType as RecordTypeEnum,
    ResOptType as ResOptTypeEnum
} from '@/utils/Enum/FEnum';// 服务端枚举
import { doc } from '@/api';// 接口文件
import { uploadHandler, fileI18nHandler } from '@/mixins/';// mixins
import approveTemp from './approve';// 借入/借出模板 组件
import { LocalStorageEnum as LSEnum, RouterToEnum } from '@/utils/Enum/VEnum';// 前端枚举
import _ from 'lodash'; // lodash 工具
import { fmtDate } from '@/filters'; // 过滤器
import moment from "moment"; // moment.js 日期工具

export default {
    components: { // 组件注册
        approveTemp
    },
    filters: { fmtDate }, // 过滤器注册
    mixins: [uploadHandler, fileI18nHandler], // mixins
    data() {
        return {
            RouterToEnum, // 枚举文件 声明 用于 Dom使用
            headers: { token: localStorage.getItem(LSEnum.token) }, // 上传文件 头部
            form: {}, // 表单数据
            checkBoxActive: 0,
            approveLines: {}, // 审查路径
            approveObj: { reqOptType: null, documentCategory: null, multiple: false }, //审批参数
            approveUser: [], // 履历
            lendData: [],//出借模板需要的数据
            tagList: [],//存档标签
        }
    },
    computed: {
        ...mapGetters([
            'queryParams',
            'user'
        ]),
        btnName() {// 按钮名称
            const { resSave, final, reqSave, lend } = RouterToEnum;
            const btnName = {
                [resSave]: this.$t('files.lend.btn.save'),
                [lend]: this.$t('files.lend.btn.lend'),
                [reqSave]: this.$t('files.lend.btn.approveSave'),
                [final]: this.$t('files.lend.btn.final'),
            }
            return btnName[this.$route.query.to]
        },
        title() {// 小标题
            const { resSave, reqSave } = RouterToEnum;
            if (this.$route.query.to == resSave || reqSave) {
                return this.$t('files.lend.title.save')
            } else {
                return this.$t('files.lend.title.lend')
            }
        },
        uploadData() {// 上传 data 数据
            return { storage: 1, documentCategory: this.form.category };
        },
        optType() {// 类型配置文件
            const { lend, back } = ResOptTypeEnum;
            return { [lend]: this.$t('files.lend.optType')[0], [back]: this.$t('files.lend.optType')[1] }
        }
    },
    methods: {
        back() {// 返回上一层
            this.$router.go(-1);
        },
        closeUpload(e, i) {// 删除上传附件
            this.form.finalPos.splice(i, 1);
        },
        async downLoad(file) {// 下载
            window.location.href = `/api/doc/download?storagePath=${file.storagePath}&name=${file.fileName}`;
        },
        uploadSuccess({ data, errorCode }, file, fileList) {// 上传成功时 回调
            if (errorCode === 0) {
                const _finalPos = { timestamp: moment().format("YYYY-MM-DD HH:mm:ss"), ...data };
                console.log(_finalPos, '_finalPos')
                this.form.finalPos.push(_finalPos);
            }
        },
        async validate() { // 表单校验
            const { final, reqSave, lend } = RouterToEnum;
            if (this.$route.query.to == final) {//提交最终存档
                this.subFinal();
                return;
            }
            if (this.$route.query.to == reqSave) {//发起请求存档
                this.subArchive();
                return;
            }
            if (this.$route.query.to == lend) {//出借归还
                this.$refs.approve.init();
                this.$refs.approve.isShow = true;
                return;
            }
            this.$refs.form.validate(valid => {//最终存档处理
                if (valid) {
                    this.subSave();
                }
            });
        },
        async subLend() { // 表单提交 出借/归还
            const { form, selectData } = this.$refs.approve;
            // let formData = {lentUid: form.lentUid, ...roundDate};
            try {
                const { _id } = this.queryParams;
                this.loading = true;
                let { data } = await doc.doc_approve(_id, form.type, { lentBody: form });
                this.loading = false;
                if (data.errorCode == 0) {
                    this.$message.success(this.$t('files.lend.success'));
                    this.$router.go(-1);
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async subSave() {//最终存档处理
            try {
                const { archiveDir, tagId, archiveEndTime } = this.form;
                const { _id, resOptType } = this.queryParams;
                this.loading = true;
                let formData = { archiveEndTime };
                if (archiveDir) {
                    formData.archiveDir = archiveDir;
                }
                if (tagId) {
                    formData.tagId = tagId
                }
                let { data } = await doc.doc_approve(_id, resOptType, formData);
                this.loading = false;
                if (data.errorCode == 0) {
                    this.$message.success(this.$t('files.lend.success'));
                    this.$router.go(-1);
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async subArchive() {//发起请求存档
            let list;
            try {
                let { category } = this.queryParams;
                let { data } = await doc.doc_process({ reqOptType: ReqOptTypeEnum.archive, documentCategory: category });
                if (data.errorCode === 0) {
                    let { archiveLines } = data.data;
                    list = archiveLines;
                }
            } catch ({ errorCode, errorMsg }) {

            }
            let archiveList = list.map(item => {
                let cache = {};
                if (item.agentUid) {
                    cache = { resUid: item.agentUid, resName: item.agentName }
                } else {
                    cache = { resUid: item.uid, resName: item.name }
                }
                return { ...cache, resDeptId: item.deptId, resOptType: item.resOptType }
            })
            let paths = archiveList;
            let form = _.cloneDeep(this.form);
            form = _.omit(form, ['_id', '__v', 'createTime', 'uid'])
            const { _id, name, deptId } = this.user;
            let data = {
                reqUid: _id,
                reqName: name,
                reqDeptId: deptId,
                reqDeptName: localStorage.getItem(LSEnum.deptName),
            }
            const formData = {
                docId: this.form._id,
                docName: this.form.name,
                reqOptType: !this.contractType[this.form.category - 1].isSave ? ReqOptTypeEnum.seal : ReqOptTypeEnum.keep,
                ...form,
                paths,
                ...data
            }
            try {
                let { data } = await doc.doc_ignite(formData);
                if (data.errorCode == 0) {
                    this.$message.success(this.$t('files.lend.success'));
                    this.$router.go(-1);
                }
            } catch (error) {

            }
        },
        async subFinal() {//单独提交最终存档
            const { _id: docId, finalPos, deptId } = this.form;
            if (!Array.isArray(finalPos) || finalPos.length === 0) {
                this.$message.warning('文件未上传')
                return;
            }
            try {
                this.loading = true;
                const { data } = await doc.doc_final({ docId, finalPos, deptId });
                this.loading = false;
                if (data.errorCode == 0) {
                    this.$message.success(this.$t('files.lend.success'));
                    this.$router.go(-1);
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async initProcess() {// 初始化 审查数据
            let { category, type, sealTypes } = this.queryParams;
            //设置审批参数
            this.approveObj.reqOptType = !this.contractType[category - 1].isSave ? ReqOptTypeEnum.seal : ReqOptTypeEnum.keep;
            this.approveObj.documentCategory = category;
            try {
                let { data } = await doc.doc_process(this.approveObj);
                if (data.errorCode === 0) {
                    this.approveLines = data.data;
                }
            } catch ({ errorCode, errorMsg }) {

            }
        },
        initList() {// 初始化 表单数据
            this.form = this.queryParams;
        },
        async initRecords(type) {//查询 审核记录 或 出借记录
            try {
                const { _id } = this.queryParams;
                let { data } = await doc.doc_records(_id, type);
                if (data.errorCode == 0) {
                    return data.data;
                }
            } catch (error) {

            }
        },
        async initTags() {// 初始化 标签
            try {
                const { data } = await doc.doc_tags();
                this.tagList = data.data;
            } catch (error) { }
        }
    },
    async created() {//钩子、页面加载前 触发
        //判断是否刷新页面
        if (!this.queryParams.name) {
            this.$router.go(-1);
            return;
        }
        this.initList();
        this.approveUser = await this.initRecords(RecordTypeEnum.review);//查询审核记录
        const { resSave, lend } = RouterToEnum;
        if (this.$route.query.to == lend) {
            this.lendData = await this.initRecords(RecordTypeEnum.lend);//查询出借记录
        } else {
            this.initProcess();
        }
        if (this.$route.query.to == resSave) {
            this.lendData = await this.initTags();
        }
    }
}
</script>
```



##### lendTemp/approve/index.vue

> 出借/归还 模板组件



```javascript
<script>
import moment from 'moment'; // moment.js 日期工具
import { user_by_dept } from '@/api/user'; // 接口文件
import { cascaderHandler } from '@/mixins'; // mixins
import { ResOptType as ResOptTypeEnum } from '@/utils/Enum/FEnum'; // 服务端枚举

export default {
    data() {
        return {
            isShow: false, // 是否展示
            elDisable: false, //判断状态 为出借 还是 归还，归还 为 true
            form: { type: ResOptTypeEnum.lend }, // 表单数据
            userList: [], // 出借人 列表
        }
    },
    mixins: [cascaderHandler], // mixins
    props: {// 父组件 传参
        formData: {
            type: Array,
            default() { }
        }
    },
    computed: {
        typeList() {
            const { lend, back } = ResOptTypeEnum;
            return [//lend: 6 /**去出借 */, back: 7 /**去归还 */,
                {
                    label: this.$t('files.lend.temp.typeList')[0],
                    value: lend
                },
                {
                    label: this.$t('files.lend.temp.typeList')[1],
                    value: back
                }
            ]
        }
    },
    methods: {
        async validate() {// 表单校验
            //校验 日期
            if (this.form.startTime_ && this.form.endTime_ && this.form.startTime_ <= this.form.endTime_) {
                this.form.startTime = this.form.startTime_ + ' 00:00:00';
                this.form.endTime = this.form.endTime_ + ' 23:59:59';
            } else {
                this.$message.warning(this.$t('files.lend.temp.warning'))//请选择正确的日期
                return;
            }
            this.$refs.form.validate(async valid => {
                if (valid) {
                    let today = moment().format("YYYY-MM-DD");
                    if (this.elDisable) {
                        this.$emit("submit");
                    } else {
                        if (this.form.startTime_ < today) {
                            try {
                                let res = await this.$confirm(this.$t('files.lend.temp.confirm'))//出借时间小于当前日期，是否确认？
                                if (res == 'confirm') {
                                    this.$emit("submit");
                                }
                            } catch (error) { }
                        } else {
                            this.$emit("submit");
                        }
                    }
                }
            });
        },
        async findUserByDeptId(deptId) {// 根据部门id查询用户列表
            try {
                let { data } = await user_by_dept(deptId);
                if (data.errorCode == 0) {
                    this.userList = data.data;
                }
            } catch (error) { }
        },
        init() {// 初始化 数据。
            const { lend, back } = ResOptTypeEnum;
            this.form = { type: lend };
            this.elDisable = false;
            if (this.formData && this.formData.length != 0 && this.formData[this.formData.length - 1].resOptType == lend) {
                const data = this.formData[this.formData.length - 1];
                this.elDisable = true;
                this.form.startTime_ = moment(data.startTime).format("YYYY-MM-DD");
                this.form.endTime_ = moment(data.endTime).format("YYYY-MM-DD");
                this.form.type = back;
                this.form.lentUid = data.reqUid;
                this.form.name = data.reqName;
            }
        }
    }
}
</script>
```

