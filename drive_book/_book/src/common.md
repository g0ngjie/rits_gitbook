### 公共包

> 公共包 拦截器、api请求工具、ant按需加载集等

##### frame

考虑性能原因，*ant-design-vue* 组件按需加载文件。如果希望用其他功能，只需要在 *components-ant.ts*文件里配置。

```typescript
import Vue from 'vue'
import Ant from 'ant-design-vue'
...

Vue.use(LocaleProvider)
Vue.use(Pagination)
Vue.use(Layout)
Vue.use(Input)
Vue.use(InputNumber)
Vue.use(Button)
...

Vue.prototype.$confirm = Modal.confirm
...
```



##### AntSelectConfig.ts

下拉框静态数据统一配置文件。

```typescript
export const selectColumns = {
  driverType: [
    {
      label: "直打",
      value: direct
    },
    {
      label: "漫游",
      value: roaming
    },
    {
      label: "其他",
      value: other
    }
  ],
  ...
}
```



##### Enum2Val.ts

枚举类型对应结果集

```typescript
import { OsType, DriverType, CpuBits, UpgradeResult } from "@/utils/VEnum";
...

/* 升级结果 */
export const Result2Val: any = {
  [UpgradeResult.Error]: "失败",
  [UpgradeResult.Success]: "成功"
};
...
```



##### FormItemLayout.ts

ant表单组件统一样式配置（此处也是考虑ant默认配置样式的格式比较丑陋，而且很多地方重复定义。）

```typescript
export default {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 5 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 }
  }
};
```



##### interceptor.ts

全局拦截器，思路和 *合同管理一样*

```typescript
import router from "@/router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { Route } from "vue-router";
import { getModule } from "vuex-module-decorators";
import User from "@/store/modules/user";
import Store from "@/store";
import { RouterPath } from "@/utils/VEnum";

NProgress.configure({ showSpinner: false });

const whiteList = [
  RouterPath.Login.toString(),
  RouterPath.InitAdmin.toString()
];

const UserModule = getModule(User, Store);

router.beforeEach(async (to: Route, _: Route, next: any) => {
  NProgress.start();
  if (UserModule.token) {
    if (to.path === RouterPath.Login) {
      next({ path: RouterPath.Home });
      NProgress.done();
    } else {
      next();
    }
  } else {
    if (whiteList.includes(to.path)) {
      next();
    } else {
      next(RouterPath.Login);
      NProgress.done();
    }
  }
});

router.afterEach((to: Route) => {
  NProgress.done();
});
```



##### request.ts

api请求工具。接口异常拦截。

```typescript
import axios from "axios";
import Ant from "ant-design-vue";
const { message } = Ant;
import Error from "@/utils/Error";
import { LocalStorage as LocalEnum } from "@/utils/VEnum";

const service = axios.create({
  timeout: 5000
});

// request interceptor
service.interceptors.request.use(
  config => {
    // Do something before request is sent
    const token = localStorage.getItem(LocalEnum.token.toString()) || "";
    if (token) {
      config.headers["token"] = token;
    }
    return config;
  },
  error => {
    Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  response => response,
  error => {
    const { status } = error.response;
    const { errNo, msg } = error.response.data;
    const err: any = Error;
    message.error(err[errNo] || "系统异常");
    return Promise.reject(error.response.data);
  }
);

export default service;
```

