### 工具包



##### index.js

自定义工具包主文件

```javascript
/**
 * 工具栏 搜索
 * @param {*} listQuery 搜索参数 
 * @param {*} keyWords 关键词查询，针对时间区间查询
 */
export function searchTool(listQuery, keyWords=null){
  let query = {}
  if(!isNull(keyWords)){//删除关键词的key
    for (let i = 0; i < keyWords.names.length; i++) {
      let name = keyWords.names[i];
      delete listQuery[name]
    }
  }
  for (let key in listQuery) {
    if (listQuery.hasOwnProperty(key)) {
      let element = listQuery[key];
      if (!isNull(element)) {
        query[key] = element
      }
      if (!isNull(keyWords) && key == keyWords.key && !isNull(element)) {
        let formate = {}
        if(element.constructor === String){
          [formate[keyWords.names[0]], formate[keyWords.names[1]]] = [element + ' 00:00:00', element + ' 23:59:59']
        }else if(element.constructor === Array){
          [formate[keyWords.names[0]], formate[keyWords.names[1]]] = [element[0] + ' 00:00:00', element[1] + ' 23:59:59']
        }
        let data = Object.assign({}, formate, query);
        query = data;
      }
    }
  }
  return query;
}
...
```



##### Enum/FEnum

服务端枚举定义

```javascript
/**
 * 用户角色
 * enum for UserRole
 * @readonly
 * @enum {Number}
 */
exports.UserRole = {
  normal: 0 /**普通用户 */,
  auditor: 1 /**文书审查担当 */,
  sealer: 2 /**盖章担当 */,
  taxer: 3 /**免税处理担当 */,
  archiver: 4 /**存档处理担当 */,
  bscSealer: 5 /**bsc部门合同担当 */,
  businesser: 6 /**事业部合同担当 */,
  sealSaver: 8 /**压印担当 */
};
```



##### Enum/VEnum

前端枚举定义

```javascript
/**
 * 服务端 用户等级
 */
exports.SUserLevelEnum = {
    class: 4, /* 课长 */
    dept: 3, /* 部长 */
    center: 2, /* 中心长 */
    boss: 1, /* 总经理 */
}

/**
 * 客户端 等级对应 用户
 */
exports.CUserLevelEnum = {
    class: 1, /* 课长 */
    dept: 2, /* 部长 */
    center: 3, /* 中心长 */
    boss: 4, /* 总经理 */
}
```



##### i18n.js

国际化菜单转义文件

```javascript
// translate router.meta.title, be used in breadcrumb sidebar tagsview
export function generateTitle(title) {
  const hasKey = this.$te('route.' + title)

  if (hasKey) {
    // $t :this method from vue-i18n, inject in @/lang/index.js
    const translatedTitle = this.$t('route.' + title)

    return translatedTitle
  }
  return title
}
```



##### request.js

api请求工具。接口异常拦截。

```javascript
import axios from 'axios'
import { Message, MessageBox } from 'element-ui'
import store from '@/store'
import i18n from '@/lang'
import router from '@/router';

// create an axios instance
const service = axios.create({
  timeout: 5000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    config.headers['token'] = localStorage.getItem("token")
    return config
  },
  error => {
    Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use( response => response , error => {
    const { status } = error.response;
    let { errorCode, errorMsg } = error.response.data;
    if(status == 500){
      Message.error(i18n.t("request.500"))
      return;
    }
    if(status == 504){
      Message.error(i18n.t("request.504"))
      return;
    }
    if (errorCode === 2017) {
      MessageBox.alert(i18n.t("request.2017.msg"), i18n.t("request.2017.title"), {
        confirmButtonText: i18n.t("request.2017.btn"),
        type: 'warning', showClose: false
      }).then(() => {
        localStorage.removeItem("token");
        location.href = '/';
        return;
      })
      return;
    }
    if (errorCode === 2042) {
      MessageBox.alert(i18n.t(`error[${errorCode}]`), { type: 'warning', showClose: false }).then(() => {
        localStorage.removeItem("token");
        location.href = '/';
        return;
      })
      return;
    }
    if (errorCode === 2005) {
      MessageBox.alert(i18n.t(`error[${errorCode}]`), {
        type: 'warning', showClose: false
      }).then(() => {
        localStorage.removeItem("token");
        router.push("/")
        return;
      })
      return;
    }
    Message({
      message: i18n.t(`error[${errorCode}]`),
      type: 'error',
      duration: 5 * 1000
    })
    //debug
    console.log('>> errorCode：', errorCode, ',errorMsg：', errorMsg , '<<')
    return Promise.reject(error.response.data)
  }
)

export default service
```



##### validate.js

表单校验文件，无需再更改

```javascript
  ...

  /**
   * 参数 item 
   * required true  必填项
   * maxLength  字符串的最大长度
   * min 和 max 必须同时给 min < max  type=number
   * type 手机号 mobile
   *      邮箱   email
   *      网址   url 
   *      各种自定义类型   定义在 src/utils/validate 中    持续添加中.......
   * */

  Vue.prototype.filter_rules = function (item){
    let rules = [];
    if(item.required){
      rules.push({ required: true, message: i18n.t('validate.required'), trigger: ['blur', 'change'] });
    }
    if(item.maxLength){
      rules.push({ min:1,max:item.maxLength, message: i18n.t('validate.maxLength.start')+item.maxLength+i18n.t('validate.maxLength.end'), trigger: 'blur' })
    }
    if(item.min&&item.max){       
      rules.push({ min:item.min,max:item.max, message: i18n.t('validate.around.start')+item.min+i18n.t('validate.around.center')+item.max+i18n.t('validate.around.end'), trigger: 'blur' })
    }
    if(item.type){
      let type = item.type;
      switch(type) {
        case 'email':
            rules.push({ type: 'email', message: i18n.t('validate.email'), trigger: ['blur', 'change']  });
            break;
        case 'upload':
            rules.push( { required: true, message: i18n.t('validate.upload'), trigger: 'change' });
            break;
        case 'mobile':
            rules.push( { validator: isvalidateMobile, trigger: ['blur', 'change'] });
            break;    
        case 'number':
            rules.push( { validator: isvalidateInteger, trigger: 'blur' });
            break;
        case 'price':
            rules.push( { validator: isPrice, trigger: 'change' });
            break;
        case 'port':
              rules.push( { required: true, validator: isPort, trigger: 'change' });
              break;
        case 'account': //用户名组成规则
              rules.push( { required: true, validator: validAccount, trigger: 'blur' });
              break;
        case 'password': //密码组成规则
              rules.push( { required: true, validator: validPass, trigger: 'blur' });
              break;
        case 'isEmpty': //非空校验
              rules.push( { required: true, validator: isEmpty, trigger: ['blur', 'change'] });
              break;;
        case 'rate': //汇率设定，保留两位小数
          rules.push( { validator: isRate, trigger: 'change' });
          break;
        default:
            rule.push({});
            break;
      }
    }
    return rules;
  }
}
```



##### webSocket.js

webcoket文件，用于头像获取使用

```javascript
export default function (avatar, callback, error) {
  /* 封装 WebSocket 实例化的方法  */
  let CreateWebSocket = (function() {
    return function(urlValue) {
      if (window.WebSocket) return new WebSocket(urlValue);
      if (window.MozWebSocket) return new MozWebSocket(urlValue);
      return false;
    };
  })();
  /* 实例化 WebSocket 连接对象, 地址为 ws 协议 */
  let webSocket = CreateWebSocket(`ws://${location.host}/ws/peer/avatar`);
  /* 接收到服务端的消息时 */
  webSocket.onmessage = msg => {
    // console.log("服务端说:" + msg.data);
    if (msg && msg.data) {
      if (msg.data != 'false') {
        callback({data: msg.data, ws: webSocket});
      } else {
        let wsData = {
          token: localStorage.getItem("token"),
          path: avatar
        }
        webSocket.send(JSON.stringify(wsData));
      }
    }
  };
  /* 关闭时 */
  webSocket.onclose = () => {
    console.log("关闭连接");
  };
  webSocket.onerror = (err) => {
    error(err)
  }
  /* 发送消息 */
  // webSocket.send(str);
  /* 关闭消息 */
  // webSocket.close();
}
```

