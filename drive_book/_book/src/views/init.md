### 管理员创建页面



1. 默认系统管理员创建账户页面。用户添加成功后，自动跳转到登录页面。
2. 用户添加成功后，自动跳转到登录页面。

![](..\..\images\init.jpg)



```typescript
<script lang="ts">
import { Component, Vue } from "vue-property-decorator"; //装饰器引入
import { user_admin } from "@/api/user"; //接口文件
import { RouterPath } from "@/utils/VEnum"; //路由枚举
import formItemLayout from "@/common/FormItemLayout"; //formItem公用样式

@Component({ name: "Init" }) //组件声明 name 可有可无，目前暂未用到name属性，但组件之间，name值不可以重复定义
export default class Init extends Vue {
  private form: any = {}; //表单值存储
  private loading: boolean = false; //loading
  private formItemLayout: any = formItemLayout; //样式赋值

  created() { //vue钩子，页面加载前
      /**
      此处需要声明一下。 ant由于是单向数据流，和element不同点这里体现
      需要通过 this.$form.createForm(this) 初始化 form表单需要的东西。
      声明的 key值具体看 当前页面的 Dom声明。
      */
    this.form = this.$form.createForm(this);
  }

  handleSubmit() {//表单提交函数
    //表单校验 注意，由于用的是TS，err: any, value:any 必须要在后面声明类型。
    this.form.validateFields((err: any, values: any) => { 
      if (!err) {
        this.submit(values);//校验通过后提交
      } else {
        return false;
      }
    });
  }

  async submit(form: any) {// 提交函数
    this.loading = true;
    try {
      const { data } = await user_admin(form); //服务端登录接口
      this.loading = false;
      if (data.errNo == 0) {
        this.$message.success("创建成功");
        setTimeout(() => {
          window.location.href = RouterPath.Home; //页面跳转到登录页面。
        }, 1000);
      }
    } catch (error) {
      this.loading = false;
    }
  }
}
</script>
```

