### api 接口文档目录

> 接口调用统一配置文件。
>
> 结构要求语义化，根据服务端接口声明。
>
> 结构清晰。排查问题的话，对应接口会比较方便。



##### dept.js

部门 接口

```javascript
import request from '@/utils/request'

//搜索叶子部门(分页模糊搜索)
export function dept_page(query) {
    return request({
        url: '/api/dept',
        method: 'get',
        params: query
    })
}
...
...
```



##### doc.js

文档 接口

```javascript
//搜索文档(文档列表部分，除我的文档)
export function doc_page(data) {
    return request({
        url: '/api/doc/docs',
        method: 'post',
        data
    })
}
...
...
```



##### operator.js

用户管理 接口

```javascript
//获取用户详情
export function operator_page(query) {
    return request({
        url: '/api/operator',
        method: 'get',
        params: query
    })
}
...
...
```



##### printing.js

用印规则 接口

```javascript
//获取用印规则
export function printing_page(query) {
  return request({
      url: '/api/rule',
      method: 'get',
      params: query
  })
}
...
...
```



##### rule.js

审批规则 接口

```javascript
//获取审批规则
export function rule_info_get(query){
  return request({
    url:'api/rule',
    method:'get',
    data:query
  })
}
...
...
```



##### system.js

系统设定 接口

```javascript
export function system_add(data) {
    return request({
        url: '/api/system',
        method: 'post',
        data
    })
}
...
...
```



##### user.js

用户登录、修改密码 等接口

```javascript
//登录
export function user_login(data) {
    return request({
        url: '/api/user/login',
        method: 'post',
        data
    })
}
...
...
```

