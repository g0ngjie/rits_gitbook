### 多继承

> Vue提供一套 对于 *script* 标签里存在重复定义的 *钩子函数*、*属性* 等，提供了一个抽象的定义 *mixins*。
>
> 类似父类。对于重复的定义，统一管理，减少冗余。提高代码的简洁性和可阅读性。
>
> <em style="color: #F56C6C">使用mixins切记重名情况。mixins在不同情况下，重名会出现复写，或者覆盖的情况，具体详情，参考官方文档</em>



##### cascaderHandler.js

由于element默认的 *级联选择器* 会把全路径展示出来。

此功能优化了用户操作。该功能高度封装。具体使用结合目前案例即可。

![](..\images\cascaderHandler.jpg)

```typescript
import { dept_child } from "@/api/operator";

export default {
  data() {
    return {
      selectData: {
        show: false,
      },
      props: {
        checkStrictly: true, 
        expandTrigger: 'hover',
        lazy: true,
        lazyLoad : async (node, resolve) => {
          const { level } = node;
          let query = {};
          if(node.data){
              query.parentId = node.value._id;
              query.isLeaf = true;
          }
          let { data } = await dept_child(query);
          let nodes = data.data.map( item => ({
            value: item,
            label: item.name,
          }))
          resolve(nodes)
        }
      }
    }
  },
  methods: {
    selectDept() {
      this.selectData.show = true;
      this.$nextTick(() => {
        document.getElementById("inputId").click();
      });
    },
    cascaderBlur(e){
      this.selectData.show = e;
    },
    cascaderChange(e) {
      let check = e[e.length - 1];
      this.form.deptId = check._id;
      this.selectData = {
        show: false,
        name: check.name
      };
      if(typeof this.findUserByDeptId == 'function'){
        this.findUserByDeptId(check._id);
      }
      const panelList = document.getElementsByClassName("el-cascader__dropdown");
      for (let i = 0; i < panelList.length; i++) {
        const item = panelList[i];
        item.innerHTML = ''
      }
    },
  }
}
```





##### fileI18nHandler.js

国际化mixins

对于一些 *下拉框* 等，在切换多语言时，需要重新渲染数据。

```javascript
export default {
  computed: {
    contractType(){//文档类别
      return require("@/views/files/conf")['contractType' + this.$t('lang.type')];
    },
    childContractType(){//文档类型
      return require("@/views/files/conf")['childContractType' + this.$t('lang.type')];
    },
    currencyUnit(){//货币单位
      return require("@/views/files/conf")['currencyUnit' + this.$t('lang.type')];
    },
    sealTypeList(){//公章对象
      return require("@/views/files/conf")['sealTypeList' + this.$t('lang.type')];
    },
    sealTypeArr(){//公章列表
      return require("@/views/files/conf")['sealTypeArr' + this.$t('lang.type')];
    },
    documentState(){//文档状态
      return require("@/views/files/conf")['documentState' + this.$t('lang.type')];
    }
  }
}
```



##### igniteDataHandler.js

发起审查时，对于审查路径做格式化

```javascript
import { user_audits } from '@/api/user';

export default {
    methods: {
        async getDefaultFormData(){
            const fmtPath = lines => {
                return lines.map( item => {
                    let cache = {};
                    if(item.agentUid){
                        cache = { resUid: item.agentUid, resName: item.agentName }
                    }else{
                        cache = { resUid: item.uid, resName: item.name }
                    }
                    return {...cache, resDeptId: item.deptId, resOptType: item.resOptType }
                })
            }
            const { auditLines, sealLines, keepLines, directLines } = this.approveLines;
            let auditList = fmtPath(auditLines);
            let sealList = fmtPath(sealLines);
            let keepList = fmtPath(keepLines);
            let directList = directLines ? fmtPath(directLines): [];

            let paths = auditList.concat(sealList).concat(keepList).concat(directList);

            const { _id, name, deptId } = this.user;
            let data = {
                reqUid: _id,
                reqName: name,
                reqDeptId: deptId,
                reqDeptName: localStorage.getItem("deptName"),
                emergency: this.form.emergency,
                deadline: this.form.date,
                paths
            }
            return data;
        },
        async getDefaultAudits(){
            const { auditLines } = this.approveLines;
            let form = {}
            for (let i = 0; i < auditLines.length; i++) {
                const audit = auditLines[i];
                form[i] = audit.agentUid || audit.uid
            }
            return form;
        },
        async getAuditList(){
            const { auditLines } = this.approveLines;
            let arr = [];
            try {
                for (let i = 0; i < auditLines.length; i++) {
                    const { level } = auditLines[i];
                    const { data } = await user_audits({level});
                    if(data.errorCode == 0){
                        arr[i] = data.data;
                    }
                }
                return arr;
            } catch (error) {
                return []
            }
        }
    }
}
```



##### toolBarAuthHandler.js

判断登录用户是否 是管理员

```javascript

import { mapGetters } from "vuex";
import { UserRole } from '@/utils/Enum/FEnum';

export default {
    computed: {
        ...mapGetters(["user"]),
        isOnlyWatch(){
            /* 
                thinking：
                如果用户
                    不为管理员
                    且user.roles.length == 1
                    且roles[0] == 0 //普通用户
            */
            const { isAdmin, roles } = this.user;
            if(!isAdmin && roles.length == 1){
                return !(roles[0] == UserRole.normal);
            }
            return true;
        }
    },
}
```



##### uploadHandler.js

附件上传，格式校验

```javascript
export default {
  methods: {
    beforeUpload({size, name}){
      if(!this.form.category){
          this.$message.warning("请先选择文档类别")
          return false;
      }
      var index = name.lastIndexOf(".");
      if(index == -1){
          this.$message.warning("请上传正确的文档格式");
          return false;
      }
      var ext = name.substr(index+1);
      const cache = [
          "doc", "docx", 
          "xlsx", "xls",
          'txt', 'TXT', 
          'pdf', 'PDF', 
          'ppt', 'pptx', 
          'pptm', 'csv', 
          'zip'
          ];
      if(cache.indexOf(ext) == -1){
          this.$message.warning("请上传正确的文档格式");
          return false;
      }
      if(size / 1024 > 10240){//10240 = 10M 
        this.$message.warning('文件不能大于10M');
        return false;
      }
    },
  }
}
```



##### userRoleHandler.js

用户角色操作相关mixin

```javascript

import { mapGetters } from "vuex";

export default {
    watch: {
        //监听 国际化
        powerList() {
            let roleCache = [];
            this.defRoles.forEach(item => {
                this.roleList.forEach(el => {
                    if (item.id == el.id) {
                        roleCache.push(el)
                    }
                })
            })
            this.defRoles = roleCache;
            let powerCache = [];
            this.defPowerList.forEach(item => {
                Object.values(this.powerList).forEach(el => {
                    if (item.id == el.id) {
                        powerCache.push(el)
                    }
                })
            })
            this.defPowerList = powerCache;
        }
    },
    computed: {
        ...mapGetters(["queryParams", "user", "urlQuery"]),
        roleList() {
            return require("@/views/usermanage/temp/conf")['roleList' + this.$t('lang.type')];
        },
        powerList() {
            return require("@/views/usermanage/temp/conf")['powerList' + this.$t('lang.type')];
        }
    },
    methods: {
        checkBox(e, bool) {
            /** 排他（一般员工） */
            if (e.id == 0 && bool) {
                this.defPowerList = this.powerList[1];
                this.defRoles = [this.roleList[0]];
            } else {
                let list = [];
                this.defRoles.forEach(item => {
                    if (item.id != 0) {
                        list.push(item);
                    }
                })
                this.defRoles = list;
            }
        },
        checkBoxHandler(e) {
            this.form.isAdmin = false;
            let set = new Set();
            let list = [];
            e.forEach(item => {
                if (item.isAdmin) {
                    this.form.isAdmin = true
                }
                item.power.forEach(id => {
                    set.add(id);
                });
            });
            for (const id of set) {
                list.push(this.powerList[id]);
            }
            this.defPowerList = list;
        },
        async validate() {
            this.$refs.form.validate(valid => {
                if (valid) {
                    if (this.defRoles.length == 0) {
                        this.$message.warning(this.$t("usermanage.temp.msg.norole"));
                        return;
                    }
                    if (!this.form.deptId) {
                        this.$message.warning(this.$t("usermanage.temp.msg.nodept"));
                        return;
                    }
                    this.submit();
                }
            });
        },
    }
}
```

