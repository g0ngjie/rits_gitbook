### 归档失败

> 我的文档下 => 归档失败 页面

![](..\..\images\faildoc.jpg)



##### index.vue

> 表格

```javascript
<script>
import { user, doc } from '@/api'; // 接口文件
import searchTemp from './searchTemp'; // 搜索组件
import { mapGetters } from 'vuex';
import { renderSize, fmtSearchTime } from '@/utils'; // 工具
import { MyDocOpt, UserRole as RoleEnum } from '@/utils/Enum/FEnum'; // 服务端枚举
import {
    ToolButton as toolBtn, // 工具栏 组件
    Pagination as paginationTemp, // 分页 组件
} from '@/components';
import { fmtTime } from '@/filters'; // 过滤器
import { sortChange } from '@/mixins'; // 排序 mixins

/* faildoc */
export default {
    components: { // 组件注册
        searchTemp,
        paginationTemp,
        toolBtn
    },
    filters: { fmtTime }, // 过滤器注册
    data() {
        return {
            loading: false, // loading
            listQuery: { page: 0, count: 10 }, // 查询数据
            tableData: [], // 表格数据
            page: { current: 1, total: 0 }, // 分页数据
            footer: { totalCount: 0 }, // 页脚 数据
            multipleSelection: [], // 表格行选择 数据
        }
    },
    computed: {
        ...mapGetters(['isAdmin', 'user']), // vuex mapGetters 
    },
    mixins: [sortChange], // mixins 继承
    methods: {
        renderSize, // 计算大小
        handleSelectionChange(val) { // 表格行选择时 触发
            this.multipleSelection = val;
        },
        downFiles(row) {//下载按钮
            const { storagePath, name } = row;
            window.location.href = `/api/doc/download?storagePath=${storagePath}&name=${name}`;
        },
        async deleteFail() {//归档失败删除
            try {
                const res = await this.$confirm(this.$t('files.table.msg.confirm'));
                if (res == "confirm") {
                    const ids = this.multipleSelection.map(item => {
                        return { _id: item._id }
                    });
                    this.loading = true;
                    let { data } = await doc.doc_delete_batch({ ids });
                    this.loading = false;
                    if (data.errorCode === 0) {
                        this.$message.success(this.$t('files.table.msg.del'));
                        this.initList();
                    }
                }
            } catch (error) {
                this.loading = false;
            }
        },
        isSelect() {//管理员 和 存档担当 具有查看其他人的权限
            if (this.isAdmin || this.user.roles.includes(RoleEnum.archiver)) {
                return true;
            } else {
                return false;
            }
        },
        async initList() {// 初始化 数据列表
            this.loading = true;
            let query = fmtSearchTime(this.listQuery);
            const { roles } = this.user;
            if (!this.isSelect()) query.uid = this.user._id
            const [{ data: page }, { data: count }] = await Promise.all([user.user_docs_pages(MyDocOpt.fail, query), user.user_docs_count(MyDocOpt.fail, query)]);
            this.page.total = count.data.totalCount;
            this.footer = count.data;
            this.loading = false;
            this.tableData = page.data;
        },
    },
    created() {// 钩子、页面初始化时 触发
        this.initList();
    }
}
</script>
```



##### searchTemp/index.vue

> 搜索栏组件



```javascript
<script>
import { user } from '@/api'; // 接口文件
import { cascaderHandler } from '@/mixins';
import { mapGetters } from 'vuex';
import { omit, verifyDateRange } from '@/utils'; // 工具
import { LocalStorageEnum as LSEnum } from '@/utils/Enum/VEnum'; // 前端枚举

export default {
    data() {
        return {
            userList: [], // 用户列表数据
            form: { deptId: null }, //归档失败form 获取deptId使用
        }
    },
    props: {
        query: { // 查询参数
            type: Object,
            default() { }
        },
    },
    mixins: [cascaderHandler], // mixins 继承
    computed: {
        ...mapGetters([
            'isAdmin', 'user'
        ]),
        option() {
            let option;
            if(this.isAdmin){
                option = {
                    dept: this.$t('files.search.placeholder'),
                    user: this.$t('files.search.placeholder')
                }
            }else{
                option = {
                    user: this.user.name,
                    dept: localStorage.getItem(LSEnum.deptName)
                }
            }
            return option;
        }
    },
    methods: {
        removeDeptInp() {// 清空部门数据时 触发
            this.query = omit(this.query, 'uid')
            this.userList = [];
        },
        searchChange(key, e) { // 用户下拉选择 改变时 触发
            if(key === 'userId' && !e){
              delete this.query.uid;
            }
            if (!e) delete this.query[key];
        },
        search() { // 点击查询 调用 父组件 事件
            let verify = verifyDateRange(this.query);
            if (!verify) {
                this.$message.warning(this.$t('files.search.warning'))//请选择正确的期间
                return;
            }
            this.$emit("search");
        },
        async findUserByDeptId(deptId){// mixins callback
            let { data } = await user.user_by_dept(deptId);
            delete this.query.uid;
            this.userList = data.data.map(item => {
                return {
                    value: item._id,
                    label: item.name
                }
            })
        }
    },
}
</script>
```

