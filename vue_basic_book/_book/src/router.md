### 路由

> 目前拆分出来两个文件**index.js**、**menu.js**
>
> index.js 是路由的主文件
>
> menu.js 结合 `@/store/modlues/permission.js`  文件，可以根据用户角色，动态添加路由。用来控制权限。



##### 懒加载

> 当打包构建应用时，Javascript 包会变得非常大，影响页面加载速度。如果我们能把不同路由对应的组件分割成不同的代码块，然后当路由被访问的时候才加载对应组件，这样就更加高效了。

```js
{ path: '/login', component: () => import('@/views/login/index')}
```

