### 多继承

> Vue提供一套 对于 *script* 标签里存在重复定义的 *钩子函数*、*属性* 等，提供了一个抽象的定义 *mixins*。
>
> 类似父类。对于重复的定义，统一管理，减少冗余。提高代码的简洁性和可阅读性。



##### DatePickerMixin.ts

此功能只在*日志管理* 的筛选栏里规范 *开始日期* 和 *结束日期* 。

用户在选择开始日期后，避免结束日期小于开始日期等。

后期如果再有日期区间，可以用此功能。

![](..\images\DatePickerMixin.jpg)

```typescript
import Vue from "vue";
import Component from "vue-class-component";

@Component({ name: "DatePickerMixin" })
export default class DatePickerMixin extends Vue {
  startValue: any = null;
  endValue: any = null;

  disabledStartDate(startValue: any) {
    const endValue = this.endValue;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }
  disabledEndDate(endValue: any) {
    const startValue = this.startValue;
    if (!endValue || !startValue) {
      return false;
    }
    return startValue.valueOf() >= endValue.valueOf();
  }
}
```





##### PaginationMixin.ts

分页 mixins

```typescript
import Vue from "vue";
import Component from "vue-class-component";

@Component({ name: "PaginationMixin" })
export default class PaginationMixin extends Vue {
  pageSize: number = 10;

  pagination: any = {
    current: 1,
    total: 0,
    defaultPageSize: 10,
    showSizeChanger: true,
    onShowSizeChange: (current: any, pageSize: any) => {
      this.setPageSize(pageSize);
    }
  };

  setPageSize(size: number) {
    this.pageSize = size;
  }
}
```

