# 新建页面



> 按照正常编码流程，重新创建一个页面。
>
> 参考 **用户管理** 模块

* 添加一个页面，路径 `src\views\newPage\index.vue` 

  ![](..\images\newPage_init.jpg)

  

* 添加`router` 路由

  * 路径 `src\router\menu.js`  找到定义用户管理模块的 `json` => `usermanage`，复制一条。（路由的配置格式都是一样的）

    ![](..\images\newPage_router_usernamage.jpg)

  * 新定义页面名称为 `newPage`

    ![](..\images\newPage_router_new.jpg)
  
  
  * 页面效果
  
    ![](..\images\newPage_view.jpg)
  
* 添加工具栏

  * 引入自定义的组件列表

    ```javascript
    import {
        ToolButton as toolBtn, // 工具栏组件
        Pagination as paginationTemp // 分页组件
    } from '@/components';
    ```

  * 局部注册

    ```javascript
    components: {// 组件注册
        paginationTemp, // 分页
        toolBtn // 按钮
    },
    ```

  * Dom层

    ```javascript
    <!-- 工具栏 -->
            <section class="filter-container search-bar-container">
                <div class="tool-btn-container">
                    <!-- 添加用户 -->
                    <tool-btn :enable="true" icon="iconfont ali-add1" content="新建" @clickHandler="add"/>
                    <!-- 用户编辑 -->
                    <tool-btn :enable="isEdit" icon="iconfont ali-edit1" content="编辑" @clickHandler="edit"/>
                </div>
                <!-- 根据登录账号、邮箱搜索 -->
                <div style="margin-right: 110px;display: flex;">
                    <el-input placeholder="请输入" prefix-icon="el-icon-search" clearable v-model="listQuery.keyword">
                    </el-input>
                    <el-button icon="el-icon-search" @click="initList"></el-button>
                </div>
            </section>
    ```

  * 效果图

    ![](..\images\newPage_toolbtn.jpg)

* 添加表格和分页

  * 接口文件引入

    ```javascript
    import { operator_page, operator_count } from '@/api/operator';// 接口文件
    ```

  * 表格

    ```javascript
    <el-table class="table" :data="tableData">
                <!-- 用户列表 -->
                <el-table-column type="selection" width="55" />
                <el-table-column prop="account" label="账户" show-overflow-tooltip/>
                <el-table-column prop="name" label="用户名" show-overflow-tooltip />
                <el-table-column prop="email" label="邮箱" show-overflow-tooltip />
                <el-table-column prop="deptName" label="部门" show-overflow-tooltip />
            </el-table>
    ```

  * 自定义分页组件

    ```javascript
    <section class="footer-container">
                <pagination-temp :page.sync="page" :query.sync="listQuery" @initList="initList" />
            </section>
    ```

  * 接口获取数据，赋值给 `tableData`

    ```javascript
    methods: {
            async initList() {// 初始化数据
                try {
                    const [{ data: page }, { data: count }] = await Promise.all([operator_page(this.listQuery), operator_count(this.listQuery)]);
                    this.page.total = count.data.count;
                    this.tableData = page.data;
                } catch (error) {
                    this.loading = false;
                }
            }
        },
        created() {
            this.initList();
        }
    ```

* 最终效果

  ![](..\images\newPage_table.jpg)

* 代码

  ```javascript
  <template>
      <div class="app-container">
          <!-- 工具栏 -->
          <section class="filter-container search-bar-container">
              <div class="tool-btn-container">
                  <!-- 添加用户 -->
                  <tool-btn :enable="true" icon="iconfont ali-add1" content="新建" @clickHandler="add"/>
                  <!-- 用户编辑 -->
                  <tool-btn :enable="isEdit" icon="iconfont ali-edit1" content="编辑" @clickHandler="edit"/>
              </div>
              <!-- 根据登录账号、邮箱搜索 -->
              <div style="margin-right: 110px;display: flex;">
                  <el-input placeholder="请输入" prefix-icon="el-icon-search" clearable v-model="listQuery.keyword">
                  </el-input>
                  <el-button icon="el-icon-search" @click="initList"></el-button>
              </div>
          </section>
          <el-table class="table" :data="tableData">
              <!-- 用户列表 -->
              <el-table-column type="selection" width="55" />
              <el-table-column prop="account" label="账户" show-overflow-tooltip/>
              <el-table-column prop="name" label="用户名" show-overflow-tooltip />
              <el-table-column prop="email" label="邮箱" show-overflow-tooltip />
              <el-table-column prop="deptName" label="部门" show-overflow-tooltip />
          </el-table>
          <section class="footer-container">
              <pagination-temp :page.sync="page" :query.sync="listQuery" @initList="initList" />
          </section>
      </div>
  </template>
  <script>
  import {
      ToolButton as toolBtn, // 工具栏组件
      Pagination as paginationTemp // 分页组件
  } from '@/components';
  import { operator_page, operator_count } from '@/api/operator';// 接口文件
  export default {
      components: {// 组件注册
          paginationTemp, // 分页
          toolBtn // 按钮
      },
      data() {
          return {
              isEdit: false,
              listQuery: { page: 0, count: 10 }, // 查询参数
              tableData: [], // 表格数据
              page: { current: 1, total: 0 }, // 分页数据
          }   
      },
      methods: {
          add() {
              console.log('add')
          },
          edit() {
              console.log('edit')
          },
          async initList() {// 初始化数据
              try {
                  const [{ data: page }, { data: count }] = await Promise.all([operator_page(this.listQuery), operator_count(this.listQuery)]);
                  this.page.total = count.data.count;
                  this.tableData = page.data;
              } catch (error) {
                  this.loading = false;
              }
          }
      },
      created() {
          this.initList();
      }
  }
  </script>
  ```

  