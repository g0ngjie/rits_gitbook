### 定义DTO、Interface文件



##### Driver.ts

定义驱动 *变量* 类型。

```typescript
import { OsType, DriverType, CpuBits } from "@/utils/VEnum";
export interface IDriver {
  _id: string /* 主键 */;
  createTime: string /* 驱动上传时间 */;
  osType: OsType /* 系统类型 */;
  driverType: DriverType /* 驱动类型 */;
  cpuBits: CpuBits /* 系统位数 */;
  fileName?: string /* 驱动名称 */;
  fileSize: number /* 驱动大小 */;
  version: string /* 版本类型 */;
  description?: string /* 变更履历 */;
  isNewest: boolean /* 是否是最新版本 */;
  httpPath: string /* 驱动静态资源的映射地址 */;
  md5?: string /* md5文件加密標示 */;
  filePath: string /* 文件上传路径 */;
}
```



##### InstallLog.ts

```typescript
import { DriverType, OsType, CpuBits, UpgradeResult } from "@/utils/VEnum";

export interface IDriver {
  _id: string;
  createTime: string /* 驱动安装时间 */;
  userName: string /* pc端用户名 */;
  hostName?: string /* pc端主机名 */;
  domainName?: string /* pc端域名 */;
  version?: string /* 升级包的版本号 */;
  driverType: DriverType /* 驱动类型 */;
  osType: OsType /* 系统类型 */;
  cpuBits: CpuBits /* 位数 */;
  osVer?: string /* 当前操作系统版本 */;
  oldVer: string /* 升级前的版本 */;
  newVer: string /* 升级后的版本 */;
  result: UpgradeResult /* 安装结果 */;
  httpPath: string /* 失败文件存储地址(静态资源) */;
}
```



##### Strategy.ts

```typescript
import { InstallMethod } from "@/utils/VEnum";
/* 策略 */
export interface IStrategy {
  _id: string /* 主键 */;
  method: InstallMethod /* 安装方式 */;
  timeInterval: number /* 间隔时间 */;
}
```



##### Response.ts

```typescript
export interface IRespones {
  errNo: number /* 服务端返回错误码 */;
  msg: string | null /* 错误信息 */;
  item?: any /* <T> */;
}
```

