# views 组件包、视图目录

* 目录

  * activate 用户申请激活页面
  * approvatemanage 审批规则管理
  * deptmanage 部门管理
  * errorPage 401 404 页面
  * files 全部文档，上下期度文档等
  * init 管理员创建页面
  * layout 布局组件
  * login 登录页面
  * myfiles 待我处理
  * passmail 密码找回
  * redirect 重定向跳转页面 => 此页面主要用来做一些页面跳转传值。
  * sysmanage 系统设置
  * template 模板管理
  * updateinfo 个人信息修改
  * usermanage 用户管理
  
  
  
* App.vue 根组件

  ```javascript
<template>
    <div id="app">
      <router-view/>
    </div>
  </template>
  
  <script>
  export default {
    name: 'App'
  }
  </script>
  ```
  
  
  
* main.ts vue主入口文件

  ```javascript
  import Vue from 'vue'
  import Cookies from 'js-cookie' //cookie缓存
  import "babel-polyfill"; //解决vue在ie的兼容性问题
  import 'normalize.css/normalize.css' // A modern alternative to CSS resets
  
  import Element from "element-ui"; //elementui
  import "element-ui/lib/theme-chalk/index.css"; //element 样式
  
  import "@/styles/index.scss"; // global css
  import "@/assets/icon/iconfont.css"; //阿里矢量图标
  
  import App from './App' //根组件
  import router from './router' //路由
  import store from "./store"; //vuex
  
  import i18n from './lang' // 国际化引入
  
  import "./permission"; //拦截器注入
  // import "./mock";
  import Validate from "@/utils/validate"; 
  Vue.use(Validate); //表单校验注册
  
  Vue.use(Element, {
    size: Cookies.get('size') || 'medium', // set element-ui default size
    i18n: (key, value) => i18n.t(key, value) //使用国际化
  })
  
  Vue.config.productionTip = false
  
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    router,
    store,
    i18n, //国际化注册
    render: h => h(App) //渲染页面
  })
  ```

  

* permission.js 全局拦截器

  ```javascript
  import router from './router'
  import store from './store'
  import NProgress from 'nprogress' // progress bar
  import 'nprogress/nprogress.css'// progress bar style
  
  NProgress.configure({ showSpinner: false })// NProgress Configuration
  
  const whiteList = ['/login','/passmail', '/init', '/msg']// no redirect whitelist
  
  router.beforeEach(async (to, from, next) => {
    NProgress.start() // start progress bar
    if (localStorage.getItem("token")) { // determine if there has token
      /* has token*/
      if (to.path === '/login') {
        next({ path: '/' })
        NProgress.done() // if current page is dashboard will not trigger	afterEach hook, so manually handle it
      } else {
        // 路由是否加载完成标识
        if(!store.getters.routerLoadDone){
          await store.dispatch('getUserInfo') // 获取用户信息
          await store.dispatch('generateRoutes') // 生成可访问的路由表
          router.addRoutes(store.getters.addRouters) // 动态添加可访问路由表
          next({ ...to, replace: true }) // hack方法 确保addRoutes已完成
        }else{
          next()
        }
      }
    } else {
      /* has no token*/
      if (whiteList.indexOf(to.path) !== -1) { // 在免登录白名单，直接进入
        next()
      } else {
        store.dispatch("setUrlQuery", to.query);
        next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
        NProgress.done() // if current page is login will not trigger afterEach hook, so manually handle it
      }
    }
  })
  
  router.afterEach(() => {
    NProgress.done() // finish progress bar
  })
  ```

  

