### 全局公用样式库



##### index.less

定义了一些全局的样式，取消html默认比较丑的样式。 调整整体宽高为100%等；

```less
html {
  height: 100%;
}

#app {
  height: 100%;
}
...
...
```

