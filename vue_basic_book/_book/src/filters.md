### 过滤器

> 定义公用的过滤器。



```js
import moment from "moment";

/**
 * 格式化日期
 * @param {*} date
 */
export function fmtDate(date) {
  return date && moment(date).format("YYYY-MM-DD");
}
```

