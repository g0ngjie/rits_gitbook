### 用印规则

![](..\..\images\sealmanage.jpg)



##### index.vue

```javascript
<script>
import { rule } from '@/api'; // 接口文件
import paginationTemp from '@/components/Pagination' // 分页组件
import toolBtn from '@/components/ToolButton'; // 工具栏组件
import { RuleType as RuleTypeEnum } from '@/utils/Enum/FEnum'; // 服务端枚举
import { SealManagePathEnum as SealPathEnum, RouterToEnum } from '@/utils/Enum/VEnum'; // 前端枚举

export default {
    components: { paginationTemp, toolBtn }, // 组件注册
    data(){
        return {
            loading: false, // loading
            btnEnable: false, // 工具栏按钮 状态
            listQuery: {page: 0, count: 10,type: RuleTypeEnum.seal }, // 查询参数
            tableData: [],   // 表格参数
            multipleSelection: [], // 表格行选择 数据
            page: {current: 1, total: 0}, // 分页数据
        }
    },
    methods: {          
        searchChange(key, e){ // 规则名称搜索 发生改变时 触发
            if(!e) delete this.listQuery[key];
        },
        handleSelectionChange(val) { // 表格行选择时 触发
            this.multipleSelection = val;
            this.btnEnable = val.length === 1;
        },
        setBtnEnable(bool, val){
            this.multipleSelection = val;
            this.btnEnable = val.length === 1;
        },
        async del(){// 删除
            try {
                const res = await this.$confirm(this.$t("sealmanage.msg.isDel"));
                if (res == "confirm") {
                    let {_id} = this.multipleSelection[0];
                    this.loading = true;
                    let {data} = await rule.rule_delete(_id);
                    this.loading = false;
                    this.$message.success(this.$t("sealmanage.msg.success"));
                    this.initList();
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async setUp(){//设置默认规则
            try {
                const res = await this.$confirm(this.$t("sealmanage.msg.isSetUp"));
                if (res == "confirm") {
                    let {_id} = this.multipleSelection[0];
                    this.loading = true;
                    let {data} = await rule.rule_default(_id);
                    this.loading = false;
                    this.$message.success(this.$t("sealmanage.msg.setSuccess"));
                    this.initList();
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async edit(){// 跳转 编辑页面
            this.$store.dispatch("setParams", this.multipleSelection[0]);
            this.$router.push({path: "/updateseal", query: { to: RouterToEnum.edit}});
        },
        openAdd(){// 跳转 添加页面
            this.$router.push({path: "/updateseal", query: { to: RouterToEnum.add}});
        },
        search(){// 点击搜索按钮
            this.initList();
        },
        async initList(){ // 初始化 数据列表
            this.loading = true;
            const [{ data: page }, { data: count }] = await Promise.all([rule.rule_page(this.listQuery), rule.rule_count(this.listQuery)]);
            this.page.total = count.data.count;
            this.loading = false;            
            let formData = page.data.map(item => {
                return {
                    ...item,
                    sealer:item.sealPath[SealPathEnum.sealer].name,
                    imprint:item.sealPath[SealPathEnum.imprint].name
                }
            });
            this.tableData = formData;
        },
    },
    created(){ // 钩子、页面加载前 触发
        this.initList();
        this.$store.dispatch("clearParams");
    }
}
</script>
```



![](..\..\images\sealmanage_add.jpg)



> 添加/编辑 页面

```javascript
<script>
import { mapGetters } from 'vuex';
import { operator, rule } from '@/api'; // 接口文件
import { 
    RuleType as RuleTypeEnum,
    UserRole as RoleEnum
} from '@/utils/Enum/FEnum'; // 服务端枚举
import { SealManagePathEnum as SealPathEnum, RouterToEnum } from '@/utils/Enum/VEnum'; // 前端枚举
import _ from 'lodash';

export default {
    data() {
        return {
            form: { sealPath: [] }, // 表单数据
            loading: false, // loading
            disabled: false,
            sealList: [], // 盖章担当列表
            imprintList: [], // 押印担当列表
            selectData: {} // select 选择的 id 数据
        };
    },
    computed: {
        ...mapGetters(['queryParams']),
        title(){
            if(!this.queryParams.name){
                return this.$t("sealmanage.temp.title.add");
            } else {
                return this.$t("sealmanage.temp.title.edit");
            }
        },
    },
    methods: {
        back(){// 返回上一层
            this.$router.go(-1);
        },
        selectHandler(key, uid){ // 下拉选择器 选择时触发
            //用户对应 sealPath 索引
            const { sealer, imprint } = SealPathEnum;
            const user2Seal = { sealList: sealer, imprintList: imprint };
            let str = this.imprintList;
            const resultList = this[key].filter(item => {
                if(item.uid === uid) return item;
            })
            this.form.sealPath[user2Seal[key]] = resultList[0];
        },
        async initSelectList(){// 初始化select 数据
            this.sealList = await this.initSealList();
            this.imprintList = await this.initImprintList();
        },
        async validate(){ // 表单校验
            this.$refs.form.validate(valid => {
                if (valid) {
                    const isRequired = ['sealerId', 'imprintId'];
                    for (let i = 0; i < isRequired.length; i++) {
                        const key = isRequired[i];
                        if(!this.selectData[key]){
                            this.$message.warning(this.$t('sealmanage.msg.selectPlaceholder'))  
                            return;
                        }
                    }
                    this.submit();
                }
            });
        },
        async submit() { // 表单提交
            let { name, sealPath } = _.cloneDeep(this.form);
            let formData = { name, sealPath, type: RuleTypeEnum.seal };
            try {
                this.loading = true;
                if (!this.queryParams.name) {
                    let data = await rule.rule_add(formData);
                    this.$message.success(this.$t("sealmanage.msg.add"));
                } else {
                    let { data } = await rule.rule_edit(this.form._id, formData);
                    this.$message.success(this.$t("sealmanage.msg.edit"));
                }
                this.loading = false;
                this.back();
            } catch (error) {
                this.loading = false;
            }
        },
        fmtInitList(list){// 格式化指定格式的 列表 函数
            return list.map( item => {
                return {
                    uid: item._id,
                    name: item.name,
                    deptId:item.deptId,
                    roles:item.roles,
                    permissions:item.permissions 
                }
            })
        },
        async initSealList(){ // 初始化 盖章担当列表
            let {data} = await operator.operator_page({role: RoleEnum.sealer});
            return this.fmtInitList(data.data);
        },
        async initImprintList(){ // 初始化 押印担当列表
            let {data} = await operator.operator_page({role: RoleEnum.sealSaver});
            return this.fmtInitList(data.data);
        },        
    },
    async created(){ // 钩子、页面加载前触发
        await this.initSelectList();
        if(this.queryParams.name){
            this.form = this.queryParams;
            const { sealPath } = this.queryParams;
            const { sealer, imprint } = SealPathEnum;
            this.selectData = {
                sealerId: sealPath[sealer].uid,
                imprintId: sealPath[imprint].uid
            }
        }
        if(this.$route.query.to == RouterToEnum.edit && !this.queryParams.name){
            this.$router.go(-1);
        }
    }
}
</script>
```

