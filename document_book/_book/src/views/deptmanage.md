### 部门管理

> 该模块由 上（工具栏）、左（部门树结构）、右布局（表格）组成。

![](..\..\images\dept.jpg)

##### inidex.vue

> 部门根组件

```javascript
<script>
import tableTemp from './table';// 表格组件
import { dept } from '@/api';// 接口文件
import { mapGetters } from 'vuex';
import {
    ToolButton as toolBtn, // 工具栏组件
    UploadCsv as uploadCsv // 部门导入 组件
} from '@/components';
import printtingTable from './dialog'; // 用印规则 组件
import { RouterToEnum } from '@/utils/Enum/VEnum'; // 前端枚举

export default {
    components: { // 组件注册
        tableTemp,
        uploadCsv,
        toolBtn,
        printtingTable
    },
    data() {
        return {
            loading: false, // loading
            defaultDeptName: '', //默认部门名称
            btnEnable: false, // 工具栏按钮 状态
            btnRuleEnable: false, // 关联规则 按钮 状态
            listQuery: { page: 0, count: 10 }, // 表格查询参数
            tableData: [], // 表格数据
            multipleSelection: [], // 表格行选择 列表
            defaultProps: {// 默认props element 的el-tree 组件属性，不需要再改动
                label: 'name'
            },
        };
    },
    computed: {// 计算函数
        ...mapGetters([
            'emptyDept', //部门是否为空，默认项目初始化时，数据库 表数据为空的情况下
        ]),
        deptName: {// 部门名称 get set 函数
            get() {
                return this.defaultDeptName || this.$t('deptmanage.btn.defaultDeptName');
            },
            set(val) {
                this.defaultDeptName = val;
            }
        }
    },
    methods: {
        async setDeptRule(ruleHashId){//设置部门规则回调接口
            const deptIds = this.multipleSelection.map( item => item._id)
            try {
                await dept.dept_put_rule(ruleHashId, { deptIds })
                this.$refs.table.initList();
            } catch (error) {
                
            }
        },
        matchRule(){// 关联规则 模态
            this.$refs.printtingTable.isShow = true;
            this.$refs.printtingTable.initList();
        },
        exportDept() {// 导入部门 模态
            this.$refs.uploadCsv.isShow= true;
        },
        setBtnEnable(bool, val) {// 子组件（表格） 行选择时 触发
            this.multipleSelection = val;
            this.btnRuleEnable = val.length > 0;
            this.btnEnable = bool;
        },
        async loadDeptNode(node, resolve) {// 加载 tree 部门节点，调用接口，获取子部门列表
            let query = {};
            if (node.data) {
                query.parentId = node.data._id
                query.isLeaf = true;
            }
            let { data } = await dept.dept_child(query);
            resolve(data.data)
        },
        handleNodeClick(data) {// 部门树 节点点击时触发
            this.deptName = data.name;
            this.listQuery.parentId = data._id;
            this.$refs.table.initList(data._id);
        },
        async del() {// 删除部门
            try {
                const res = await this.$confirm(this.$t('deptmanage.msg.confirm'));
                if (res == "confirm") {
                    this.loading = true;
                    let { _id } = this.multipleSelection[0];
                    await dept.dept_delete(_id);
                    this.loading = false;
                    this.$message.success(this.$t('deptmanage.msg.del'));
                    this.$router.replace({ path: '/redirect/deptmanage' })
                }
            } catch (error) {
                this.loading = false;
            }
        },
        edit() {// 编辑部门
            this.$store.dispatch("setParams", this.multipleSelection[0])
            this.$router.push({ path: "/updatedept", query: { to: RouterToEnum.edit, is: this.emptyDept } })
        },
        openAdd() {// 添加部门
            this.$store.dispatch("clearParams");
            this.$router.push({ path: "/updatedept", query: { to: RouterToEnum.add, is: this.emptyDept } })
        },
        search() {// 查询函数
            this.initList();
        },
        async initList() {// 初始化数据
            this.$refs.table.initList();
        },
    },
    mounted() {// 钩子函数、 页面加载后触发
        this.initList();
    },
}
</script>
```



##### dialog/index.vue

> 关联用印规则 模态



```javascript
<script>

import { rule } from '@/api';// 接口文件
import { 
    RuleType as RuleTypeEnum // 服务端枚举
} from '@/utils/Enum/FEnum';
import { 
    Pagination as paginationTemp,  //分页组件
    ToolButton as toolBtn //工具栏
} from '@/components';

export default {
    components: {
        paginationTemp, toolBtn
    },
    data(){
        return {
            isShow: false, // 模态 是否展示
            loading: false, // loading
            listQuery: { page: 0, count: 10, type: RuleTypeEnum.seal },// 表格查询参数
            tableData: [],   //表格数据
            page: {current: 1, total: 0}, // 分页数据
        }
    },
    methods: {
        async submit(row){//部门关联用印规则
            const { _id, name } = row;
            try {
                const nameLen = name.length;
                console.log(nameLen,' nameLen')
                let _title = name;
                if(nameLen > 15){
                    _title = name.substring(0, 15) + '...';
                }
                const res = await this.$confirm(this.$t('deptmanage.msg.confirmRule') + _title);
                if (res == "confirm") {
                    this.$emit('submit', _id);
                    this.isShow = false;
                }
            } catch (error) {
            }
        },
        async initList(){// 页面初始化 数据
            this.loading = true;
            const [{ data: page }, { data: count }] = await Promise.all([rule.rule_page(this.listQuery), rule.rule_count(this.listQuery)]);
            this.loading = false;            
            this.page.total = count.data.count;
            let formData = page.data.map(item => {
                return {
                    ...item,
                    sealer:item.sealPath[0].name,
                    imprint:item.sealPath[1].name
                }
            });
            this.tableData = formData;
        },
    }
}
</script>
```



##### table/index.vue

> 表格

```javascript
<script>
import { dept } from '@/api';// 接口文件
import { Pagination as paginationTemp } from '@/components';// 分页组件

export default {
    name: "table-temp",
    data() {
        return {
            loading: false, // loading
            tableData: [], // 表格数据
            page: { current: 1, total: 0 } // 分页数据
        };
    },
    components: { paginationTemp }, // 组件注册
    props: { // 父组件传值
        deptName: { // 部门名称
            type: String,
            default: ''
        },
        query: { // 查询参数
            type: Object,
            default: () => { }
        },
    },
    methods: {
        handleSelectionChange(val) { // 表格行选择 时触发
            this.$emit("setBtnEnable", val.length === 1, val)
        },
        async initList(pid) { // 初始化数据
            if (pid) {
                this.query.parentId = pid;
            }
            this.loading = true;
            const [{ data: page }, { data: count }] = await Promise.all([dept.dept_page(this.query), dept.dept_count(this.query)]);
            this.page.total = count.data.count;
            this.$store.dispatch("isEmptyDept", count.data.total || 0)
            this.loading = false;
            this.tableData = page.data;
        }
    }
};
</script>
```



![](..\..\images\dept_add.jpg)



##### temp/index.vue

> 部门添加/编辑



```javascript
<script>
import { mapGetters } from 'vuex';
import { dept } from '@/api'; // 接口文件
import { cascaderHandler } from '@/mixins';// 级联选择器 mixins
import { Switch as vswitch } from '@/components'; // switch 组件引入
import { RouterToEnum } from '@/utils/Enum/VEnum'; // 前端枚举

export default {
    data() {
        return {
            form: { isSealDept: false }, // 表单数据
            title: '', // title
        }
    },
    components: { vswitch }, // 组件注册
    mixins: [cascaderHandler], // mixins 继承
    computed: {// 计算函数
        ...mapGetters([
            'queryParams' // 页面跳转参数
        ]),
    },
    methods: {
        back() {// 返回上一层
            this.$router.go(-1);
        },
        async submit() {//表单提交
            if (!this.queryParams.name) {
                let data = await dept.dept_add(this.form);
                this.$message.success(this.$t('deptmanage.temp.msg.add'));
            } else {
                let { _id, parentId, name, referred, description, isSealDept } = this.form;
                let { data } = await dept.dept_edit(_id, { parentId, name, referred, description, isSealDept })
                this.$message.success(this.$t('deptmanage.temp.msg.edit'));
            }
            setTimeout(() => {
                this.back();
            }, 100);
        },
        async validate() {// 表单校验
            this.$refs.form.validate(valid => {
                if (!valid) {
                    return false;
                }
                if (this.form.deptId) {
                    this.form.parentId = this.form.deptId;
                    delete this.form.deptId;
                }
                this.submit();
            });
        },
        initTile() {// 初始化 title
            this.title = !this.queryParams.name ? this.$t('deptmanage.temp.title.add') : this.$t('deptmanage.temp.title.edit');
            if (this.queryParams.name) {
                this.form = this.queryParams;
                this.selectData.name = this.queryParams.parentName;
            }
        }
    },
    created() {// 钩子、页面加载前触发
        this.initTile();
        if (this.$route.query.to == RouterToEnum.edit && !this.queryParams.name) {
            this.$router.go(-1);
        }
    }

}
</script>
```

