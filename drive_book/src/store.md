### Vuex



实际上就是一个全局的数据存储仓库。具体理解参考官方文档。



##### modules/user.ts

```typescript
import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";// vuex 需要用到的装饰器
import { LocalStorage as LocalEnum } from "@/utils/VEnum";// 枚举
import { user_login } from "@/api/user";// 接口

interface IUserState {// 定义变量声明类型
  _isAdmin: boolean;
}

interface IUser {// 定义变量声明类型
  name?: string;
  account?: string;
  email?: string;
  isAdmin?: boolean;
}

@Module({ name: "User" })// 注册
export default class User extends VuexModule implements IUserState {
  _isAdmin: boolean = false;

  _user: IUser = {};

  _spinning: boolean = true;

  get spinning() {// 类似 vue的 computed 计算函数， get set 方法
    return this._spinning;
  }

  get isAdmin() {
    return this._isAdmin;
  }

  get token() {
    return localStorage.getItem(LocalEnum.token.toString()) || "";
  }

  get user() {
    const userStr = localStorage.getItem(LocalEnum.user.toString());
    return userStr ? JSON.parse(userStr) : {};
  }

  @Mutation // 参考 原生 vuex的 mutation
  private LOADING(loading: boolean) {
    this._spinning = loading;
  }

  @Mutation
  private UPDATE_USER(user: IUser) {
    // this._user = user;
    localStorage.setItem(LocalEnum.user.toString(), JSON.stringify(user));
  }

  @Mutation
  private UPDATE_TOKEN(token: string) {
    if (token) {
      localStorage.setItem(LocalEnum.token.toString(), token);
    } else {
      localStorage.clear();
    }
  }

  @Action({ commit: "LOADING" }) //vuex action
  public async loading(load: boolean) {
    return load;
  }

  @Action({ commit: "UPDATE_TOKEN" })
  public async userLogin(form: any) {
    try {
      this.LOADING(false); // loading
      const { data } = await user_login(form);
      if (data.errNo === 0) {
        const { email, account, isAdmin } = data?.item?.user;
        this.UPDATE_USER({ name: account, email, account });
        return "token";
      }
      return "token";
    } catch (error) {
      this.LOADING(true);
      return "";
    }
  }

  @Action({ commit: "UPDATE_TOKEN" })
  public async resetToken() {
    return "";
  }

  //前端登出
  @Action({ commit: "UPDATE_TOKEN" })
  public async logout(e: any) {
    return "";
  }
}
```



##### store/index.ts

```typescript
import Vue from "vue";
import Vuex from "vuex";
import User from "./modules/user";
import App from "./modules/app";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  modules: {
    User,
    App
  }
});
```

