### Vuex



> 实际上就是一个全局的数据存储仓库。具体理解参考官方文档。
>
> modules下/ app.js、parameter.js、tagsView.js、url.js、user.js



##### modules/user.js

用户操作比如登录，退出，获取用户信息，都可以在此文件获取信息（通过vuex方式）

```javascript
import { user_login, user_info_get, user_logout } from '@/api/user';
import webSocket from "@/utils/webSocket";

const user = {
  state: {
    user: {},
    token: localStorage.getItem("token"),
    name: null,
    avatar: null,
    isAdmin: null,
    ws: null,
    version: null,
    isTax: false, //免税处理担当
    status: null
  },
  mutations: {
    SET_USER: (state, user) => {
      state.user = user
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_IS_ADMIN: (state, bool) => {
      state.isAdmin = bool
    },
    SET_WS: (state, ws) => {
      state.ws = ws;
    },
    SET_VERSION: (state, v) => {
      state.version = v
    },
    SET_IS_TAX: (state, bool) => {
      state.isTax = bool
    },
    SET_USER_STATUS: (state, status) => {
      state.status = status
    },
  },
  actions: {
    // 用户名登录
    async userLogin({ commit }, userInfo) {
      const { data } = await user_login(userInfo);
      if(data.errorCode === 0){
        const { token, deptName, status } = data.data;
        localStorage.setItem("token", token);
        localStorage.setItem("deptName", deptName || '');
        commit("SET_USER_STATUS", status);
        return data.data;
      }
    },
    async getUserInfo({ commit, state }) {
      //代激活状态
      if(state.status == 2) return;          
      const { data } = await user_info_get();
      if(data.errorCode === 0){
        const { isAdmin, avatar, name, version, roles } = data.data;
        if(avatar){
          if(state.ws) state.ws.close();
          webSocket(avatar, ({ data: ws_avatar, ws }) => {
            commit("SET_WS", ws);
            commit("SET_AVATAR", ws_avatar? 'data:image/jpeg;base64,' + ws_avatar: '');
          }, err => {})
        }
        commit("SET_VERSION", version.toLowerCase());
        commit("SET_NAME", name);
        commit("SET_IS_ADMIN", isAdmin);
        commit("SET_USER", data.data);
        commit("SET_IS_TAX", roles.indexOf(3) != -1);
      }
    },
    async setAvatar({ commit, state  }, avatar){
      commit("SET_AVATAR", avatar);
    },
    // 登出
    async userLogOut({ commit, state }) {
        // localStorage.clear();
        const { data } = await user_logout();
        if(data.errorCode === 0){
          localStorage.removeItem("deptName");
          localStorage.removeItem("token");
        }
        if(state.ws) state.ws.close();
    },
    // 前端 登出
    async fedLogOut({ commit }) {
      localStorage.removeItem("deptName");
      localStorage.removeItem("token");
    }
  }
}

export default user
```



##### store/app.js 

项目的一些公用功能，无需在更改



##### store/permission.js

路由根据权限生成。

动态菜单根据日本年度改变。

```javascript
import { asyncRouterMap, constantRouterMap } from '@/router'
import moment from 'moment';
import Layout from '@/views/layout/Layout';
import user from './user'
import i18n from '@/lang'

function setMenu(title, path){
  return {
    path: `/${path}`,
    component: Layout,
    redirect: `${path}`,
    meta: { title: title, icon: 'iconfont ali-wendang' },
    children: [
        {
            path: `/${path}`,
            name: `${path}`,
            component: () => import('@/views/files'),
            meta: { title: title, noCache: false }
        }
    ]
  }
}

function getSectionMenu(){
  return new Promise( resolve => {
    let month = moment().format("M");
    let year = moment().year();
    let list = [];
    const upDoc   = i18n.t("route.upStage");
    const downDoc = i18n.t("route.downStage");
    const upDateStart   = '-04-01 00:00:00';//上半年区间头
    const upDateEnd     = '-09-30 23:59:59';//上半年区间尾
    const downDateStart = '-10-01 00:00:00';//下半年区间头
    const downDateEnd   = '-03-31 23:59:59';//下半年区间尾
    const beforeDate    = '2000-01-01 00:00:00';
    let before;
    let mapMenu = {};

    if(month >= 3 && month <= 9){//上半年
      list = [
        {//上期度
          title: year+ upDoc,
          path: year+ 'up',
          params: {startTime: year + upDateStart, endTime: year + upDateEnd}
        },
        {
          title: (year -1)+ downDoc,
          path: (year -1)+ 'down',
          params: {startTime: (year-1) + downDateStart, endTime: year + downDateEnd}
        },
        {//上期度
          title: (year-1) + upDoc,
          path: (year-1) + 'up',
          params: {startTime: (year-1) + upDateStart, endTime: (year-1) + upDateEnd}
        },
        {
          title: (year - 2) + downDoc,
          path: (year - 2) + 'down',
          params: {startTime: (year-2) + downDateStart, endTime: (year-1) + downDateEnd}
        }
      ]
      before = `${year-2}-06-30before`;
      mapMenu[`/${before}`] = {startTime: beforeDate, endTime: (year-2) + upDateEnd};
    }else{//下半年
      list = [
        {//下期度
          title: year+ downDoc,
          path: year+ 'down',
          params: {startTime: year + downDateStart, endTime: (year + 1) + downDateEnd}
        },
        {
          title: year+ upDoc,
          path: year+ 'up',
          params: {startTime: year + upDateStart, endTime: year + upDateEnd}
        },
        {
          title: (year -1)+ downDoc,
          path: (year -1)+ 'down',
          params: {startTime: (year-1) + downDateStart, endTime: year + downDateEnd}
        },
        {
          title: (year-1) + upDoc,
          path: (year-1) + 'up',
          params: {startTime: (year-1) + upDateStart, endTime: (year-1) + upDateEnd}
        }
      ]
      before = `${year-2}-12-31before`;
      mapMenu[`/${before}`] = {startTime: beforeDate, endTime: (year-1) + downDateEnd};
    }
    let asyncMenu = list.map(item => {
      mapMenu[`/${item.path}`] = item.params;
      return setMenu(item.title, item.path)
    });
    asyncMenu.push(setMenu('earlier', before));
    resolve({asyncMenu, mapMenu});
  })
}

const permission = {
  state: {
    routers: constantRouterMap,
    addRouters: [],
    routerLoadDone: false,
    cacheMenu: {}
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      state.routers = constantRouterMap.concat(routers)
      state.routerLoadDone = true;
    },
    SET_CACHE_MENU: (state, menu) => {
      state.cacheMenu = menu
    },
  },
  actions: {
    async generateRoutes({ commit }) {
      let {asyncMenu, mapMenu} = await getSectionMenu();
      let asyncRouter = [];
      if(!user.state.isAdmin){
        asyncRouterMap.forEach(item => {
          if(!item.admin){
            asyncRouter.push(item)
          }
        })
      }else{
        asyncRouter = asyncRouterMap;
      }
      let accessedRouters = asyncMenu.concat(asyncRouter);
      commit("SET_CACHE_MENU", mapMenu)
      commit('SET_ROUTERS', accessedRouters);
    },
  }
}

export default permission
```



##### stroe/url.js

登录跳转、用户激活等功能使用来url传参判断。为了便于管理，写在vuex里面

```javascript
const url = {
  state: {
    query: JSON.parse(localStorage.getItem('url_query'))
  },
  mutations: {
    SET_QUERY: (state, query) => {
      state.query = query
      localStorage.setItem('url_query', JSON.stringify(query))
    }
  },
  actions: {
    setUrlQuery({ commit }, args) {
      commit('SET_QUERY', args)
    },
    clearUrlQuery({ commit }) {
      commit('SET_QUERY', {})
    },
  }
}

export default url
```



##### stroe/parameter.js

对于一些页面跳转传参的使用。 vue传参有很多中，比如父子组件传参等。

还有一些隐式的传参方式。可以用此功能。

```javascript

const parameter = {
  state: {
    params: {},
    emptyDept: 0
  },

  mutations: {
    SET_PARAMS: (state, data) => {
      state.params = data
    },
    CLEAR_PARAMS: (state) => {
      state.params = {}
    },
    SET_EMPTY_DEPT: (state, num) => {
      state.emptyDept = num
    },
  },
  
  actions: {
    setParams({ commit }, data) {
      commit('SET_PARAMS', data)
    },
    clearParams({commit}){
      commit('CLEAR_PARAMS')
    },
    isEmptyDept({ commit }, num) {
      commit('SET_EMPTY_DEPT', num)
    },
  }
}

export default parameter
```



##### store/getters.js

vuex *getters*

```javascript
const getters = {
  sidebar: state => state.app.sidebar,
  language: state => state.app.language,
  size: state => state.app.size,
  device: state => state.app.device,
  theme: state => state.app.theme,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  user: state => state.user.user,
  token: state => state.user.token,
  version: state => state.user.version,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  isTax: state => state.user.isTax,
  isAdmin: state => state.user.isAdmin,
  introduction: state => state.user.introduction,
  setting: state => state.user.setting,
  permission_routers: state => state.permission.routers,
  addRouters: state => state.permission.addRouters,
  routerLoadDone: state => state.permission.routerLoadDone, //router是否已经加载完成

  cacheMenu: state => state.permission.cacheMenu, //动态渲染的菜单
  queryParams: state => state.parameter.params,// 页面跳转 携带报文
  emptyDept: state => state.parameter.emptyDept,// 部门列表是否空

  urlQuery: state => state.url.query, //url 参数
}
export default getters
```



##### store/index.js

modules下所有文件自定解析定义。 无需在更改

```javascript
import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'

Vue.use(Vuex)

const modulesFiles = require.context('./modules', true, /\.js$/)

const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

const store = new Vuex.Store({
  modules,
  getters
})

export default store
```

