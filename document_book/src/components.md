### 公共组件库



> 组件库部分组件已经高度封装，无需在做处理。可以按照目前现有的调用即可。
>
> 如若新增公用组件，可以此包中增加。



##### BackToTop

回到最顶端组件，结合分页组件一起使用。目前屏蔽掉



##### Breadcrumb

面包屑封装



##### LangSelect

国际化选择器



##### Pagination

分页组件



##### Screenfull

全屏组件



##### SizeSelect

全局空间大小选择器



##### Switch

switch选择器



##### ThemePicker

主题选择器，已作废



##### ToolButton

工具栏（按钮）组件



##### UploadCsv

csv上传组件