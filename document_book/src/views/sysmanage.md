### 系统设定

![](..\..\images\sysmanage.jpg)



##### index.vue

```javascript
<script>
import { system_add, system_get, system_vertify, system_rate } from '@/api/system'; // 接口文件
import { 
    SetType as SetTypeEnum,
    StorageType as StorageTypeEnum
} from '@/utils/Enum/FEnum'; // 服务端枚举

export default {
    data() {
        return {
            SetTypeEnum, // 枚举注册到data属性里， 用于 在 Dom层使用
            loading: false, // loading
            options: [ // 存储方式，默认配置列表
                {
                    value: StorageTypeEnum.smb,
                    label: 'SMB'
                }
            ],
            form1: { type: StorageTypeEnum.smb }, //存储设定
            form2: {}, //邮件设定
            form3: {}, //汇率设定
            form4: {}, //税率设定
            btnEnable: {// 按钮状态
                form1: { disabled: true, loading: false },
                form2: { disabled: true, loading: false },
            }
        }
    },
    methods: {
        async submit(name, setType) { //点击保存按钮后调用。
            const init = code => {
                if (code === 0) {
                    this.$message.success(this.$t("sysmanage.msg.save"));
                    this.initList();
                }
            }
            const reset = () => {
                this.loading = false;
                this.btnEnable = {
                    form1: { disabled: true, loading: false },
                    form2: { disabled: true, loading: false },
                }
            }
            try {
                this.loading = true;   //点击后loading
                let content = this.rmEmpty(this[name]);
                const { email, storage, exchangeRate, taxRate } = SetTypeEnum;
                const systemSets = []
                if ([email, storage].includes(setType)) {
                    let { data } = await system_add({ setType, type: this[name].type, content }); //获取数据，返回写入数据
                    init(data.errorCode);
                }
                if ([exchangeRate, taxRate].includes(setType)) {
                    let { data } = await system_rate({ setType, type: this[name].type, content }); //获取数据，返回写入数据
                    init(data.errorCode);
                }
                reset();
            } catch (error) {
                reset();
            }
        },
        rmEmpty(obj) {//移除掉 对象里 空的 key/value
            let content = {};
            for (const key in obj) {
                if (obj.hasOwnProperty(key)) {
                    const item = obj[key];
                    if (item) {
                        content[key] = item;
                    }
                }
            }
            return content;
        },
        async test(name, setType) {  //点击测试按钮后调用，1.获取数据，2.调服务端验证的接口，并且返回data
            try {
                this.btnEnable[name].loading = true;
                let content = this.rmEmpty(this[name]);
                let { data } = await system_vertify({ setType, type: this[name].type, content });
                this.btnEnable[name].loading = false;
                if (data.errorCode == 0) {  //2.判断正确后  返回errorcode=0  
                    this.btnEnable[name].disabled = false; //3.连接成功后保存按钮亮起
                    this.$message.success(this.$t("sysmanage.msg.test"));
                }
            } catch (error) {
                this.btnEnable[name].loading = false;
            }
        },
        validate(name, check, setType) {  //判断连接成功后保存按钮亮起的条件
            this.$refs[name].validate(bool => {
                if (bool) {
                    check == 'save' ? this.submit(name, setType) : this.test(name, setType);
                }
                return bool;
            })
        },
        async initList() {
            let { data } = await system_get();  //渲染到页面上
            if (data.errorCode === 0) {
                data.data.forEach(item => {
                    switch (item.setType) {
                        case 2:
                            this.form1 = { ...item.content, type: item.type };
                            break;
                        case 1:
                            this.form2 = item.content;
                            break;
                        case 3:
                            this.form3 = item.content;
                            break;
                        default:
                            this.form4 = item.content;
                            break;
                    }
                })
            }
        }
    },
    created() {// 钩子、页面加载前触发
        this.initList();
    }
}
</script>
```

