### 用户管理

![](..\..\images\usermanage.jpg)



##### index.vue

```javascript
<script>
import { operator_delete, operator_page, operator_count } from '@/api/operator';// 接口文件
import { mapGetters } from "vuex";
import {
    ToolButton as toolBtn, // 工具栏组件
    Pagination as paginationTemp, // 分页组件
    UploadCsv as uploadCsv, // 导入组件
} from '@/components';
import { UserStatus as UserStatusEnum } from '@/utils/Enum/FEnum';// 服务端枚举
import { RouterToEnum } from '@/utils/Enum/VEnum';// 前端枚举

export default {
    components: {// 组件注册
        uploadCsv,
        paginationTemp,
        toolBtn
    },
    data() {
        return {
            UserStatusEnum, // 枚举
            isActivate: false, // 用户是否激活判断
            loading: false, // loading
            btnEnable: false, // 按钮 状态
            delEnable: false, // 删除按钮 状态
            listQuery: { page: 0, count: 10 }, // 查询参数
            tableData: [], // 表格数据
            multipleSelection: [], // 表格行选择 数据
            page: { current: 1, total: 0 }, // 分页数据
            defaultProps: {
                label: 'name'
            },
        }
    },
    computed: {
        ...mapGetters(["user"]),
        roleEnum() {// 用户角色 枚举文件
            return require("./temp/conf")['roleEnum' + this.$t('lang.type')];
        },
        powerList() {// 用户权限 枚举文件
            return require("./temp/conf")['powerList' + this.$t('lang.type')];
        }
    },
    methods: {
        searchChange(key, e) {// 搜索输入框 改变时 触发
            if (!e) delete this.listQuery[key];
        },
        handleSelectionChange(val) {// 表格行选择时 触发
            this.multipleSelection = val;
            this.btnEnable = val.length === 1;
            this.delEnable = val.length > 0;
            this.isActivate = val.length === 1 && val[0].status == UserStatusEnum.inactivated;
        },
        upload() {// 点击导入用户时 触发
            this.$refs.uploadCsv.isShow = true;
        },
        checkBoxHandler(bool) {// 未激活用户 checkbox 触发函数
            this.listQuery.activated = bool
            this.initList()
        },
        async del() {// 删除
            try {
                const res = await this.$confirm(this.$t('usermanage.msg.confirm'));
                if (res == "confirm") {
                    let delslef = false;
                    let ids = []
                    for (let i = 0; i < this.multipleSelection.length; i++) {
                        const item = this.multipleSelection[i];
                        if (item._id == this.user._id) {
                            delslef = true;
                        }
                        ids.push(item._id);
                    }
                    if (delslef) {
                        this.$message.warning(this.$t('usermanage.msg.warning'));
                        return;
                    }
                    this.loading = true;
                    let { data } = await operator_delete({ suids: ids });
                    this.loading = false;
                    this.$message.success(this.$t('usermanage.msg.del'));
                    this.initList();
                }
            } catch (error) {
                this.loading = false;
            }
        },
        edit() {// 编辑
            this.$store.dispatch("setParams", this.multipleSelection[0])
            this.$router.push({ path: "/updateuser", query: { to: RouterToEnum.edit } })
        },
        activate() {// 激活
            this.$store.dispatch("setUrlQuery", { c: this.multipleSelection[0]._id })
            this.$router.push({ path: "/updateuser", query: { to: RouterToEnum.activate } })
        },
        openAdd() {// 添加
            this.$router.push({ path: "/updateuser", query: { to: RouterToEnum.add } })
        },
        search() {// 搜索
            this.initList();
        },
        async initList() {// 初始化数据
            try {
                this.loading = true;
                const [{ data: page }, { data: count }] = await Promise.all([operator_page(this.listQuery), operator_count(this.listQuery)]);
                this.page.total = count.data.count;
                this.loading = false;
                this.tableData = page.data;
            } catch (error) {
                this.loading = false;
            }
        }
    },
    created() {// 钩子、页面加载前触发 
        this.initList();
        this.$store.dispatch("clearParams");
        this.$store.dispatch("clearUrlQuery");
    }
}
</script>
```



![](..\..\images\usermanage_add.jpg)



##### temp/conf.js

> 用户管理 配置文件

```javascript
import { UserRole, UserAccess } from "@/utils/Enum/FEnum";


/**
 * businesser: 1 普通用户
 * sealer: 2 盖章担当
 * taxer: 3 免税处理担当
 * archiver: 4 存档处理担当
 * sealSaver: 5 压印担当
 */
const { businesser, sealer, taxer, archiver, sealSaver } = UserRole;

//管理员
export const ADMIN = 99;

/**
 * view: 1 查看|预览
 * update: 2 创建|编辑
 * audit: 3 审查
 * seal: 4 盖章
 * tax: 5 免税处理
 * archive: 6 存档
 * delete: 7 删除
 */
const { view, update, audit, seal, tax, archive, delete: _delete } = UserAccess;

export const roleListZh = [
    {
        id: businesser,
        name: '一般员工',
        power: [view, update],
    },
    {
        id: sealer,
        name: '合同盖章担当',
        power: [view, update, seal]
    },
    {
        id: taxer,
        name: '免税处理担当',
        power: [view, update, tax]
    },
    {
        id: archiver,
        name: '存档处理担当',
        power: [view, update, archive, _delete]
    },
    {
        id: sealSaver,
        name: '押印担当',
        power: [view, update, seal]
    },
    {
        id: ADMIN,
        name: '管理员',
        power: [view, update, _delete],
        isAdmin: true
    }
]
export const roleListJa = [
    ...
]
export const roleListEn = [
    ...
]

export const powerListZh = {
    [view]:{
        id: view,
        label: '文档查看',
    },
    [update]: {
        id: update,
        label: '文档创建、编辑',
    },
    [audit]: {
        id: audit,
        label: '文档审查、承认',
    },
    [seal]: {
        id: seal,
        label: '盖章处理',
    },
    [tax]: {
        id: tax,
        label: '免税处理',
    },
    [archive]: {
        id: archive,
        label: '存档处理',
    },
    [_delete]: {
        id: _delete,
        label: '文档删除',
    }
}
export const powerListJa = {
    ...
}
export const powerListEn = {
    ...
}

export const roleEnumZh = {
    [businesser]: '一般员工', [sealer]: '合同盖章担当', [taxer]: '免税处理担当', [archiver]: '存档处理担当', [sealSaver]: '押印担当', [ADMIN]: '管理员'
}
export const roleEnumJa = {
    [businesser]: '一般従業員', [sealer]: '契約捺印担当', [taxer]: '免税処理担当', [archiver]: 'アーカイブ処理担当', [sealSaver]: '捺印担当', [ADMIN]: '管理者'
}
export const roleEnumEn = {
    [businesser]: 'General staff', [sealer]: 'Contract Sealed', [taxer]: 'Tax-free processing', [archiver]: 'Filing Handling', [sealSaver]: 'Imprint lithography', [ADMIN]: 'Admin'
}
```



##### temp/index.vue

```javascript
<script>
import { user, operator } from '@/api';// 接口文件
import { cascaderHandler, userRoleHandler } from '@/mixins';// mixins
import { Switch as vswitch } from '@/components';// switch组件
import { LocalStorageEnum as LSEnum, RouterToEnum } from '@/utils/Enum/VEnum';// 前端枚举
import { UserAccess } from "@/utils/Enum/FEnum";// 服务端枚举

export default {
    mixins: [cascaderHandler, userRoleHandler], // mixins 继承
    components: {
        vswitch // 组件注册
    },
    data() {
        return {
            loading: false, // loading
            form: { isLeader: false, roles: [], permissions: [], isAdmin: false }, // 表单数据
            isEdit: false, // 是否为编辑
            defRoles: [], // 默认角色
            defPowerList: [] // 默认权限
        };
    },
    computed: {
        title() {// 小标题
            const queryId = this.$route.query.c || this.urlQuery.c
            if (queryId) {
                return this.$t('usermanage.temp.title.activate')
            }
            if (!this.queryParams.name) {
                return this.$t("usermanage.temp.title.add");
            } else {
                return this.$t("usermanage.temp.title.edit");
            }
        }
    },
    methods: {
        back() {// 返回上一层
            this.$router.replace({ path: '/usermanage' });
        },
        async submit() {// 表单提交
            let _permissions = this.defPowerList.map(item => item.id)
            this.form.permissions = _permissions;
            let _roles = [];
            let set = new Set();
            this.defRoles.map(item => {
                set.add(item.id)
            });
            for (const item of set) {
                if (item != 99) {//管理员 不填入 角色里面
                    _roles.push(item)
                }
            }
            this.form.roles = _roles;
            //表单提交
            try {
                this.loading = true;
                if (!this.queryParams.name) {
                    let data = await operator.operator_add(this.form);
                    this.$message.success(this.$t("usermanage.temp.msg.add"));
                } else {
                    let { data } = await operator.operator_edit(this.form._id, this.form);
                    if (this.user._id == this.form._id) {
                        this.$store.dispatch("getUserInfo");
                        localStorage.setItem(LSEnum.token, data.data.token);
                    }
                    this.$message.success(this.$t("usermanage.temp.msg.edit"));
                }
                this.back();
                this.loading = false;
            } catch (error) {
                this.loading = false;
            }
        },
        initData() {// 初始化数据
            const { view, update, delete: _delete } = UserAccess;
            if (!this.queryParams.name) {
                this.defRoles.push(this.roleList[0])
                this.defPowerList.push(this.powerList[view], this.powerList[update])
            } else {
                if (this.queryParams.isAdmin) {
                    this.form.isAdmin = true;
                    //默认 roleList 最后一位是 管理员
                    this.defRoles.push(this.roleList[this.roleList.length - 1])
                    //默认 powerList 管理员 文档删除权限
                    this.defPowerList.push(this.powerList[_delete])
                }
                this.isEdit = true;
                this.form = this.queryParams;
                for (let i = 0; i < this.roleList.length; i++) {
                    const item = this.roleList[i];
                    for (let j = 0; j < this.form.roles.length; j++) {
                        const id = this.form.roles[j];
                        if (item.id === id) {
                            this.defRoles.push(item);
                            continue;
                        }
                    }
                }
                this.checkBoxHandler(this.defRoles);
                this.selectData = {
                    show: false,
                    name: this.queryParams.deptName
                }
            }
            const { edit, activate } = RouterToEnum;
            if (this.$route.query.to == activate && !this.urlQuery.c) {
                this.$router.go(-1);
            }
            if (this.$route.query.to == edit && !this.queryParams.name) {
                this.$router.go(-1);
            }
        },
        async getUserById(uid) {// 根据uid获取用户信息
            const { data } = await user.user_detail({ uid });
            if (data.errorCode == 0) {
                if (data.data.length === 0) {
                    this.$alert(this.$t("activate.activated"), { type: 'warning', showClose: false }).then(() => {
                        this.back();
                    })
                } else {
                    await this.$store.dispatch("setParams", data.data[0]);
                    this.initData();
                }
            }
        }
    },
    created() {// 钩子、页面加载前触发
        const queryId = this.$route.query.c || this.urlQuery.c
        //激活
        if (queryId) {
            this.getUserById(queryId);
        } else {
            this.initData();
        }
    }
};
</script>
```

