### 我的文档



![](..\..\images\myfiles.jpg)



##### index.vue

```javascript
<script>
import { user, doc, file } from '@/api';// 接口文件
import readTemp from '@/views/files/readTemp'; // 预览组件
import searchTemp from '@/views/files/searchTemp'; // 搜索栏 组件
import { mapGetters } from 'vuex';
import { renderSize, fmtSearchTime } from '@/utils'; // 工具
import { fileI18nHandler, sortChange } from '@/mixins'; // mixins
import { 
    MyDocOpt,
    DocumentState as DocStateEnum,
    ReqOptType as ReqOptTypeEnum,
    ResOptType as ResOptTypeEnum,
    UserRole as RoleEnum
} from '@/utils/Enum/FEnum'; // 服务端枚举
import { DomArgsEnum, RouterPathEnum, LocalStorageEnum as LSEnum, RouterToEnum } from '@/utils/Enum/VEnum'; // 前端枚举
import {
    ToolButton as toolBtn,
    Pagination as paginationTemp,
} from '@/components'; // 工具栏、分页 组件
import printList from './printList'; // 打印预览 组件
import { fmtDate } from '@/filters'; // 过滤器

/* myfiles */
export default {
    components: { // 组件注册
        readTemp,
        searchTemp,
        paginationTemp,
        toolBtn,
        printList
    },
    filters: { fmtDate }, // 过滤器注册
    data() {
        return {
            RouterPathEnum, // 路由枚举 声明，用于Dom使用
            RouterToEnum, // 枚举 用于Dom 使用
            DomArgsEnum, /* toolBar形参枚举 */
            toolBarShow: { /* 工具栏展示判断 */
                dealt: false,
                making: false,
                created: false
            },
            loading: false, // loading
            listQuery: { page: 0, count: 10, category: null, type: null }, // 查询参数
            pageType: null, // 当前页面的具体板块分类 => 待我处理、 做成中、 我创建的
            tableData: [], // 表格数据
            page: { current: 1, total: 0 }, // 分页数据
            btnEnable: false, /* 按钮启用/禁用 */
            isSave: false, // 提交保管
            isTax: false,//申请免税处理
            isEdit: false, //是否可编辑
            btnFinal: false, //最终存档
            isBatchSave: false,//是否批量保管
            isBatchTax: false,//是否批量去免税
            taxList: [], // 免税处理 缓存列表
            saveList: [], // 保管处理 缓存列表
            form: {}, // 当前选中对象
            footer: { totalCount: 0, totalAmount: 0, details: [] }, // 页脚 数据
            multipleSelection: [], // 行选中数据 列表
        }
    },
    mixins: [fileI18nHandler, sortChange], // mixins
    computed: {
        ...mapGetters([
            'isAdmin',
            'queryParams',
            'user'
        ]),
        resOptType() {// 去处理 类型配置文件
            return require("./conf")['resOptType' + this.$t('lang.type')];
        },
        isNormal() {//是否为普通用户
            const { isAdmin, roles } = this.user;
            if (isAdmin) {
                return false;
            }else {
                return roles.includes(RoleEnum.businesser);
            }
        }
    },
    methods: {
        renderSize, // 文件大小 格式化 添加单位
        dispatchClick(arg){//分发toolBar 点击事件
            const { del, batchTax, batchSave, print } = DomArgsEnum;
            const { add, copy, edit, tax, final, reqSave } = RouterToEnum;
            switch (arg) {
                case add:
                case copy:
                case edit:
                    this.$router.push({ path: RouterPathEnum.addTemp, query: { to: arg } })
                    break;
                case tax: /* 请求免税处理 */
                    this.$router.push({ path: RouterPathEnum.reqTaxTemp, query: { to: arg } })
                    break;
                case reqSave: /* 发起 提交保管 || 最终存档*/
                case final:
                    this.$router.push({ path: RouterPathEnum.lendTemp, query: { to: arg } })
                    break;
                case del: /* 删除文档 */
                    this.deleteFile()
                    break;
                case batchTax: /* 批量处理 */
                case batchSave:
                    this.batchHandler(arg)
                    break;
                case print:
                    this.printHandler();
                    break;
                default:
                    break;
            }
        },
        async printHandler(){// 点击打印按钮，获取 当前选中行 的附件列表
            let { _id, viewPos, uploadPos } = this.multipleSelection[0];
            console.log(viewPos, 'viewPos')
            // viewPos = uploadPos; 临时为了测试 所以才吧 uploadPos 赋值给 viewPos
            if(!viewPos || viewPos.length == 0){
                this.$message.warning('当前文档文件正在做格式转换,请稍后再试...');
                return;
            }
            await this.$store.dispatch('cacheLocalPrintJob', _id);
            this.form = this.multipleSelection[0]
            this.$refs.printList.isShow = true;
        },
        async choicePrint([row, { fileName, storagePath }]){ // 根据选中附件， 跳转到 pdf 预览页面
            try {
                const { _id } = row;
                // this.loading = true;
                const { data: view } = await file.file_view({ docId: _id, fileName, storagePath });
                console.log(view,' view')
                this.loading = false;
                if(view.errorCode == 0){
                    const secretKey = view.data;
                    const { data: urlData } = await file.file_pdf(secretKey);
                    // const pdfPublicUri = `/api/file/pdf?key=${secretKey}`;
                    console.log(urlData, 'urlddata')
                    const { value } = urlData;
                    console.log(value, 'value')
                    // const src = 'https://cdn.filestackcontent.com/wcrjf9qPTCKXV3hMXDwK'
                    const job = await this.$store.dispatch('cacheLocalPrintJob', { _id, fileName, src: value })
                    if(job){
                        const { href } = this.$router.resolve({ path: '/pdfview' });
                        window.open(href, '_blank');
                    }
                }
            } catch (err){
                this.loading = false;
            }
        },
        resOptHandler(row) {//用户去做处理 功能
            const { archive } = ResOptTypeEnum;
            this.$store.dispatch("setParams", row);
            if (row.resOptType === archive) {//去存档
                this.$router.push({ path: RouterPathEnum.lendTemp, query: { to: RouterToEnum.resSave } })
                return
            }
            this.$router.push({ path: RouterPathEnum.resOptTemp })
        },
        async batchHandler(arg) {// 批量去处理， 保管，免税
            const { taxes, archives } = ResOptTypeEnum;
            try {
                const res = await this.$confirm(this.$t("files.approve.msg.batch"));
                if (res == "confirm") {
                    //type 免税处理:10, 存档:11
                    let type = null, docIds = []
                    if (arg == DomArgsEnum.batchTax) {
                        type = taxes;
                        docIds = this.taxList;
                    } else {
                        type = archives;
                        docIds = this.saveList;
                    }
                    this.loading = true;
                    try {
                        const { data } = await doc.doc_batch({ type, docIds })
                        if (data.errorCode == 0) {
                            this.$message.success(this.$t('files.approve.msg.success'));//操作成功
                            this.initList();
                        }
                    } catch (error) {
                        this.loading = false
                    }
                }
            } catch (error) {
                this.loading = false;
            }
        },
        handleSelectionChange(val) {// 行选中 触发
            this.multipleSelection = val;
            //判断批量免税和存档 
            //resOptType 4 去免税 5 出存档
            if (this.$route.path == RouterPathEnum.dealtView) {
                this.taxList = [];
                this.saveList = [];
                let isBatch = true;
                const { tax, archive } = ResOptTypeEnum;
                for (let i = 0; i < val.length; i++) {
                    const item = val[i];
                    const batchs = [tax, archive];
                    if (!batchs.includes(item.resOptType)) {
                        isBatch = false;
                        break
                    }
                    if (item.resOptType == tax) {
                        this.taxList.push(item._id)
                    } else {
                        this.saveList.push(item._id)
                    }
                }
                const taxLen = this.taxList.length;
                const saveLen = this.saveList.length;
                if (isBatch) {
                    if (taxLen > 0 && saveLen > 0) {
                        this.isBatchTax = false;
                        this.isBatchSave = false;
                    } else {
                        this.isBatchTax = taxLen > 0 && saveLen == 0;
                        this.isBatchSave = taxLen == 0 && saveLen > 0;
                    }
                } else {
                    this.isBatchTax = false;
                    this.isBatchSave = false;
                }
            }

            this.btnEnable = val.length === 1;
            if (val.length === 1) {
                const { tax, archive } = ReqOptTypeEnum;
                const { making, auditing, denied, reject } = DocStateEnum;
                if (val[0].reqOptType && val[0].reqOptType == tax) {
                    this.isTax = true;
                }
                if (val[0].reqOptType && val[0].reqOptType == archive) {
                    this.isSave = true;
                }
                if (val[0].state == making || val[0].state == reject) {
                    this.isEdit = true;
                }

                const isFianlStates = [making, auditing, denied, reject]
                if (!isFianlStates.includes(+val[0].state)) {
                    this.btnFinal = true;
                }
                this.$store.dispatch("setParams", val[0]);
            } else {
                this.$store.dispatch("clearParams");
                this.isSave = false;
                this.isTax = false;
                this.isEdit = false;
                this.btnFinal = false;
            }
        },
        async rowClick(row, column, event) {// 行点击时 触发
            if (!column.label || column.property == "createTime" || column.type == "selection") {
                return;
            }
            this.form = row;
            await this.$refs.readTemp.initRecords(row._id);
            this.$refs.readTemp.isShow = true;
        },
        async deleteFile() {// 删除文件
            try {
                const res = await this.$confirm(this.$t('files.table.msg.confirm'));
                if (res == "confirm") {
                    this.loading = true;
                    let { data } = await doc.doc_delete(this.queryParams._id);
                    this.loading = false;
                    if (data.errorCode === 0) {
                        this.$message.success(this.$t('files.table.msg.del'));
                        this.initList();
                    }
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async initList() {// 初始化 列表数据
            this.loading = true;
            let query = fmtSearchTime(this.listQuery);
            const [{ data: page }, { data: count }] = await Promise.all([user.user_docs_pages(this.pageType, query), user.user_docs_count(this.pageType, query)]);
            this.page.total = count.data.totalCount;
            this.footer = count.data;
            this.loading = false;
            this.tableData = page.data;
        },
        initToolBar() {// 初始化 工具栏
            //判断功能 按钮是否展示
            const { dealtView, makingView, createdView } = RouterPathEnum;
            switch (this.$route.path) {
                case dealtView: /* 待我处理 */
                    this.toolBarShow.dealt = true;
                    this.pageType = MyDocOpt.dealt;
                    break;
                case makingView: /* 我做成中 */
                    this.toolBarShow.making = true;
                    this.pageType = MyDocOpt.making;
                    break;
                case createdView: /* 我创建的 */
                    this.toolBarShow.created = true;
                    this.pageType = MyDocOpt.created;
                    break;
            }
        },
    },
    created() {// 钩子、页面加载前 触发
        this.initToolBar();
        this.initList();
        this.$store.dispatch("clearParams");
    }
}
</script>
```



##### printList/index.vue

> 打印附件列表

```javascript
<script>
export default {
    data() {
        return {
            isShow: false,
        }
    },
    props: {
        form: {
            type: Object,
            default() { }
        }
    },
    methods: {
        choice(item){
            this.$emit("choice", [this.form, item]);
            this.isShow = false;
        }
    },
}
</script>
```



![](..\..\images\myfiles_approve.jpg)



##### approve/index.vue



```javascript
<script>
import { doc } from '@/api'; // 接口文件
import { mapGetters } from 'vuex';
import { 
    ReqOptType as ReqOptTypeEnum,
    ResOptType as ResOptTypeEnum,
    DocumentState as DocStateEnum
} from '@/utils/Enum/FEnum'; // 服务端枚举
import { fileI18nHandler } from '@/mixins'; // mixins
import { RouterToEnum, LocalStorageEnum as LSEnum } from '@/utils/Enum/VEnum'; // 前端枚举
import _ from 'lodash'; // 工具
import { fmtDate } from '@/filters'; // 过滤器

export default {
    filters: { fmtDate }, // 过滤器注册
    data() {
        return {
            DocStateEnum, // 枚举声明 用于在Dom上使用
            form: {}, // 表单数据
            approveUser: [] // 审查的用户列表
        }
    },
    mixins: [fileI18nHandler],
    computed: {
        ...mapGetters([
            'queryParams',
            'user'
        ]),
        title() {// 小标题
            if (this.$route.query.to == RouterToEnum.tax) {
                return this.$t('files.approve.taxTitle')
            }
            const resBtnOptType = require("./../conf")['resBtnOptType' + this.$t('lang.type')]
            return resBtnOptType[this.queryParams.resOptType - 1];
        },
    },
    methods: {
        back() { // 返回上一层
            this.$router.go(-1);
        },
        async downLoad(file) { // 附件下载
            window.location.href = `/api/doc/download?storagePath=${file.storagePath}&name=${file.fileName}`;
        },
        async submit() { // 表单提交
            //判断是否是免税处理
            if (this.$route.query.to == RouterToEnum.tax) {
                this.subTax();
                return;
            }
            try {
                const { _id, resOptType } = this.queryParams;
                this.loading = true;
                let { data } = await doc.doc_approve(_id, resOptType);
                this.loading = false;
                if (data.errorCode == 0) {
                    this.$message.success(this.$t('files.approve.msg.success'));//操作成功
                    this.$router.go(-1);
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async subReject(type) {//文书否决 或 保留
            try {
                const res = await this.$confirm(this.$t('files.approve.msg.confirm'));//确认操作
                if (res == "confirm") {
                    const { _id } = this.queryParams;
                    this.loading = true;
                    let { data } = await doc.doc_approve(_id, ResOptTypeEnum[type]);
                    this.loading = false;
                    if (data.errorCode == 0) {
                        this.$message.success(this.$t('files.approve.msg.success'));//操作成功
                        this.$router.go(-1);
                    }
                }
            } catch (error) {
                this.loading = false;
            }
        },
        initList() {// 页面初始化 数据
            this.form = this.queryParams;
        },
        async initRecords(type) {//查询 审核记录 或 出借记录
            try {
                const { _id } = this.queryParams;
                let { data } = await doc.doc_records(_id, type);
                if (data.errorCode == 0) {
                    this.approveUser = data.data
                }
            } catch (error) { }
        },
        async subTax() {//发起免税处理
            let list;
            try {
                let { category } = this.queryParams;
                let { data } = await doc.doc_process({ reqOptType: ReqOptTypeEnum.tax, documentCategory: category });
                if (data.errorCode === 0) {
                    let { taxLines } = data.data;
                    if (taxLines.length == 0) {
                        this.$message.warning(this.$t('files.approve.msg.warning'));//当前暂未设置免税处理担当
                        return;
                    }
                    list = taxLines;
                }
            } catch ({ errorCode, errorMsg }) {

            }
            let taxList = list.map(item => {
                let cache = {};
                if (item.agentUid) {
                    cache = { resUid: item.agentUid, resName: item.agentName }
                } else {
                    cache = { resUid: item.uid, resName: item.name }
                }
                return { ...cache, resDeptId: item.deptId, resOptType: item.resOptType }
            })
            let paths = taxList;
            let form = _.cloneDeep(this.form);
            form = _.omit(form, ['_id', '__v', 'createTime', 'uid'])
            
            const { _id, name, deptId } = this.user;
            let data = {
                reqUid: _id,
                reqName: name,
                reqDeptId: deptId,
                reqDeptName: localStorage.getItem(LSEnum.deptName),
            }
            const formData = {
                docId: this.form._id,
                docName: this.form.name,
                ...form,
                reqOptType: ReqOptTypeEnum.tax,
                paths,
                ...data
            }
            try {
                let { data } = await doc.doc_ignite(formData);
                if (data.errorCode == 0) {
                    this.$message.success(this.$t('files.approve.msg.success'));//操作成功
                    this.$router.go(-1);
                }
            } catch (error) {

            }
        },
    },
    created() {// 钩子、页面加载前触发
        //判断是否刷新页面
        if (!this.queryParams.name) {
            this.$router.go(-1);
            return;
        }
        this.initList();
        this.initRecords(1);//查询审核记录
    }
}
</script>
```



##### conf.js

> 我的文档下 配置文件

```javascript

/**
 * 响应操作类型
 * enum for ResOptType
 * @readonly
 * @enum {Number}
 */

export const resOptTypeZh = [
  '审查', '盖章前审查', '盖章处理', '免税处理', '保管处理', '出借', '归还', '保留', '否决'
]
export const resOptTypeJa = [
  ...
]
export const resOptTypeEn = [
  ...
]

/**
 * 操作页面 button 文案
 */
export const resBtnOptTypeZh = [
  '审查完毕', '做盖章前审查', '盖章完毕', '免税处理完毕', '存档完毕', '出借', '归还', '保留', '否决'
]
export const resBtnOptTypeJa = [
  ...
]
export const resBtnOptTypeEn = [
  ...
]
```

