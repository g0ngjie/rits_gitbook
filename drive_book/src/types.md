### 全局自定义TS声明文件



TS在一些包的引用时，无法找到目标路径，需要自定义一些声明。此处无需在处理。

```typescript
declare module "ant-design-vue/es/notification" {
  const Ant: any;
  export default Ant;
}
...
...
```

