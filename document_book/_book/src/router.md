### 路由



Vue路由配置文件

##### index.js / menu.js

```javascript
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/views/layout/Layout'
import menu   from './menu'

export const constantRouterMap = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true
  },
  {
    path: '/passmail',
    component: () => import('@/views/passmail'),
    hidden: true
  },
  {
    path: '/init',
    component: () => import('@/views/init'),
    hidden: true
  },
  ...
  ...
]

export default new Router({
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [
  ...menu,
  /**
   * 404页面默认是要添加到最底层，
   * 不然404之后的页面会因为 通配符的原因，
   * 页面刷新直接进入404页面。
   */
  { path: '*', redirect: '/404', hidden: true }
]

...
```

