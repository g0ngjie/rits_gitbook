# 概要



> 此手册仅限于梳理结构。
>
> 对于一些概念。相信实践会更有效率。
>
> 功能使用最好结合实际操作。



##### Book本地预览

本地启动的话需要预装 `http-server` 模块

```shell
npm install -g http-server
```

通过启动命令，然后在浏览器输入 `http://localhost:8080`，就可以查看

```javascript
http-server -p [port] [book_path] // cd到项目根目录下 http-server -p 8080 . 
```



##### 项目目录结构

```javascript

.
├── build					//webpack项目文件
├── config					//webpack配置文件，可以修改开发环境 代理、IP、端口号等
├── docker-compose.yml		//docker-compose 脚本配置文件，无需修改
├── favicon.ico				//favicon 项目图标
├── index.html				//主页，项目入口
├── Jenkinsfile				//jenkins配置文件
├── jsconfig.json			//vscode 对js提供路径支持等的功能文件
├── nginx					//nginx配置文件
├── package.json			//说明文件
├── README.md				
├── src						//source
│   ├── api					//接口文件
│   ├── App.vue				//根组件
│   ├── assets				//静态资源
│   ├── components			//公共组件
│   ├── filters				//过滤器
│   ├── lang				//国际化
│   ├── main.js				//主入口
│   ├── mixins				//mixins
│   ├── mock				//mock，忽略
│   ├── permission.js		//拦截器
│   ├── router				//路由
│   ├── store				//vuex
│   ├── styles				//公共样式
│   ├── utils				//工具包
│   └── views				//组件包，视图
└── static					//暂未用到

```



##### 启动

cd到项目根目录下

`yarn dev`  或者 `npm run dev`

##### 打包

`yarn build` 或者 `npm run build`

在根目录下面会生成 **dist** 文件夹，把里面的所有文件都放到 **服务端** 的静态资源访问路径下。

