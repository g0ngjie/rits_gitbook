### 接口文档目录

> 接口统一处理，引入自定义请求工具类**Request.js** 



##### Request.js

> 文件路径 @/utils/Request.js
>
> 是基于 [axios](https://github.com/axios/axios)[ ](https://github.com/axios/axios) 的封装
>
> 请求拦截、异常信息、请求头、连接超时等处理，统一设置
>



##### cfg.js

> api配置文件
>
> 打包时，是否需要前缀：/api

```javascript
export const PREFIX = process.env.NODE_ENV === "development" ? "/api" : "";
```



##### drive.js

> 驱动管理接口调用 示例

```javascript
import request from "@/utils/Request";
import { PREFIX } from "./cfg";

/**
 * 获取驱动列表
 * @param data
 */
export function drive_pages(data) {
  return request({
    url: `${PREFIX}/driver/drivers`,
    method: "post",
    data
  });
}

/**
 * 计算驱动列表个数
 * @param data
 */
export function drive_count(data) {
  return request({
    url: `${PREFIX}/driver/drivers/count`,
    method: "post",
    data
  });
}
```

