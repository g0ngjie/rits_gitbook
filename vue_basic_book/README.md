# 概要

##### 分支

* master 基础模板
* dev 参照**驱动管理**，添加一些基本功能。

##### 技术选型

> vue-cli@4、webpack@4、element-ui@2.13.0、eslint@6.7.2、scss

* **[vue-cli](https://cli.vuejs.org/zh/guide/)** 【了解】
  * Vue CLI 是一个基于 Vue.js 进行快速开发的完整系统。
  * 选择4的版本，性能会比 **合同管理** 优化要好一些。
  * **目录结构** 清晰。
* **[webpack](https://www.webpackjs.com/)** 【了解】
  * 4版本，官方宣传能够提升构建速度60%-98%
* **[element-ui](https://element.eleme.cn/#/zh-CN )**【开发需要看文档】
* **[eslint](https://eslint.bootcss.com/)**  【了解】
  * 引入了vue官方的默认配置
* **[scss](https://www.sass.hk/)** 【了解】 



##### 脚手架创建vue项目

> 全局安装vue脚手架

```shell
npm install -g @vue-cli
```

> 创建vue项目

```shell
vue create rits-vue-basic-template
```

![](images\vue_create.jpg)




##### 项目目录结构

```bash
.
├── babel.config.js				#babel配置文件，js代码向下兼容
├── dev.config.js			    #Vue配置文件，开发环境的 **ip，端口号，代理** 等
├── dist                        #yarn build 打包的静态资源
├── package.json                #项目说明文件
├── public                      #主页，项目入口
│   ├── favicon.ico
│   └── index.html
├── README.md                   #阅读指南
├── src                         
│   ├── App.vue                 #根组件
│   ├── main.js                 #入口文件
│   ├── api                     #接口文件
│   ├── assets                  #资源文件 图片等
│   ├── components              #公共组件库
│   ├── filters                 #过滤器
│   ├── icons                   #svg文件
│   ├── layout                  #布局组件
│   ├── mixins                  #多继承
│   ├── router                  #路由
│   ├── store                   #Vuex
│   ├── styles                  #全局共有样式库
│   ├── utils                   #工具包
│   └── views                   #组件包、视图
├── vue.config.js               #Vue配置文件
├── jsconfig.json               #vscode编辑器下js的显示引用文件
└── yarn.lock
```



##### 启动

> cd到项目根目录下
>

`yarn serve`  或者 `npm run serve`

##### 打包

> 在根目录下面会生成 **dist** 文件夹，把里面的所有文件都放到 **服务端** 的静态资源访问路径下。

`yarn build` 或者 `npm run build`

##### 压缩svg

> 在package.json里面，配置了压缩svg的命令。

`yarn svgo`

##### 缓存处理

> 当新建页面或者添加文件的时候。有时会遇到一些 *undefined* 或者文件不存在的问题。
>
> 这类情况下可通过下面三种方式。

* webpack通过`cache-loader` 会默认为 Vue/Babel/TypeScript 编译开启。文件会缓存在 `node_modules/.cache` 中。通过命令 `rm -rf node_modules/.cache` 或者手动在`node_modules`下找到`.cache`文件，删除掉。
* 通过快捷键`ctrl` + `shift` + `delete` 清理浏览器缓存。
* 通过快捷键 `ctrl` + `f5` 强制刷新浏览器。