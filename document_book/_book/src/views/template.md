### 模板管理

![](..\..\images\template.jpg)



##### index.vue

```javascript
<script>
/* 模板管理页面 */
import { doc } from '@/api'; // 接口文件
import { 
    DocumentCategory as DocCategoryEnum,
    StorageType as StorageEnum,
    UserRole as RoleEnum
} from '@/utils/Enum/FEnum'; // 服务端枚举
import { LocalStorageEnum as LSEnum } from '@/utils/Enum/VEnum'; // 前端枚举
import { mapGetters } from 'vuex';
import { renderSize } from '@/utils'; // 工具
import { fmtDate } from '@/filters'; // 过滤器
import { sortChange } from '@/mixins'; // mixins
import {
    ToolButton as toolBtn, // 工具栏组件
    Pagination as paginationTemp, // 分页组件
} from '@/components';

export default {
    components: { // 组件注册
        paginationTemp,
        toolBtn
    },
    filters: { fmtDate }, // 过滤器注册
    data() {
        return {
            loading: false, // loading
            listQuery: { page: 0, count: 10 }, // 查询参数
            page: { current: 1, total: 0 }, // 分页数据
            tableData: [], // 表格数据
            form: {}, // 上传模板 使用的变量
            btnEnable: false, // 按钮状态
            uploadData: { // 上传的参数 data 属性
                storage: StorageEnum.smb,
                documentCategory: DocCategoryEnum.template, 
            },
            headers: {token: localStorage.getItem(LSEnum.token)}, // 上传 添加 头部token
            multipleSelection: [] // 表格选中 数据
        }
    },
    computed: {
        ...mapGetters(['user', 'isAdmin']),
        isPower() { // 判断是否由删除权限
            if (this.isAdmin || this.user.roles.includes(RoleEnum.archiver)) {
                return true;
            } else {
                return false;
            }
        }
    },
    mixins: [sortChange], // mixin 继承
    methods: {
        renderSize, // 文件大小。显示 单位
        handleSelectionChange(val){ // 表格行选则 事件
            this.multipleSelection = val;
            this.btnEnable = val.length > 0;
        },
        downFiles({ storagePath, name }){ //下载
            window.location.href = `/api/doc/download?storagePath=${storagePath}&name=${name}`;
        },
        async deleteFile(){//批量删除
            try {
                const res = await this.$confirm(this.$t("template.msg.confirm"));
                if (res == "confirm") {
                    this.loading = true;
                    const ids = this.multipleSelection.map(item => item._id).join();
                    let { data } = await doc.doc_template_delete({templateIds: ids});
                    this.loading = false;
                    this.$message.success(this.$t("template.msg.success"));
                    this.initList();
                }
            } catch (error) {
                this.loading = false;
            }
        },
        uploadSuccess({ data, errorCode }, file, fileList) {// 上传成功 回调
            if (errorCode === 0) {
                const { fileName: name, storagePath  } = data;
                this.createTemplate({
                    name, 
                    storagePath,
                    size: this.form.size,
                    uid: this.user._id
                })
            }
        },
        beforeUpload({size, name}){// 上传前 回调
            const index = name.lastIndexOf(".");
            if(index == -1){
                this.$message.warning("请上传正确的文档格式");
                return false;
            }
            const ext = name.substr(index+1);
            const cache = [
                "doc", "docx", 
                "xlsx", "xls",
                'txt', 'TXT', 
                'pdf', 'PDF', 
                'ppt', 'pptx', 
                'pptm', 'csv', 
                'zip'
                ];
            if(cache.indexOf(ext) == -1){
                this.$message.warning("请上传正确的文档格式");
                return false;
            }
            if(size / 1024 > 10240){//10240 = 10M 
                this.$message.warning('文件不能大于10M');
                return false;
            }
            this.form.size = size;
        },
        searchChange(key, e) {// 日期排序 函数
            if (!e) delete this.listQuery[key];
        },
        async createTemplate(form){// 调用 服务端 添加模板 接口
            this.loading = true;
            try {
                const { data } = await doc.doc_template(form);
                this.loading = false;
                if(data.errorCode == 0){
                    this.initList();
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async initList(){// 页面初始化 获取数据
            this.loading = true;
            const [{ data: page }, { data: count }] = await Promise.all([doc.doc_template_page(this.listQuery), doc.doc_template_count(this.listQuery)]);
            this.page.total = count.data;
            this.loading = false;
            this.tableData = page.data;
        }
    },
    created(){// 钩子、页面加载前触发
        this.initList();
    }
}
</script>
```

