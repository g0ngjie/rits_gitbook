### 全局公用样式库



> 定义了一些全局的样式，取消html默认比较丑的样式。 调整整体宽高为100%等；
>
> 为了语义化和结构清晰，在该包下把常用样式拆分成了各个小模块。



##### index.scss

> 主文件，引入其他默认样式

```scss
@import './variables.scss'; 	//常用的颜色
@import './mixin.scss';			//scss的mixin方式，抽象一些常用的类样式
@import './transition.scss';	//动画样式
@import './element-ui.scss';	//重写element的一些样式
@import './sidebar.scss';		//菜单栏的一些样式
@import './btn.scss';			//按钮样式
@import './customization.scss'; //部分自定义样式， 包括element默认主题

body {
  height: 100%;
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  text-rendering: optimizeLegibility;
  font-family: Helvetica Neue, Helvetica, PingFang SC, Hiragino Sans GB, Microsoft YaHei, Arial, sans-serif;
}
...
...
```

