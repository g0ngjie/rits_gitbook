### 登录页面



> 登录页面 

![](..\..\images\login.jpg)

```typescript
<script lang="ts">
import { Component, Vue } from "vue-property-decorator";// 装饰器引入
import { Action } from "vuex-class";//装饰器引入
import { RouterPath } from "@/utils/VEnum"; //路由枚举

interface ILogin { // 接口定义 可以参考 typescript文档，主要用于声明变量类型。
  username: string;
  password: string;
}
/**
	此页大部分功能可以参考 drive_book init 页面
*/
@Component({ name: "Login" })
export default class Login extends Vue {
  private form: any = {};

  created() {
    this.form = this.$form.createForm(this);
  }

  @Action("userLogin") //vuex action 函数调用
  private userLogin: any;

  handleSubmit(e: any) {
    e.preventDefault();
    this.form.validateFields((err: any, values: any) => {
      if (!err) {
        this.submit(values);
      }
    });
  }

  async submit(values: ILogin) {
    try {
      const isLogin = await this.userLogin(values);
      if (isLogin) {
        window.location.href = RouterPath.Home;
      }
    } catch (error) {
      return null;
    }
  }
}
</script>
```

