# 架构

##### 技术选型

> vue-cli@3、webpack@4、typescript@3.7.4、ant-design-vue@1.4.10、eslint@5.16.0、less@3.10.3

* **vue-cli** 【了解】 选择3的版本，考虑性能会比 **合同管理** 优化要好一些，**目录结构** 会更清晰，减少冗余的webpack配置。
* **webpack** 【了解】 选择4的版本，也是考虑到开发环境上的优化效果
* **typescript ** 【了解】
  * 对数据类型的支持
  * 可以在编译阶段就发现大部分错误。
  * 可以定义一些 `Interface`、`Enum` 等，提高代码可读性和可维护性
  * ……
* **[ant-design-vue](https://www.antdv.com/docs/vue/introduce/ )**【需要看文档】
  * 因为项目不大，考虑到UI交互上的细节要优于 *ElementUI*。
  * 可能编码上没有 *ElementUI* 友好，但目前的功能正常开发是完全OK的。
  * 由于 ant 思想是从 **React** 过渡的，所以组件渲染数据的方式都是采用的 **单向数据流** 的方式。
* **eslint**  【了解】
  * 也是考虑到代码规范化的问题。比如定义变量而未使用会报错，大大优化了可能会出现问题的代码。
  * 配置文件在项目根目录下的 **.eslintrc.js** 文件。
  * 对于一些格式的规范，可以考虑 vscode 安装 **Vetur** 格式化工具。
* **less** 【了解】 
  * ant-design-vue底层使用的是Less。
  * 考虑如果用其他预编译Css框架（Scss），可能会出现一些不必要的问题。
  * 用法和 *Scss* 类似

