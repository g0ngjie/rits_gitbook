### 用户申请激活页面



![](..\..\images\activate.jpg)



```javascript
<script>
import { user_activate, user_detail } from "@/api/user";// 接口引入
import { userRoleHandler, cascaderHandler } from '@/mixins';// 引入公共mixins
import { UserAccess } from "@/utils/Enum/FEnum";// 用户权限 服务端枚举

export default {
    mixins: [cascaderHandler, userRoleHandler],// 注册mixins
    data() {
        return {
            loading: false,
            form: { isLeader: false, roles: [], permissions: [], isAdmin: false },// form表单数据
            defRoles: [],// 默认角色列表
            defPowerList: [],// 默认权限列表
            adminList: []// 管理员列表
        };
    },
    methods: {
        async submit() {// 表单提交
            let _permissions = this.defPowerList.map(item => item.id)
            this.form.permissions = _permissions;
            let _roles = [];
            let set = new Set();
            this.defRoles.map(item => {
                set.add(item.id)
            });
            for (const item of set) {
                if (item != 99) {//管理员 不填入 角色里面
                    _roles.push(item)
                }
            }
            this.form.roles = _roles;
            //表单提交
            try {
                this.loading = true;
                let { data } = await user_activate(this.form._id, this.form);
                this.loading = false;
                this.$alert(this.$t("activate.submit"), { type: 'success', showClose: false }).then(() => {
                    this.logout();
                })
            } catch (error) {
                this.loading = false;
            }
        },
        logout() {// 调用vuex 前端登出
            this.$store.dispatch('fedLogOut').then(() => {
                location.href = '/';
            })
        },
        initList() {// 初始化页面所需要的数据
            if (this.queryParams.isAdmin) {
                this.form.isAdmin = true;
                //默认 roleList 最后一位是 管理员
                this.defRoles.push(this.roleList[this.roleList.length - 1])
                //默认 powerList 管理员 文档删除权限
                this.defPowerList.push(this.powerList[UserAccess.delete])
            }
            this.form = this.queryParams;
            for (let i = 0; i < this.roleList.length; i++) {
                const item = this.roleList[i];
                for (let j = 0; j < this.form.roles.length; j++) {
                    const id = this.form.roles[j];
                    if (item.id === id) {
                        this.defRoles.push(item);
                        continue;
                    }
                }
            }
            this.checkBoxHandler(this.defRoles);
            this.selectData = {
                show: false,
                name: this.queryParams.deptName
            }
        },
        async initAdminList() {//获取管理员列表
            const { data } = await user_detail({ isAdmin: true });
            if (data.errorCode == 0) {
                this.adminList = data.data.map(({ email, name }) => {
                    return { email, name: `${name} | ${email}` }
                })
            }
        }
    },
    beforeDestroy() {// 钩子、页面销毁前调用
        this.logout();
    },
    created() {//钩子 页面加载前调用
        // if(!this.queryParams._id){
        //   this.logout();
        // }
        this.initAdminList();
        this.initList();
    }
};
</script>
```

