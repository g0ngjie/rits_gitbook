### 驱动管理



> 驱动管理分成两个子模块： 当前驱动、驱动一览



##### components/Current

> 当前驱动

![](..\..\images\current_drive.jpg)



```typescript
<script lang="ts">
import { Component, Vue } from "vue-property-decorator";//装饰器引入
import { OsType, DriverType } from "@/utils/VEnum";//枚举
import PanelTemp from "./Panel.vue";// 自定义面板组件
import { drive_pages } from "@/api/drive";//接口
@Component({
  name: "Current",
  components: { PanelTemp }//组件注册
})
export default class Current extends Vue {
  private winData: any = { direct: null, roaming: null };
  private macData: any = { direct: null, roaming: null };
  
  //接口调用，获取 面板初始化 数据
  async initList() {
    const { data } = await drive_pages({ isNewest: true });
    if (data.errNo == 0) {
      const { win, mac } = OsType;
      const { direct, roaming } = DriverType;
      data.item.forEach((item: any) => {
        switch (item.osType) {
          case win:
            if (item.driverType == direct) {
              this.winData.direct = item;
            } else if (item.driverType == roaming) {
              this.winData.roaming = item;
            }
            break;
          case mac:
            if (item.driverType == direct) {
              this.macData.direct = item;
            } else if (item.driverType == roaming) {
              this.macData.roaming = item;
            }
            break;
          default:
            break;
        }
      });
    }
  }
  created() {//vue 钩子
    this.initList();
  }
}
</script>
```





##### components/DriveTable/table.ts

> 此处是ant-design-vue 使用表格（a-table）在字段声明时，和element使用的不同之处。

```typescript
export const columns: any[] = [
  {
    title: "时间",
    dataIndex: "createTime",
    sorter: true,
    scopedSlots: { customRender: "createTime" }
  },
  {
    title: "系统类型",
    dataIndex: "osType",
    scopedSlots: { customRender: "osType" }
  },
  {
    title: "驱动类型",
    dataIndex: "driverType",
    scopedSlots: { customRender: "driverType" }
  },
  {
    title: "系统位数",
    dataIndex: "cpuBits",
    scopedSlots: { customRender: "cpuBits" }
  },
  {
    title: "驱动名称",
    dataIndex: "fileName"
  },
  {
    title: "版本信息",
    dataIndex: "version"
  },
  {
    title: "变更履历",
    dataIndex: "description",
    width: "30%"
  },
  {
    title: "操作",
    align: "center",
    width: "15%",
    scopedSlots: { customRender: "handler" }
  }
];
```



##### components/DriveTable

> 驱动一览

![](..\..\images\drive_table.jpg)

```typescript
<script lang="ts">
import { Component, Vue } from "vue-property-decorator";//装饰器
import { drive_pages, drive_count, drive_delete } from "@/api/drive";//接口
import { columns } from "./table";//a-table 表格声明文件
import { OsType2Val, DriverType2Val, CpuBits2Val } from "@/common/Enum2Val";
import { OsType, CpuBits } from "@/utils/VEnum";//枚举
import { fmtDate } from "@/filters";//过滤器
import ModalTemp from "./Modal.vue";//自定义模态框组件
import { PaginationMixin } from "@/mixins";//分页 mixin

@Component({//组件声明
  name: "DriveTable",
  components: { ModalTemp },//组件注册
  filters: { fmtDate },//过滤器注册
  data() {//属性声明
    return {
      OsType2Val,
      DriverType2Val,
      CpuBits2Val,
      OsType
    };
  }
})
export default class DriveTable extends PaginationMixin {
  private loading: boolean = false;

  private data: any[] = [];
  private columns: any[] = columns;

  private visited: number = OsType.win;

  //modal
  private title: string = "";

  //下载函数
  handlerDownload(path: string) {
    window.location.href = path;
  }

  //表格 点击日期排序 时 触发
  handleTableChange(pagination: any, filters: any, sorter: any) {
    const pager = { ...this.pagination };
    pager.current = pagination.current;
    this.pagination = pager;
    this.initList();
  }

  //打开 modal
  openModal(title: string, row: any) {
    this.title = title;
    const temp: any = this.$refs.modal;
    this.$nextTick(() => {
      if (title === "edit") {
        temp.initFormData(title, row);
        temp.showModal(title);
      } else {
        temp.initFormData(title, { cpuBits: CpuBits.general });
        temp.showModal(title);
      }
    });
  }

  //删除 函数
  async handlerDelete(id: string) {
    try {
      const { data } = await drive_delete(id);
      this.$message.success("删除成功");
      this.initList();
    } catch (error) {
      return null;
    }
  }

  //操作系统 标签页 筛选框触发函数
  handlerBitsClick(e: number) {
    this.visited = e;
    this.initList();
  }

  //接口调用 获取表格数据
  async initList() {
    const paper: any = this.pagination;
    const current: number = paper.current;
    const query = {
      isNewest: false,
      page: current,
      count: this.pageSize,
      osType: this.visited
    };
    try {
      this.loading = true;
      const [{ data: page }, { data: count }] = await Promise.all([
        drive_pages(query),
        drive_count(query)
      ]);
      this.loading = false;
      this.pagination.total = count.item;
      this.data = page.item;
    } catch (error) {
      this.loading = false;
    }
  }
}
</script>
```



> 新建/编辑模态

![](..\..\images\drive_modal.jpg)

```typescript
<script lang="ts">
import { Component, Prop, Emit, Vue } from "vue-property-decorator";//装饰器
import { selectColumns } from "@/common/AntSelectConfig";//下拉框静态变量
import { Upload as UploadTemp } from "@/components";//自定义上传组件
import { drive_upload, drive_edit } from "@/api/drive";//接口
import { CpuBits } from "@/utils/VEnum";//枚举
import { IDriver } from "@/model/Driver";//变量声明文件
import { CloneDeep } from "@/utils";//工具
import formItemLayout from "@/common/FormItemLayout";//formItem样式

@Component({
  components: { UploadTemp }//组件注册
})
export default class DriveModdal extends Vue {
  private selectColumns: any = selectColumns;
  private isUpload: boolean = false;
  private visible: boolean = false;
  private confirmLoading: boolean = false;
  private form: any = {};
  private formItemLayout: any = formItemLayout;
  private uploadData: any = {};

  @Prop({ type: String }) title!: string;// vue prop

  created() {//钩子
    this.form = this.$form.createForm(this);
  }

  //附件上传、移除时触发
  handlerUpload(check: string, file: any) {
    if (check == "remove") {
      this.form.setFieldsValue({ filePath: null });
      delete this.uploadData.filePath;
      delete this.uploadData.fileName;
    }
    if (check == "done") {
      const { filePath, fileSize } = file;
      this.form.setFieldsValue({ filePath, fileSize });
    }
  }

  //modal 展示
  showModal(check: string) {
    this.visible = true;
    this.$nextTick(() => {
      const vm: any = this.$refs.uploadTemp;
      vm.initTemp(check);
    });
  }

  //表单提交
  async handleOk(): Promise<any> {
    //表单校验
    this.form.validateFields((err: any, values: any) => {
      if (!err) {
        this.submit(values);
      }
      return null;
    });
  }

  @Emit("submit")// this.$emit("submit", [boolean]);
  async submit(form: any) {
    if (!form) return null;
    const { fileName } = form;
    const _fileName = fileName.replace(/^\s+|\s+$/g, "");
    if (!_fileName) {
      this.$message.error("驱动名称不能为空！");
      return null;
    }
    try {
      if (this.title === "edit") {
        const _editForm: IDriver = CloneDeep(form);
        delete _editForm._id;
        const { data } = await drive_edit(form._id, _editForm);
        this.handleCancel();
        this.$message.success("编辑成功");
        return true;
      } else {
        const { data } = await drive_upload(form);
        if (data.errNo == 0) {
          this.handleCancel();
          this.$message.success("操作成功");
          return true;
        }
      }
    } catch (error) {
      return false;
    }
  }

  //modal 关闭时 触发
  handleCancel() {
    const vm: any = this.$refs.uploadTemp;
    this.visible = false;
  }

  //初始化form表单 数据
  initFormData(check: string, values: IDriver) {
    if (check == "edit") {
      this.$nextTick(() => {
        const { httpPath } = values;
        const datas = httpPath.split("/");
        const driverName = datas[datas.length - 1];
        this.uploadData = { driverName };
      });
    } else {
      this.uploadData = {};
    }
    const {
      _id,
      fileName,
      fileSize,
      driverType,
      osType,
      cpuBits,
      version,
      description,
      filePath
    } = values;
    this.form.setFieldsValue({
      _id,
      fileName,
      fileSize,
      driverType,
      osType,
      cpuBits,
      version,
      description,
      filePath
    });
  }
}
</script>
```

