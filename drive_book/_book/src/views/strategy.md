### 登录页面



> 升级策略 

![](..\..\images\strategy.jpg)

```typescript
<script lang="ts">
import { Component, Vue } from "vue-property-decorator";
import { ModalTemp } from "./components";// 设置的模态组件引入
import { strategy } from "@/api/strategy";// 接口引入
import { IStrategy } from "@/model/Strategy";// Strategy Model DTO
import { TimesToDayHourMin } from "@/utils";// 时间戳转换工具
import { Method2Val } from "@/common/Enum2Val"; // 枚举对应value

@Component({ name: "Strategy", components: { ModalTemp } })
export default class Strategy extends Vue {
  private Method2Val: any = Method2Val;// 需要先声明才可以再Dom上使用
  private data: any = {
    time: {
      day: 0,
      hour: 0,
      min: 10
    },
    method: 0
  };

  async openModal() {// 打开设置模态 函数
    const temp: any = this.$refs.modal;
    this.$nextTick(() => {
      temp.showModal(this.data);
    });
  }

  async initData() {// 获取数据接口
    const { data } = await strategy();
    if (data.errNo == 0) {
      const { strategy } = data.item;
      const { method, timeInterval }: IStrategy = strategy;
      if (timeInterval) {
        const [day, hour, min]: number[] = TimesToDayHourMin(timeInterval);
        this.data.time = { day, hour, min };
      }
      this.data.method = method || 0;
    }
  }

  created() {// 钩子，页面加载前触发
    this.initData();
  }
}
</script>
```



> 设置Modal



![](..\..\images\strategy_modal.jpg)



```typescript
<script lang="ts">
import { Component, Emit, Vue } from "vue-property-decorator";
import { selectColumns } from "@/common/AntSelectConfig";
import { strategy_set } from "@/api/strategy";
import { CloneDeep, DayHourMinToTimes } from "@/utils";
import { IStrategy } from "@/model/Strategy";
import formItemLayout from "@/common/FormItemLayout";

@Component({ name: "StrategyModal" })
export default class StrategyModal extends Vue {
  private selectColumns: any = selectColumns;// 驱动升级方式 下拉的静态数据
  private visible: boolean = false; // Modal 显示/隐藏
  private form: any = {}; // 表单数据
  private formItemLayout: any = formItemLayout; // form 的样式
  private minMin: number = 0; // 最小 分钟

  private upgradeTimes: any = {
    day: 0,
    hour: 0,
    min: 10
  };

  created() {// 钩子 页面加载前
    this.form = this.$form.createForm(this);// 初始化 form表单
  }

  handlerTime(key: string) {// 当 日/时/分 发生改变时 触发
    this.upgradeTimes = this.fmtDayHourMin(this.upgradeTimes, key);
  }

  fmtDayHourMin(upgrades: any, key: string) {// 格式化 日/时/分 最大 7 0 0 最小 0 0 10
    const maxDay = 7;
    const cache = CloneDeep(upgrades);
    const { day, hour, min } = upgrades;
    if (!day) cache.day = 0;
    if (!hour) cache.hour = 0;
    if (!min) cache.min = 0;
    try {
      if (day === maxDay && (hour != 0 || min != 0)) {
        if (["hour", "min"].includes(key)) {
          cache.day = 0;
        } else {
          cache.hour = 0;
          cache.min = 0;
        }
      }
      this.minMin = day === 0 && hour === 0 ? 10 : 0;
      return cache;
    } catch (error) {
      return cache;
    }
  }

  handleCancel() {// 关闭 Modal
    this.visible = false;
  }

  handleSubmit() {// 表单提交前校验
    this.form.validateFields((err: any, values: any) => {
      if (!err) {
        const { day, hour, min } = this.upgradeTimes;
        const timeInterval = DayHourMinToTimes(day, hour, min);
        const result: any = { ...values, timeInterval };
        this.submit(result);
      }
    });
  }

  @Emit("submit")
  async submit(form: IStrategy) {// 表单提交
    try {
      await strategy_set(form);
      this.handleCancel();
      this.$message.success("操作成功");
      return true;
    } catch (error) {
      return false;
    }
  }

  showModal(values: any): void {// Modal显示，并设置 表单默认值
    const { method, time } = CloneDeep(values);
    this.upgradeTimes = time;
    setTimeout(() => {
      this.form.setFieldsValue({ method });
    }, 0);
    this.visible = true;
  }
}
</script>
```

