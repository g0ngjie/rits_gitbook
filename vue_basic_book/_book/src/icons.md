### svg文件

> 如果你没有在本项目 Icons中找到需要的图标，可以到iconfont.cn上选择并生成自己的业务图标库，再进行使用。或者其它 svg 图标网站，下载 svg 并放到文件夹之中就可以了。



#####  生成图标库代码

![](..\images\iconfont.gif)



##### 使用方式

```javascript
<svg-icon icon-class="password" /> // icon-class 为 icon 的名字
```

