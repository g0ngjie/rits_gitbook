### 登录首页



> 该包下共四个文件，index.js、forget、thirdParty、office.js



![](..\..\images\login.jpg)



##### index.js

登录界面



```javascript
<script>
import forget from './forget'; //密码找回模态
import officeExtend from './office'; //office登录js
import { LangSelect } from '@/components'; //多语言选择器，目前是屏蔽状态
import { LocalStorageEnum as LSEnum } from '@/utils/Enum/VEnum';//枚举

export default {
    name: 'Login',
    components: { forget, LangSelect },
    extends: officeExtend, //三方登录函数继承
    data() {
        return {
            loginForm: {}, //登录参数
            passwordType: 'password', //密码输入框 展示、隐藏
            loading: false,	//loading
            redirect: undefined, //watch 监听 $router 赋值重定向页面
            style: {},  // 根据主题改变
            fontStyle: {}, // 根据主题改变
            guideImg: { isShow: false }, // 引导功能。 目前暂未使用
            msalUserName: localStorage.getItem(LSEnum.officeLogin) //office登录时的账户
        }
    },
    watch: {
        $route: { //检测 $router 是否存在重定向页面
            handler: function (route) {
                this.redirect = route.query && route.query.redirect
            },
            immediate: true
        }
    },
    methods: {
        forget() {// 忘记密码 模态框打开
            this.$refs.forget.open();
        },
        showPwd() {// 密码输入框 隐藏、展示
            if (this.passwordType === 'password') {
                this.passwordType = ''
            } else {
                this.passwordType = 'password'
            }
        },
        userLogin(data) {// 登录接口
            this.loading = true
            this.$store.dispatch('userLogin', data).then(user => {
                this.loading = false
                this.$notify({ title: this.$t('login.isLogin.title'), message: this.$t('login.isLogin.success'), type: 'success' })
                this.$router.push({ path: this.redirect || '/' })
            }).catch(() => {
                this.loading = false
            })
        },
        handleLogin() {// 用户点击登录
            if (this.msalUserName) {
                this.$message.warning(this.$t('activate.tooltip'));
                return;
            }
            this.$refs.loginForm.validate(valid => {
                if (valid) {
                    this.userLogin(this.loginForm, null);
                } else {
                    console.log('error submit!!')
                    return false
                }
            })
        },
        initStyle() { // 初始化 主题样式
            const defaultColor = localStorage.getItem(LSEnum.localTheme) || '#3C8DBD'//'#409EFF';
            this.style = {
                'background-color': defaultColor,
                'border-color': defaultColor
            }
            this.fontStyle = {
                color: defaultColor
            }
            if (this.msalUserName) this.style = {};
        }
    },
    created() {// 钩子 页面加载前
        this.initStyle();
    }
}
</script>
```



##### office.js

office登录js文件

```javascript
import Driver from 'driver.js';// 引导页插件
import 'driver.js/dist/driver.min.css';// 引导页 css
import { user_office } from '@/api/user';// 获取office365配置信息
import { LocalStorageEnum as LSEnum } from '@/utils/Enum/VEnum';// 枚举

export default {
    methods: {
        runGuide() {// 引导页 默认参数配置
            const driver = new Driver({
                allowClose: false,
                onReset: (Element) => {
                    this.onClose(Element);
                }
            });
            driver.defineSteps([
                {
                    element: '#on',
                    popover: {
                        title: '提示',
                        description: '首次登录，浏览器可能会出现阻止弹窗事件。',
                        position: 'bottom'
                    }
                },
                {
                    element: '#tw',
                    popover: {
                        title: '提示',
                        description: '我们以FireFox为例',
                        position: 'right'
                    }
                },
                {
                    element: '#th',
                    popover: {
                        title: '提示',
                        description: '使用前请允许弹出窗口',
                        position: 'bottom'
                    }
                }
            ])
            driver.start();
        },
        onClose(e) {// 引导页 关闭
            localStorage.setItem(LSEnum.guide, Date.now());
            this.guideImg.isShow = false;
            this.$router.replace({ path: '/msg', query: { el: 'login' } });
        },
        async openLink() {// office登录 按钮触发
            const { data } = await user_office();
            if(data.errorCode == 0){
                const url = data.data.url;
                window.location.href = url;
            }
        },
        logout() {// office 登出 按钮触发
            this.$router.replace({ path: '/msg', query: { el: 'logout' } });
        },
    },
}
```



##### thirdParty

office登录组件

```javascript
<script>
import { mapGetters } from 'vuex';
import { user_office } from '@/api/user';// 获取office365配置信息
import { LocalStorageEnum as LSEnum } from '@/utils/Enum/VEnum';// 枚举

export default {
    computed: {
        ...mapGetters(['urlQuery']) // url参数
    },
    methods: {
        async logout() {// office 登出
            const { data } = await user_office();
            if (data.errorCode == 0) {
                localStorage.removeItem(LSEnum.officeLogin)
                const redirectUrl = data.data.redirectUrl;
                if (redirectUrl) {
                    const _url = encodeURIComponent(redirectUrl)
                    /**
                    	此链接为 office官方注销链接。重定向uri为项目主页。
                    	注销成功后会自动跳转到uri指向的页面
                    	注意：此uri必须为外网可以访问的地址。实际公网的域名。
                    */
                    window.location.href = `https://login.microsoftonline.com/common/oauth2/v2.0/logout?post_logout_redirect_uri=${_url}`;
                }
            }
        },
        officeLogin(data) {// office登录
            const query = this.urlQuery;
            this.$store.dispatch('userLogin', data).then(user => {
                const { status, email } = user;
                localStorage.setItem(LSEnum.officeLogin, email);
                if (status == 2) {
                    this.$store.dispatch("setParams", user);
                    this.$router.replace({ path: '/activate' });
                    return;
                } else if (query && query.c) {
                    this.$router.replace('/updateuser');
                } else {
                    this.$router.push({ path: this.redirect || '/' })
                }
                this.$notify({ title: this.$t('login.isLogin.title'), message: this.$t('login.isLogin.success'), type: 'success' })
            }).catch(() => { })
        }
    },
    created() {// 钩子，页面加载前， 判断url参数
        const query = this.$route.query;
        const { state, code, el } = query;
        if (el && el == 'logout') {
            this.logout();
        } else if (state == 'undefined' || code == 'undefined') {
            this.$router.replace("/");
        } else if (state && code) {
            this.officeLogin({ state, code, type: 2 })
        } else {
            this.$router.replace("/");
        }
    }
};
</script>
```



##### forget

忘记密码模态

```javascript
<script>
import { user_pass } from "@/api/user";

export default {
    data() {
        return {
            loading: false,// loading
            isShow: false, //模态框 是否展示
            form: {}// 表单数据
        };
    },
    props: {
        theme: {// 主题样式
            type: Object,
            default() { }
        }
    },
    methods: {
        open() {// 初始化函数，父组件调用
            this.form = {};
            setTimeout(() => {
                this.$refs.form.clearValidate();
            }, 100);
            this.isShow = true;
        },
        async submit() {// 表单提交函数
            this.loading = true;
            try {
                let { data } = await user_pass(this.form);
                this.loading = false;
                if (data.errorCode == 0) {
                    this.$message.success(this.$t("login.msg.success"));
                    this.isShow = false;
                }
            } catch (error) {
                this.loading = false;
            }
        },
        async validate() {// 表单校验
            this.$refs.form.validate(valid => {
                if (valid) {
                    this.submit();
                }
            });
        }
    }
};
</script>
```

