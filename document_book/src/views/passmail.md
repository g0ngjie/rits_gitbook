### 密码找回

![](..\..\images\passmail.jpg)



##### index.vue

```javascript
<script>
import { user_passwd } from '@/api/user'; // 接口文件
import { LocalStorageEnum as LSEnum } from '@/utils/Enum/VEnum'; // 前端枚举
import { PassUpdMethod as PassEnum } from '@/utils/Enum/FEnum'; // 服务端枚举

export default {
    data() {
        return {
            loading: false, // loading
            form: {} // 表单数据
        }
    },
    methods: {
        async submit() { // 表单提交
            const { newPass, repeatPass } = this.form;
            if (newPass != repeatPass) {
                this.$message.warning("两次密码不一致");
                return;
            }
            try {
                this.loading = true;
                let { data } = await user_passwd({ type: PassEnum.email, newPass, repeatPass, verification: this.$route.query.verification });
                this.loading = false;
                this.$alert('密码修改成功，请重新登录', '提示', {
                    confirmButtonText: '确定',
                    type: 'success', showClose: false
                }).then(() => {
                    localStorage.removeItem(LSEnum.token);
                    location.href = '/';
                    return;
                })
            } catch (error) {
                this.loading = false;
            }
        },
        async validate() {// 表单校验
            this.$refs.form.validate(valid => {
                if (valid) {
                    this.submit();
                }
            });
        },
    }
}
</script>
```

