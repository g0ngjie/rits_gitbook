### 日志管理



![](..\..\images\logs.jpg)



##### components

搜索栏组件

```typescript
<script lang="ts">
import { Component, Vue } from "vue-property-decorator";//装饰器引入
import { selectColumns } from "@/common/AntSelectConfig";//下拉框静态变量
import { ObjectToCleanUndefined as CleanUndefined } from "@/utils";//工具
import { DatePickerMixin } from "@/mixins";//日期 mixin
import { log_installversions } from "@/api/log";//接口
@Component({
  name: "LogsSearchTemp"
})
export default class LogsSearchTemp extends DatePickerMixin {
  private selectColumns: any = selectColumns;
  private form: any = {};
  //点击搜索 函数
  handleSearch(e: any) {
    e.preventDefault();
    const [startTime, endTime] = [
      this.startValue?.valueOf(),
      this.endValue?.valueOf()
    ];
    if ((startTime && !endTime) || (endTime && !startTime)) {
      this.$message.warning("日期区间格式错误");
      return;
    }
    let _formDate = {};
    if (startTime && endTime) {
      _formDate = { startTime, endTime };
    }
    const values = this.form.getFieldsValue();
    const _push = CleanUndefined({ ...values, ..._formDate });
    this.$emit("search", _push);
  }

  //清空搜索函数
  handleReset() {
    this.startValue = null;
    this.endValue = null;
    this.form.resetFields();
    this.$emit("search", {});
  }
  
  //初始化 搜索栏 升级前后版本 数据
  async initVersions() {
    const { data } = await log_installversions();
    if (data.errNo === 0) {
      const { oldVers, newVers } = data.item;
      this.form = { oldVer: oldVers.filter((ver: any) => ver), newVer: newVers.filter((ver: any) => ver), ...this.form };
    }
  }

  created() {//vue钩子函数 页面加载前触发
    this.form = this.$form.createForm(this);
    this.initVersions();
  }
}
</script>
```



##### logs.ts

> 此处是ant-design-vue 使用表格（a-table）在字段声明时，和element使用的不同之处。

```typescript
export const columns: any[] = [
  {
    title: "时间",
    dataIndex: "createTime",
    sorter: true,
    scopedSlots: { customRender: "createTime" }
  },
  {
    title: "用户名",
    dataIndex: "userName"
  },
  {
    title: "系统类型",
    dataIndex: "osType",
    scopedSlots: { customRender: "osType" }
  },
  {
    title: "系统版本",
    dataIndex: "osVer"
  },
  {
    title: "驱动类型",
    dataIndex: "driverType",
    scopedSlots: { customRender: "driverType" }
  },
  {
    title: "系统位数",
    dataIndex: "cpuBits",
    scopedSlots: { customRender: "cpuBits" }
  },
  {
    title: "升级前版本",
    dataIndex: "oldVer"
  },
  {
    title: "升级后版本",
    dataIndex: "newVer"
  },
  {
    title: "结果",
    dataIndex: "result",
    scopedSlots: { customRender: "result" }
  },
  {
    title: "操作",
    align: "center",
    width: "15%",
    scopedSlots: { customRender: "handler" }
  }
];
```



##### index.vue

日志主页，表格、分页等

```typescript
<script lang="ts">
import { Component, Vue } from "vue-property-decorator";//装饰器
import { SearchTemp } from "./components";//搜索组件引入
import { log_pages, log_count } from "@/api/log";//接口
import { columns } from "./logs";//a-table 表格字段 声明文件
import { OsType2Val, DriverType2Val, CpuBits2Val, Result2Val } from "@/common/Enum2Val";//枚举对应结果集
import { CpuBits, UpgradeResult } from "@/utils/VEnum";//枚举文件引入
import { PaginationMixin } from "@/mixins";//分页 mixin
import { fmtDate } from "@/filters";//过滤器

/**
	对于 components、filters、引入的变量等 需要按照类似下面 这种写法。
	此处使用 按照原生vue的使用方式。
*/
@Component({// 组件声明
  name: "DriveTable",
  components: { SearchTemp }, //组件注册
  filters: { fmtDate }, //过滤器注册
  data() { // 属性声明
    return {
      OsType2Val,
      DriverType2Val,
      CpuBits2Val,
      Result2Val,
      UpgradeResult,
      CpuBits
    };
  }
})
export default class Logs extends PaginationMixin {
  private loading: boolean = false;
  private data: any[] = [];
  private columns: any[] = columns;

  //下载函数
  handlerDownload(path: string) {
    window.location.href = path;
  }

  //表格 点击日期排序 时 触发
  handleTableChange(pagination: any, filters: any, sorter: any) {
    const pager = { ...this.pagination };
    pager.current = pagination.current;
    this.pagination = pager;
    this.initList();
  }

  //接口获取 列表数据函数
  async initList(search?: any) {
    const paper: any = this.pagination;
    const current: number = paper.current;
    let query = {
      page: current,
      count: this.pageSize
    };
    if (search) query = { ...query, ...search };
    try {
      this.loading = true;
      const [{ data: page }, { data: count }] = await Promise.all([
        log_pages(query),
        log_count(query)
      ]);
      this.loading = false;
      this.pagination.total = count.item;
      this.data = page.item;
    } catch (error) {
      this.loading = false;
    }
  }

  created() {//钩子 页面创建前触发
    this.initList();
  }
}
</script>
```

