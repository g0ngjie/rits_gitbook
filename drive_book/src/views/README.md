# views 组件包、视图目录

* 目录

  * drive 驱动管理

  * init 管理员初始化

  * login 登录页面

  * logs 日志管理

  * redirect 重定向跳转页面 => 此页面主要用来做一些页面跳转传值。目前暂未使用，无需处理。

* App.vue 根组件

  > 因为Ant默认国际化用的是 *英文* ，所以这里需要调整为中文状态。

  ```typescript
  	<template>
    <a-locale-provider :locale="locale">
      <div id="app">
        <loader :spinning="spinning" fullScreen></loader>
        <router-view />
      </div>
    </a-locale-provider>
  </template>
  <script lang="ts">
  import { Component, Vue } from "vue-property-decorator"; //装饰器引入
  import { Action, Getter } from "vuex-class"; //装饰器引入
  import zhCN from "ant-design-vue/lib/locale-provider/zh_CN";//中文支持
  import { Loader } from "@/components"; //加载全局 loading组件
  @Component({
    components: { Loader } //注册组件
  })
  export default class App extends Vue {
    locale = zhCN;
    @Getter("spinning") //获取vuex里的属性
    private spinning: any;
  }
  </script>
  ```

  

* main.ts vue主入口文件

  ```typescript
  import Vue from "vue";
  //解决vue在ie的兼容性问题
  import "babel-polyfill";
  //根组件引入
  import App from "./App.vue";
  //路由引入
  import router from "./router";
  //vuex
  import store from "./store";
  // global styles
  import "@/styles/index.less";
  //iconfont 暂未使用
  // import "@/assets/icon/iconfont.css";  
  // vue-antdesign 按需加载
  import "@/common/frame";
  // interceptor 拦截器注入
  import "@/common/interceptor";
  
  Vue.config.productionTip = false;
  
  new Vue({
    router,  //路由注册
    store,   //vuex 注册
    render: h => h(App) //跟组件渲染
  }).$mount("#app");  //挂载到 public/index.html 里面的 id为app的Dom元素下
  ```

  

* shims-tsx.d.ts  shmis-vue.d.ts  => TS 针对vue的声明文件，配置一次，无需在处理

