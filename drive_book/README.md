# 概要



> 此手册仅限于梳理结构。
>
> 对于一些概念。相信实践会更有效率。
>
> 功能使用最好结合实际操作。



##### Book本地预览

本地启动的话需要预装 `http-server` 模块

```shell
npm install -g http-server
```

通过启动命令，然后在浏览器输入 `http://localhost:8080`，就可以查看

```javascript
http-server -p [port] [book_path] // cd到项目根目录下 http-server -p 8080 . 
```



##### 项目目录结构

```javascript
.
├── babel.config.js
├── dev.config.js			    //Vue配置文件，开发环境的 **ip，端口号，代理** 等
├── dist                        //yarn build 打包的静态资源
├── LICENSE                     //开源协议
├── package.json                //项目说明文件
├── public                      //主页，项目入口 => index.html、favicon.ico
│   ├── favicon.ico
│   └── index.html
├── README.md                   //阅读指南
├── src                         //
│   ├── api                     //接口文件
│   ├── App.vue                 //根组件
│   ├── assets                  //资源文件 icon，image
│   ├── common                  //公共包 拦截器、api请求工具、ant按需加载集等
│   ├── components              //公共组件库
│   ├── filters                 //过滤器
│   ├── layout                  //布局组件
│   ├── main.ts                 //入口文件
│   ├── mixins                  //多继承
│   ├── mock                    //Mock数据文件，何以忽略
│   ├── model                   //定义DTO、Interface文件
│   ├── router                  //路由
│   ├── shims-tsx.d.ts          //TS声明文件 无需再处理
│   ├── shims-vue.d.ts          //TS声明文件 无需再处理
│   ├── store                   //Vuex
│   ├── styles                  //全局共有样式库
│   ├── types                   //全局自定义TS声明文件 无需再处理
│   ├── utils                   //工具包
│   └── views                   //组件包、视图
├── tests                       //单元测试 可以忽略，没有维护
│   └── unit
├── tsconfig.json               //TS配置文件
├── vue.config.js               //Vue配置文件
└── yarn.lock
```



##### 启动

cd到项目根目录下

`yarn dev`  或者 `npm run dev`

##### 打包

`yarn build` 或者 `npm run build`

在根目录下面会生成 **dist** 文件夹，把里面的所有文件都放到 **服务端** 的静态资源访问路径下。

