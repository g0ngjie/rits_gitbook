### 个人信息修改

![](..\..\images\updateinfo_main.jpg)



##### index.vue

```javascript
<script>
import infoTemp from './info';// 个人信息组件
import passTemp from './pass';// 密码修改组件
import { mapGetters } from 'vuex';

export default {
    components: {// 组件注册
        infoTemp,
        passTemp
    },
    data() {
        return {
            isTemp: 'info-temp',
            active: 'info'
        }
    },
    computed: {
        ...mapGetters(['theme']),
        activeColor() {
            return { color: this.theme }
        }
    },
    methods: {
        back() {// 返回
            this.$router.go(-1);
        },
        edit(url) {// 点击 展示对应的 组件
            this.active = url;
            this.isTemp = url + '-temp';
        }
    }
}
</script>
```



##### info/index.vue

```javascript
<script>
import { user_agents, user_own, user_info_get } from '@/api/user'; // 接口文件
import { mapGetters } from 'vuex';
import { Switch as vswitch } from '@/components'; // 组件
import { LocalStorageEnum as LSEnum } from '@/utils/Enum/VEnum'; // 前端枚举

export default {
    data() {
        return {
            loading: false, // loading
            headers: { token: localStorage.getItem(LSEnum.token) }, // 请求头部信息，用于 头像上传
            form: { // form表单数据，代理人
                agentUid: null
            },
            switchData: {// switch 选择器，是否启用代理
                isProxy: false,
            },
            agentList: [], // 代理人数据列表
            defRoles: [], // 默认角色列表
        }
    },
    components: { // 组件注册
        vswitch
    },
    watch: {// 监听 roleList 变化
        roleList() {
            let roleCache = [];
            this.defRoles.forEach(item => {
                this.roleList.forEach(el => {
                    if (item.id == el.id) {
                        roleCache.push(el)
                    }
                })
            })
            this.defRoles = roleCache;
        }
    },
    computed: {
        ...mapGetters([
            'avatar'
        ]),
        roleList() {// 角色 配置文件
            return require("@/views/usermanage/temp/conf")['roleList' + this.$t('lang.type')];
        },
        powerList() {// 权限 配置文件
            return require("@/views/usermanage/temp/conf")['powerList' + this.$t('lang.type')];
        }
    },
    methods: {
        proxyChange(e) {// switch 改变是 触发
            if (!e) {
                delete this.form.agentUid
            }
        },
        beforeUpload({ size, name }) {// 上传头像 前 触发
            var index = name.lastIndexOf(".");
            if (index == -1) {
                this.$message.warning(this.$t('updateinfo.info.msg.incorrect'));//请上传正确的图片格式
                return false;
            }
            var ext = name.substr(index + 1);
            const cache = ["png", "jpeg", "jpg"];
            if (cache.indexOf(ext) == -1) {
                this.$message.warning(this.$t('updateinfo.info.msg.incorrect'));
                return false;
            }
            if (!(size / 1024 < 2400)) {//2048 = 2M 考虑包容性 拓宽为2.4M
                this.$message.warning(this.$t('updateinfo.info.msg.exceed'));
                return false;
            }
            this.loading = true;
        },
        uploadSuccess({ data, errorCode }, file, fileList) {// 头像上传成功后 触发
            this.loading = false;
            if (errorCode === 0) {
                this.$store.dispatch("setAvatar", 'data:image/jpeg;base64,' + data.avatarBuffer);
                this.$message.success(this.$t('updateinfo.info.msg.success'));
            }
        },
        uploadError() {// 头像上传失败 触发
            this.loading = false;
        },
        async submit() {//表单提交
            const { email, account, agentUid } = this.form;
            try {
                this.loading = true;
                let { data } = await user_own({ email, account, agentUid });
                this.loading = false;
                this.$message.success(this.$t('updateinfo.info.msg.edit'));
            } catch (error) {
                this.loading = false;
            }
        },
        async validate() {// 表单校验
            this.$refs.form.validate(valid => {
                if (valid) {
                    this.submit();
                }
            });
        },
        async initAgentList() {// 初始化数据，获取 代理人 列表
            try {
                let { data } = await user_agents();
                if (data.errorCode == 0) {
                    let invalid = true;
                    this.agentList = data.data.map(({ _id, email, name }) => {
                        return { _id, name: `${name} | ${email}` }
                    })
                    for (let i = 0; i < data.data.length; i++) {
                        const { _id } = data.data[i];
                        if (this.form.agentUid == _id) {
                            invalid = false;
                            break;
                        }
                    }
                    if (invalid) {
                        this.proxyChange();
                    }
                    if (this.form.agentUid) {
                        this.switchData.isProxy = true;
                    }
                }
            } catch (error) {

            }
        },
        fmtRolePower() {// 格式化 默认角色列表
            if (this.form.isAdmin) {
                this.form.isAdmin = true;
                //默认 roleList 最后一位是 管理员
                this.defRoles.push(this.roleList[this.roleList.length - 1])
            }
            for (let i = 0; i < this.roleList.length; i++) {
                const item = this.roleList[i];
                for (let j = 0; j < this.form.roles.length; j++) {
                    const id = this.form.roles[j];
                    if (item.id === id) {
                        this.defRoles.push(item);
                        continue;
                    }
                }
            }
        },
        async initUser() {// 初始化获取 用户信息
            const { data } = await user_info_get();
            if (data.errorCode === 0) {
                this.form = data.data;
            }
        }
    },
    async created() {// 钩子、页面加载前触发
        await this.initUser();
        this.fmtRolePower();
        this.initAgentList();
    }
}
</script>
```



![](..\..\images\updateinfo_pass.jpg)



```javascript
<script>
import { user_passwd } from '@/api/user';// 接口文件
import { mapGetters } from 'vuex';
import { LocalStorageEnum as LSEnum } from '@/utils/Enum/VEnum';// 前端枚举
import { PassUpdMethod as PassEnum } from '@/utils/Enum/FEnum';// 服务端枚举

export default {
    data() {
        return {
            loading: false, // loading
            form: {} // 表单数据
        }
    },
    computed: {
        ...mapGetters([
            'user'
        ]),
    },
    methods: {
        async submit() {// 表单提交
            const { newPass, repeatPass, originPass } = this.form;
            // 两次密码不一致
            if (newPass != repeatPass) {
                this.$message.warning(this.$t('updateinfo.password.msg.warning'));
                return;
            }
            const { account, email } = this.user;
            // 密码修改成功，请重新登录
            try {
                this.loading = true;
                let { data } = await user_passwd({ type: PassEnum.reset, account, email, newPass, repeatPass, originPass });
                this.loading = false;
                this.$alert(this.$t('updateinfo.password.msg.alert.content'), this.$t('updateinfo.password.msg.alert.tips'), {
                    type: 'success', showClose: false
                }).then(() => {
                    localStorage.removeItem(LSEnum.token);
                    location.href = '/';
                    return;
                })
            } catch (error) {
                this.loading = false;
            }
        },
        async validate() {// 表单校验
            this.$refs.form.validate(valid => {
                if (valid) {
                    this.submit();
                }
            });
        },
    }
}
</script>
```

