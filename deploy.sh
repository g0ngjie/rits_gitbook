#!/bin/bash

git pull origin master \
&& docker-compose down \
&& docker rmi ritsbook_image \
&& docker-compose build \
&& docker-compose up -d
